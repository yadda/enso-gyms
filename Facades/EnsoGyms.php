<?php

namespace Yadda\Enso\Gyms\Facades;

use Illuminate\Support\Facades\Facade;

class EnsoGyms extends Facade
{
    protected static function getFacadeAccessor()
    {
        return 'enso-gyms';
    }
}
