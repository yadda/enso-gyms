<?php

namespace Yadda\Enso\Gyms\Controllers\Json;

use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Response;
use Yadda\Enso\Gyms\Contracts\Basket;
use Yadda\Enso\Gyms\Contracts\BasketController as ControllerContract;
use Yadda\Enso\Gyms\Contracts\Package;
use Yadda\Enso\Gyms\Contracts\Promotion;

class BasketController extends BaseController implements ControllerContract
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    /**
     * Gets the current contents of the basket.
     *
     * @return Response
     */
    public function getBasket()
    {
        $basket = App::make(Basket::class);

        return Response::json($basket->toArrayWithDiscounts(), 200);
    }

    /**
     * Updates the quantity of a package already in the Basket, or adds the
     * given quantity of that package to the Basket
     *
     * @param Request $request
     *
     * @return Response
     */
    public function setPackage(Request $request)
    {
        $this->validate($request, [
            'quantity' => ['nullable', 'numeric']
        ]);

        $package = resolve(Package::class)::find($request->get('package_id'));
        $quantity = $request->get('quantity', 1);

        if (!$package) {
            return Response::json([
                'message' => 'Unable to find requested Package',
            ], 404);
        }

        $basket = App::make(Basket::class);

        $basket->setPackageQuantity($package, $quantity);

        return Response::json($basket->toArrayWithDiscounts(), 200);
    }

    /**
     * Removes the given Package from the Basket
     *
     * @param Request $request
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function removePackage(Request $request)
    {
        $package = resolve(Package::class)::find($request->get('package_id'));

        if (!$package) {
            return Response::json([
                'message' => 'Unable to find requested Package',
            ], 404);
        }

        $basket = App::make(Basket::class);

        $basket->removePackage($package);

        return Response::json($basket->toArrayWithDiscounts(), 200);
    }

    /**
     * Adds a Promotion to the current Basket, if it is valid for the current
     * Basket.
     *
     * @param Request $request
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function setPromotion(Request $request)
    {
        $promotion = resolve(Promotion::class)::query()
            ->where('code', $request->get('code'))
            ->isRedeemable()
            ->first();

        if (!$promotion) {
            return Response::json([
                'message' => 'Unable to find requested Promotion',
            ], 404);
        }

        $basket = App::make(Basket::class);

        if (!$basket->promotionIsValidForBasket($promotion)) {
            Response::json([
                'message' => 'That Promotion is invalid for the current Basket',
            ], 403);
        }

        $basket->addPromotion($promotion);

        return Response::json($basket->toArrayWithDiscounts(), 200);
    }

    /**
     * Removes the given Promotion from the Basket
     *
     * @param Request $request
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function removePromotion(Request $request)
    {
        $promotion = resolve(Promotion::class)::query()
            ->where('code', $request->get('code'))
            ->isRedeemable()
            ->first();

        if (!$promotion) {
            return Response::json([
                'message' => 'Unable to find requested Promotion',
            ], 404);
        }

        $basket = App::make(Basket::class);

        $basket->removePromotion($promotion);

        return Response::json($basket->toArrayWithDiscounts(), 200);
    }
}
