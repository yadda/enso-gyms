<?php

namespace Yadda\Enso\Gyms\Controllers\Json;

use Exception;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Response;
use Yadda\Enso\Gyms\Checkout\OrderGenerator;
use Yadda\Enso\Gyms\Checkout\PaymentGateway;
use Yadda\Enso\Gyms\Contracts\Basket;
use Yadda\Enso\Gyms\Contracts\CheckoutController as ControllerContract;
use Yadda\Enso\Gyms\Exceptions\OrderGenerationException;
use Yadda\Enso\Gyms\Exceptions\PaymentGatewayException;
use Yadda\Enso\Gyms\Models\Billing;
use Yadda\Enso\Gyms\Traits\UsesPage;
use Yadda\Enso\Users\Contracts\User;

class CheckoutController extends BaseController implements ControllerContract
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests, UsesPage;

    protected $payment_gateway;

    protected $generator;

    protected $billing_information_rules = [
        'billing_name' => ['required'],
        'billing_email' => ['required', 'email'],
        'billing_phone' => ['required'],
        'billing_address_1' => ['required'],
        'billing_city' => ['required'],
        'billing_postcode' => ['required'],
        'billing_country' => ['required'],
    ];

    public function __construct(PaymentGateway $gateway, OrderGenerator $generator)
    {
        $this->payment_gateway = $gateway;
        $this->generator = $generator;
    }

    /**
     * Validates that the given billing information is valid
     *
     * @param Request $request
     *
     * @return JsonResponse
     */
    public function validateBillingInformation(Request $request)
    {
        $this->validate($request, $this->billing_information_rules);

        return Response::json([
            'message' => 'ok',
        ], 200);
    }

    /**
     * Processes an Order
     *
     * @param Request $request
     *
     * @return Response
     */
    public function processOrder(Request $request) : JsonResponse
    {
        $this->validate($request, array_merge(
            $this->billing_information_rules,
            [
                'payment_token' => ['required'],
            ]
        ));

        DB::beginTransaction();

        try {
            $billing = $this->applyBillingInformation($request);

            $order = $this->generator->createFromBasket();

            $order->addBillingInformation($billing);

            $order->update(['notes' => $request->get('notes')]);
        } catch (OrderGenerationException $e) {
            DB::rollback();

            Log::error($e);

            $code = $e->getCode();
            if (empty($code)) {
                $code = 500;
            }

            return Response::json([
                'message' => $e->getMessage()
            ], $code);
        } catch (Exception $e) {
            DB::rollback();

            Log::error($e);

            return Response::json([
                'message' => 'Something has gone wrong putting your order together. Please try again',
            ], 500);
        }

        try {
            $charge = $this->payment_gateway->charge(
                $request->get('payment_token'),
                $order->total
            );

            $order->update([
                'payment_method' => $charge->gateway,
                'payment_token' => $charge->id,
                'status' => $charge->status,
            ]);

            DB::commit();
        } catch (PaymentGatewayException $e) {
            DB::rollback();

            Log::error($e);

            $code = $e->getCode();
            if (empty($code)) {
                $code = 500;
            }

            return Response::json([
                'message' => 'There was a problem attempting to make the '
                    . 'payment. Please check your details and try again'
            ], $code);
        } catch (Exception $e) {
            DB::rollback();

            Log::error($e);

            $code = $e->getCode();
            if (empty($code)) {
                $code = 500;
            }

            return Response::json([
                'message' => 'There was a problem fulfilling this request. Please try again later'
            ], $code);
        }

        App::make(Basket::class)->emptyBasket();

        foreach (config('enso.gyms.actions.checkout.process_order.after', []) as $action) {
            $response = (new $action($order))->run();

            if ($response instanceof Response) {
                return $response;
            }
        }

        return Response::json([
            'message' => 'Your order has been taken',
            'order' => $order->toArray()
        ], 200);
    }

    /**
     * Gets or create the billing Model for the given user and populates it with
     * form data.
     *
     * @param Request $request
     * @param User    $user
     *
     * @return array
     */
    protected function applyBillingInformation(Request $request) : array
    {
        $billing_information = [
            'billing_name' => $request->get('billing_name'),
            'billing_company' => $request->get('billing_company'),
            'billing_email' => $request->get('billing_email'),
            'billing_phone' => $request->get('billing_phone'),
            'billing_address_1' => $request->get('billing_address_1'),
            'billing_address_2' => $request->get('billing_address_2'),
            'billing_city' => $request->get('billing_city'),
            'billing_state' => $request->get('billing_state'),
            'billing_postcode' => $request->get('billing_postcode'),
            'billing_country' => $request->get('billing_country'),
        ];

        if (Auth::check()) {
            $user = Auth::user();

            $billing = $user->billingInformation ?? Billing::create([
                'user_id' => $user->getKey()
            ]);

            $billing->update($billing_information);
        }


        return $billing_information;
    }
}
