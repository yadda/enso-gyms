<?php

namespace Yadda\Enso\Gyms\Controllers\Admin;

use Alert;
use Exception;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Redirect;
use Yadda\Enso\Crud\Controller;
use Yadda\Enso\Gyms\Contracts\MembershipController as MembershipControllerContract;
use Yadda\Enso\Gyms\MindBody\Updaters\Membership as MembershipUpdater;
use Yadda\Enso\Gyms\Traits\CanPublishItems;

class MembershipController extends Controller implements MembershipControllerContract
{
    use CanPublishItems;

    /**
     * Imports Instuctors (Staff) from MindBody  api.
     *
     * @return Redirect
     */
    public function import()
    {
        try {
            $memberships = App::make(MembershipUpdater::class)->fetch();

            if ($memberships->count()) {
                Alert::success($memberships->count() . ' Memberships listed on MindBody');
            } else {
                Alert::warning('No Memberships found');
            }
        } catch (Exception $e) {
            Log::error($e);

            Alert::error('Something went wrong fetching Memberships');
        }

        return Redirect::route('admin.gyms.memberships.index');
    }
}
