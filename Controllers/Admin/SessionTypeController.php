<?php

namespace Yadda\Enso\Gyms\Controllers\Admin;

use Alert;
use Exception;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Redirect;
use Yadda\Enso\Crud\Controller;
use Yadda\Enso\Gyms\Contracts\SessionTypeController as SessionTypeControllerContract;
use Yadda\Enso\Gyms\MindBody\Facades\MindBody;
use Yadda\Enso\Gyms\MindBody\Updaters\SessionType as SessionTypeHandler;
use Yadda\Enso\Gyms\Traits\CanPublishItems;

class SessionTypeController extends Controller implements SessionTypeControllerContract
{
    use CanPublishItems;

    /**
     * Imports Session Types from MindBody  api.
     *
     * @return Redirect
     */
    public function import()
    {
        try {
            $session_types = App::make(SessionTypeHandler::class)->fetch();

            if ($session_types->count()) {
                Alert::success($session_types->count() . ' SessionTypes listed on MindBody');
            } else {
                Alert::warning('No SessionTypes found');
            }
        } catch (Exception $e) {
            Log::error($e);

            Alert::error('Something went wrong fetching SessionTypes');
        }

        return Redirect::route('admin.gyms.session-types.index');
    }
}
