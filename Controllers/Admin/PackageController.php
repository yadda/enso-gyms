<?php

namespace Yadda\Enso\Gyms\Controllers\Admin;

use Alert;
use Exception;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Redirect;
use Yadda\Enso\Crud\Controller;
use Yadda\Enso\Gyms\Contracts\PackageController as PackageControllerContract;
use Yadda\Enso\Gyms\MindBody\Updaters\Package as PackageHandler;
use Yadda\Enso\Gyms\Traits\CanPublishItems;

class PackageController extends Controller implements PackageControllerContract
{
    use CanPublishItems;

    /**
     * Imports Packages from MindBody  api.
     *
     * @return Redirect
     */
    public function import()
    {
        try {
            $packages = App::make(PackageHandler::class)->fetch();

            if ($packages->count()) {
                Alert::success($packages->count() . ' Packages listed on MindBody');
            } else {
                Alert::warning('No Packages found');
            }
        } catch (Exception $e) {
            Log::error($e);

            Alert::error('Something went wrong fetching Packages');
        }

        return Redirect::route('admin.gyms.packages.index');
    }
}
