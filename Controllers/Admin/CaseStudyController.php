<?php

namespace Yadda\Enso\Gyms\Controllers\Admin;

use Yadda\Enso\Crud\Controller;
use Yadda\Enso\Gyms\Contracts\CaseStudyController as CaseStudyControllerContract;
use Yadda\Enso\Gyms\Traits\CanPublishItems;

class CaseStudyController extends Controller implements CaseStudyControllerContract
{
    use CanPublishItems;
}
