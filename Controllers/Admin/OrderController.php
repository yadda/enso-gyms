<?php

namespace Yadda\Enso\Gyms\Controllers\Admin;

use Yadda\Enso\Crud\Controller;
use Yadda\Enso\Gyms\Contracts\OrderController as OrderControllerContract;

class OrderController extends Controller implements OrderControllerContract
{
    //
}
