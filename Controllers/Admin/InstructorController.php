<?php

namespace Yadda\Enso\Gyms\Controllers\Admin;

use Alert;
use Exception;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Redirect;
use Yadda\Enso\Crud\Controller;
use Yadda\Enso\Gyms\Contracts\InstructorController as InstructorControllerContract;
use Yadda\Enso\Gyms\MindBody\Updaters\Instructor as InstructorUpdater;
use Yadda\Enso\Gyms\Traits\CanPublishItems;

class InstructorController extends Controller implements InstructorControllerContract
{
    use CanPublishItems;

    /**
     * Imports Instuctors (Staff) from MindBody  api.
     *
     * @return Redirect
     */
    public function import()
    {
        try {
            $instructors = App::make(InstructorUpdater::class)->fetch();

            if ($instructors->count()) {
                Alert::success($instructors->count() . ' Instructors listed on MindBody');
            } else {
                Alert::warning('No Instructors found');
            }
        } catch (Exception $e) {
            Log::error($e);

            Alert::error('Something went wrong fetching Instructors');
        }

        return Redirect::route('admin.gyms.instructors.index');
    }
}
