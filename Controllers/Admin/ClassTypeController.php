<?php

namespace Yadda\Enso\Gyms\Controllers\Admin;

use Alert;
use Exception;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Redirect;
use Yadda\Enso\Crud\Controller;
use Yadda\Enso\Gyms\Contracts\ClassTypeController as ClassTypeControllerContract;
use Yadda\Enso\Gyms\MindBody\Facades\MindBody;
use Yadda\Enso\Gyms\MindBody\Updaters\ClassType as ClassTypeUpdater;
use Yadda\Enso\Gyms\Traits\CanPublishItems;

class ClassTypeController extends Controller implements ClassTypeControllerContract
{
    use CanPublishItems;

    /**
     * Imports ClassTypes from MindBody  api.
     *
     * @return Redirect
     */
    public function import()
    {
        try {
            $class_types = App::make(ClassTypeUpdater::class)->fetch();

            if ($class_types->count()) {
                Alert::success($class_types->count() . ' ClassTypes listed on MindBody');
            } else {
                Alert::warning('No ClassTypes found');
            }
        } catch (Exception $e) {
            Log::error($e);

            Alert::error('Something went wrong fetching ClassTypes');
        }

        return Redirect::route('admin.gyms.class-types.index');
    }
}
