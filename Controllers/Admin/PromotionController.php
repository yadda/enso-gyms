<?php

namespace Yadda\Enso\Gyms\Controllers\Admin;

use Eventy;
use Illuminate\Validation\Rule;
use Yadda\Enso\Crud\Controller;
use Yadda\Enso\Gyms\Contracts\PromotionController as PromotionControllerInterface;
use Yadda\Enso\Gyms\Traits\CanPublishItems;

class PromotionController extends Controller implements PromotionControllerInterface
{
    use CanPublishItems;

    /**
     * Configures the hooks and filters for this controller
     *
     * @return void
     */
    protected function configureHooks()
    {
        Eventy::addFilter('crud.update.rules', function ($rules, $id) {
            $rules['main.code'] = [
                'required',
                Rule::unique('enso_gyms_promotions', 'code')->ignore($id)
            ];
            return $rules;
        }, 10, 2);
    }
}
