<?php

namespace Yadda\Enso\Gyms\Controllers\Admin;

use Alert;
use Exception;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Redirect;
use Yadda\Enso\Crud\Controller;
use Yadda\Enso\Gyms\Contracts\GymController as GymControllerContract;
use Yadda\Enso\Gyms\MindBody\Facades\MindBody;
use Yadda\Enso\Gyms\MindBody\Updaters\Gym as GymUpdater;
use Yadda\Enso\Gyms\Traits\CanPublishItems;

class GymController extends Controller implements GymControllerContract
{
    use CanPublishItems;

    /**
     * Imports Gyms (Locations) from MindBody  api.
     *
     * @return Redirect
     */
    public function import()
    {
        try {
            $gyms = App::make(GymUpdater::class)->fetch();

            if ($gyms->count()) {
                Alert::success($gyms->count() . ' Gyms listed on MindBody');
            } else {
                Alert::warning('No Gyms found');
            }
        } catch (Exception $e) {
            Log::error($e);

            Alert::error('Something went wrong fetching Gyms');
        }

        return Redirect::route('admin.gyms.gyms.index');
    }

    /**
     * Pulls a list of Programs from the MindBody API. This can be used to
     * populate a setting to specify which Programs to pull further information
     * from.
     *
     * @return View
     */
    public function getPrograms()
    {
        $class_programs = MindBody::v('5')->sites()->getPrograms();

        $appointment_programs = MindBody::v('5')->sites()->getPrograms([
            'ScheduleType' => 'Appointment',
        ]);

        $crud = $this->getConfig();

        return view('enso-crud::gyms.programs', compact(
            'crud',
            'class_programs',
            'appointment_programs'
        ));
    }
}
