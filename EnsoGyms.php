<?php

namespace Yadda\Enso\Gyms;

use Illuminate\Contracts\Foundation\Application;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Route;
use ReflectionFunction;
use Yadda\Enso\Facades\EnsoCrud;
use Yadda\Enso\Facades\EnsoMenu;
use Yadda\Enso\Gyms\Contracts\BasketController;
use Yadda\Enso\Gyms\Contracts\CaseStudyController;
use Yadda\Enso\Gyms\Contracts\CheckoutController;
use Yadda\Enso\Gyms\Contracts\ClassTypeController;
use Yadda\Enso\Gyms\Contracts\GymController;
use Yadda\Enso\Gyms\Contracts\InstructorController;
use Yadda\Enso\Gyms\Contracts\MembershipController;
use Yadda\Enso\Gyms\Contracts\OrderController;
use Yadda\Enso\Gyms\Contracts\PackageController;
use Yadda\Enso\Gyms\Contracts\PromotionController;
use Yadda\Enso\Gyms\Contracts\SessionTypeController;
use Yadda\Enso\Settings\Facades\EnsoSettings;

class EnsoGyms
{
    /**
     * Adds Menu Items to Enso at the given position
     *
     * @return void
     */
    public function addMenu()
    {
        EnsoMenu::addItem(Config::get('enso.gyms.menu', []));
    }

    /**
     * Gets program ids for the Mindbody installation, checking the Setting
     * first, then falling back to the Config file.
     *
     * It then splits a comma separated list into an array, if required.
     *
     * @return array
     */
    public function getProgramIds()
    {
        $settings_value = EnsoSettings::get('program-ids', Config::get('enso.gyms.mindbody.programs', ''));

        return array_reduce(
            explode(',', $settings_value),
            function ($carry, $program_id) {
                $trimmed = trim($program_id);

                if (strlen(($trimmed))) {
                    $carry[] = $trimmed;
                }

                return $carry;
            },
            []
        );
    }

    /**
     * Adds routes for Enso Gyms
     *
     * @return void
     */
    public function adminRoutes()
    {
        Route::group(
            ['prefix' => 'admin'],
            function () {
                $this->addCaseStudyRoutes();
                $this->addInstructorRoutes();
                $this->addGymRoutes();
                $this->addClassTypeRoutes();
                $this->addSessionTypeRoutes();
                $this->addPackageRoutes();
                $this->addOrderRoutes();
                $this->addPromotionRoutes();
                $this->addMembershipRoutes();
            }
        );
    }

    /**
     * Adds Routes that should be guest-accessible.
     *
     * @return void
     */
    public function guestRoutes()
    {
        $this->addBasketRoutes();
        $this->addCheckoutRoutes();
    }

    /**
     * Adds CMS routes specific to the CaseStudy data type
     *
     * @return void
     */
    protected function addCaseStudyRoutes()
    {
        if (Config::get('enso.gyms.routes.casestudy', false)) {
            $case_study_controller = $this->getBoundConcreteClass(
                CaseStudyController::class
            );

            Route::post(
                'gyms/case-studies/publish',
                '\\' . $case_study_controller . '@publish'
            )->name('admin.gyms.case-studies.publish');

            Route::post(
                'gyms/case-studies/unpublish',
                '\\' . $case_study_controller . '@unpublish'
            )->name('admin.gyms.case-studies.unpublish');

            EnsoCrud::routes(
                'gyms/case-studies',
                '\\' . $case_study_controller,
                'admin.gyms.case-studies'
            );
        }
    }

    /**
     * Adds CMS routes specific to the Gym data type
     *
     * @return void
     */
    protected function addInstructorRoutes()
    {
        $instructor_controller = $this->getBoundConcreteClass(InstructorController::class);

        Route::get(
            'gyms/instructors/import',
            '\\' . $instructor_controller . '@import'
        )->name('admin.gyms.instructors.import');

        Route::post(
            'gyms/instructors/publish',
            '\\' . $instructor_controller . '@publish'
        )->name('admin.gyms.instructors.publish');

        Route::post(
            'gyms/instructors/unpublish',
            '\\' . $instructor_controller . '@unpublish'
        )->name('admin.gyms.instructors.unpublish');

        EnsoCrud::routes(
            'gyms/instructors',
            '\\' . $instructor_controller,
            'admin.gyms.instructors'
        );
    }

    /**
     * Adds CMS routes specific to the Gym data type
     *
     * @return void
     */
    protected function addGymRoutes()
    {
        $gym_controller = $this->getBoundConcreteClass(GymController::class);

        Route::get(
            'gyms/gyms/import',
            '\\' . $gym_controller . '@import'
        )->name('admin.gyms.gyms.import');

        Route::get(
            'gyms/gyms/get-programs',
            '\\' . $gym_controller . '@getPrograms'
        )->name('admin.gyms.gyms.get-programs');

        Route::post(
            'gyms/gyms/publish',
            '\\' . $gym_controller . '@publish'
        )->name('admin.gyms.gyms.publish');

        Route::post(
            'gyms/gyms/unpublish',
            '\\' . $gym_controller . '@unpublish'
        )->name('admin.gyms.gyms.unpublish');

        EnsoCrud::routes(
            'gyms/gyms',
            '\\' . $gym_controller,
            'admin.gyms.gyms'
        );
    }

    /**
     * Adds CMS routes specific to the Class Type data type
     *
     * @return void
     */
    protected function addClassTypeRoutes()
    {
        $class_type_controller = $this->getBoundConcreteClass(
            ClassTypeController::class
        );

        Route::get(
            'gyms/class-types/import',
            '\\' . $class_type_controller . '@import'
        )->name('admin.gyms.class-types.import');

        Route::post(
            'gyms/class-types/publish',
            '\\' . $class_type_controller . '@publish'
        )->name('admin.gyms.class-types.publish');

        Route::post(
            'gyms/class-types/unpublish',
            '\\' . $class_type_controller . '@unpublish'
        )->name('admin.gyms.class-types.unpublish');

        EnsoCrud::routes(
            'gyms/class-types',
            '\\' . $class_type_controller,
            'admin.gyms.class-types'
        );
    }

    /**
     * Adds CMS routes specific to the Session Type data type
     *
     * @return void
     */
    protected function addSessionTypeRoutes()
    {
        $session_type_controller = $this->getBoundConcreteClass(
            SessionTypeController::class
        );

        Route::get(
            'gyms/session-types/import',
            '\\' . $session_type_controller . '@import'
        )->name('admin.gyms.session-types.import');

        Route::post(
            'gyms/session-types/publish',
            '\\' . $session_type_controller . '@publish'
        )->name('admin.gyms.session-types.publish');

        Route::post(
            'gyms/session-types/unpublish',
            '\\' . $session_type_controller . '@unpublish'
        )->name('admin.gyms.session-types.unpublish');

        EnsoCrud::routes(
            'gyms/session-types',
            '\\' . $session_type_controller,
            'admin.gyms.session-types'
        );
    }

    /**
     * Adds CMS routes specific to the Package data type
     *
     * @return void
     */
    protected function addPackageRoutes()
    {
        $package_controller = $this->getBoundConcreteClass(
            PackageController::class
        );

        Route::get(
            'gyms/packages/import',
            '\\' . $package_controller . '@import'
        )->name('admin.gyms.packages.import');

        Route::post(
            'gyms/packages/publish',
            '\\' . $package_controller . '@publish'
        )->name('admin.gyms.packages.publish');

        Route::post(
            'gyms/packages/unpublish',
            '\\' . $package_controller . '@unpublish'
        )->name('admin.gyms.packages.unpublish');

        EnsoCrud::routes(
            'gyms/packages',
            '\\' . $package_controller,
            'admin.gyms.packages'
        );
    }

    /**
     * Adds CMS routes specific to the Promotion data type
     *
     * @return void
     */
    protected function addPromotionRoutes()
    {
        if (Config::get('enso.gyms.routes.promotion', false)) {
            $promotion_controller = $this->getBoundConcreteClass(
                PromotionController::class
            );

            Route::post(
                'gyms/promotions/publish',
                '\\' . $promotion_controller . '@publish'
            )->name('admin.gyms.promotions.publish');

            Route::post(
                'gyms/promotions/unpublish',
                '\\' . $promotion_controller . '@unpublish'
            )->name('admin.gyms.promotions.unpublish');

            EnsoCrud::routes(
                'gyms/promotions',
                '\\' . $promotion_controller,
                'admin.gyms.promotions'
            );
        }
    }

    /**
     * Adds CMS routes specific to the Order data type
     *
     * @return void
     */
    protected function addOrderRoutes()
    {
        $order_controller = $this->getBoundConcreteClass(
            OrderController::class
        );

        EnsoCrud::routes(
            'gyms/orders',
            '\\' . $order_controller,
            'admin.gyms.orders'
        );
    }

    /**
     * Adds Json routes for the Basket
     *
     * @return void
     */
    protected function addBasketRoutes()
    {
        $basket_controller = $this->getBoundConcreteClass(
            BasketController::class
        );

        Route::group(['prefix' => 'json', 'middleware' => 'api'], function () use ($basket_controller) {
            Route::post(
                'basket/promotions/edit',
                '\\' . $basket_controller . '@setPromotion'
            )->name('json.basket.promotions.update');

            Route::post(
                'basket/promotions/delete',
                '\\' . $basket_controller . '@removePromotion'
            )->name('json.basket.promotions.remove');

            Route::post(
                'basket/packages/edit',
                '\\' . $basket_controller . '@setPackage'
            )->name('json.basket.packages.update');

            Route::post(
                'basket/packages/delete',
                '\\' . $basket_controller . '@removePackage'
            )->name('json.basket.packages.remove');

            Route::get(
                'basket',
                '\\' . $basket_controller . '@getBasket'
            )->name('json.basket');
        });
    }

    /**
     * Adds Json routes for the Checkout
     *
     * @return void
     */
    protected function addCheckoutRoutes()
    {
        $checkout_controller = $this->getBoundConcreteClass(
            CheckoutController::class
        );

        Route::group(['prefix' => 'json', 'middleware' => 'api'], function () use ($checkout_controller) {
            Route::post(
                'checkout/validate-billing',
                '\\' . $checkout_controller . '@validateBillingInformation'
            )->name('json.checkout.validate-billing');

            Route::post(
                'checkout/charge',
                '\\' . $checkout_controller . '@processOrder'
            )->name('json.checkout.process-order');
        });
    }

    protected function addMembershipRoutes()
    {
        if (Config::get('enso.gyms.routes.membership', false)) {
            $membership_controller = $this->getBoundConcreteClass(
                MembershipController::class
            );

            Route::post(
                'gyms/memberships/publish',
                '\\' . $membership_controller . '@publish'
            )->name('admin.gyms.memberships.publish');

            Route::post(
                'gyms/memberships/unpublish',
                '\\' . $membership_controller . '@unpublish'
            )->name('admin.gyms.memberships.unpublish');

            Route::get(
                'admin/memberships/import',
                '\\' . $membership_controller . '@import'
            )->name('admin.gyms.memberships.import');

            EnsoCrud::routes(
                'gyms/memberships',
                '\\' . $membership_controller,
                'admin.gyms.memberships'
            );
        }
    }

    /**
     * Gets the concrete class bound to the given contract
     *
     * @param string $contract_class
     *
     * @return string
     */
    protected function getBoundConcreteClass($contract_class)
    {
        if (App::bound($contract_class)) {
            return (new ReflectionFunction(
                App::getBindings()[$contract_class]['concrete']
            ))->getStaticVariables()['concrete'];
        }

        return $contract_class;
    }
}
