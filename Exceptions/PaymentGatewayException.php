<?php

namespace Yadda\Enso\Gyms\Exceptions;

use Exception;

class PaymentGatewayException extends Exception
{
    /**
     * Returns an exception with a message about the basket being empty
     *
     * @return PaymentGatewayException
     */
    public static function failedCharge($exception = null) : PaymentGatewayException
    {
        return new static('Unable to charge the give payment source', 502, $exception);
    }

    /**
     * Returns an exception with a message about the basket being empty
     *
     * @return PaymentGatewayException
     */
    public static function unknownError($exception = null) : PaymentGatewayException
    {
        return new static('Unable to complete payment right now. Please try again later.', 500, $exception);
    }
}
