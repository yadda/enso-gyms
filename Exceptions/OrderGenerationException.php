<?php

namespace Yadda\Enso\Gyms\Exceptions;

use Exception;

class OrderGenerationException extends Exception
{
    /**
     * Returns an exception with a message about the basket being empty
     *
     * @return OrderGenerationException
     */
    public static function emptyBasket(Exception $exception = null): OrderGenerationException
    {
        return new static('Cannot create an order from an empty Basket', 400, $exception);
    }

    /**
     * Returns an exception with a message about authentication state
     *
     * @return OrderGenerationException
     */
    public static function noUser(Exception $exception = null): OrderGenerationException
    {
        return new static('You must be logged in to create an order', 401, $exception);
    }

    /**
     * Returns an exception with a message about failed create state
     *
     * @return OrderGenerationException
     */
    public static function creationFailed(Exception $exception = null): OrderGenerationException
    {
        return new static('Something went wrong creating your order', 500, $exception);
    }
}
