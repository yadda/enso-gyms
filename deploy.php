<?php

namespace Deployer;

inventory('hosts.yml');

task('tag_new_version', function () {
    // phpcs:ignore
    $semver_regex = '/\bv(?:0|[1-9]\d*)\.(?:0|[1-9]\d*)\.(?:0|[1-9]\d*)(?:-[\da-z-]+(?:\.[\da-z-]+)*)?(?:\+[\da-z-]+(?:\.[\da-z-]+)*)?\b/i';
    $matches = [];

    $version = ask('What version is this? e.g. v0.2.1');

    preg_match($semver_regex, $version, $matches);
    $is_semver = count($matches) > 0;

    if (!$is_semver) {
        writeln('<error>That version is not a semver version.</error>');
        exit;
    }

    runLocally('git tag ' . $version);
    runLocally('git push origin ' . $version);
});

task('rebuild_satis', function () {
    cd('{{ deploy_path }}');
    run('php bin/satis build ../satis.json ../web -n');
});

desc('Release a new version.');
task('new', [
    'tag_new_version',
    'rebuild_satis',
    'success',
]);

desc('Show a success message.');
task('success', function () {
    writeln('<info>Success!</info>');
});
