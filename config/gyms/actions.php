<?php

return [
    'checkout' => [
        'process_order' => [
            /*
            |------------------------------------------------------------------
            | Process Complete Actions
            |------------------------------------------------------------------
            |
            | These actions will be run when an order is processed succesfully.
            | The constructor will be passed the order. The run method may
            | return a response, if so this response will be passed directly
            | back to the user.
            |
            */
            'after' => [
                \Yadda\Enso\Gyms\Actions\Checkout\SendUserEmail::class,
                \Yadda\Enso\Gyms\Actions\Checkout\SendAdminEmail::class,
            ],
        ],
    ],
];
