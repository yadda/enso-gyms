<?php

return [

    /*
    |----------------------------------------------------------------------
    | This should be set to true if we are developing the site and using
    | the sandbox API.
    |----------------------------------------------------------------------
    */
    'sandbox' => env('MINDBODY_SANDBOX_MODE', false),

    /*
    |----------------------------------------------------------------------
    | Provide Test=true when making shopping cart requests
    |----------------------------------------------------------------------
    */
    'cart_test_mode' => env('MINDBODY_CART_TEST_MODE', false),

    'api_key' => env('MINDBODY_API_KEY'),
    'site_id' => env('MINDBODY_SITE_ID'),
    'programs' => env('MINDBODY_PROGRAMS'),
    'source' => [
        'name' => env('MINDBODY_SOURCE_NAME'),
        'password' => env('MINDBODY_SOURCE_PASSWORD'),
        'site_ids' => explode(',', env('MINDBODY_SITE_IDS')),
    ],
    'user' => [
        'username' => env('MINDBODY_USERNAME'),
        'password' => env('MINDBODY_USER_PASSWORD'),
        'site_ids' => explode(',', env('MINDBODY_SITE_IDS')),
    ],
    'app_name' => env('MINDBODY_APP_NAME'),
    'currency' => env('MINDBODY_CURRENCY', 'THB'),
    'locale' => env('MINDBODY_LOCALE', 'th-TH'),
    'money_min_fraction_digits' => env('MINDBODY_MONEY_MIN_FRACTION_DIGITS', 0),
];
