<?php

return [
    'route' => ['admin.gyms.gyms.index'],
    'label' => 'Gyms',
    'icon' => 'fa fa-file-o',
    'order' => 41,
    'items' => [
        [
            'route' => ['admin.gyms.instructors.index'],
            'label' => 'Instructors',
            'icon' => 'fa fa-file-o',
            'order' => 1,
        ],
        [
            'route' => ['admin.gyms.gyms.index'],
            'label' => 'Gyms',
            'icon' => 'fa fa-file-o',
            'order' => 2,
        ],
        [
            'route' => ['admin.gyms.case-studies.index'],
            'label' => 'Case Studies',
            'icon' => 'fa fa-file-o',
            'order' => 3,
        ],
        [
            'route' => ['admin.gyms.class-types.index'],
            'label' => 'Class Types',
            'icon' => 'fa fa-file-o',
            'order' => 4,
        ],
        [
            'route' => ['admin.gyms.session-types.index'],
            'label' => 'Session Types',
            'icon' => 'fa fa-file-o',
            'order' => 5,
        ],
        [
            'route' => ['admin.gyms.packages.index'],
            'label' => 'Packages',
            'icon' => 'fa fa-file-o',
            'order' => 6,
        ],
        [
            'route' => ['admin.gyms.orders.index'],
            'label' => 'Orders',
            'icon' => 'fa fa-file-o',
            'order' => 7,
        ],
        [
            'route' => ['admin.gyms.promotions.index'],
            'label' => 'Promotions',
            'icon' => 'fa fa-file-o',
            'order' => 8,
        ],
        [
            'route' => ['admin.gyms.memberships.index'],
            'label' => 'Memberships',
            'icon' => 'fa fa-file-o',
            'order' => 8,
        ],
    ],
];
