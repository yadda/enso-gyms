<?php

return [

    'casestudy' => [
        'controller'    => '\Yadda\Enso\Gyms\Controllers\CaseStudyController',
        'config'        => '\Yadda\Enso\Gyms\Crud\CaseStudy',
        'model'         => '\Yadda\Enso\Gyms\Models\CaseStudy',
    ],

    'instructor' => [
        'controller'    => '\Yadda\Enso\Gyms\Controllers\InstructorController',
        'config'        => '\Yadda\Enso\Gyms\Crud\Instructor',
        'model'         => '\Yadda\Enso\Gyms\Models\Instructor',
    ],

    'gym' => [
        'controller'    => '\Yadda\Enso\Gyms\Controllers\GymController',
        'config'        => '\Yadda\Enso\Gyms\Crud\Gym',
        'model'         => '\Yadda\Enso\Gyms\Models\Gym',
    ],

    'classtype' => [
        'controller'    => '\Yadda\Enso\Gyms\Controllers\ClassTypeController',
        'config'        => '\Yadda\Enso\Gyms\Crud\ClassType',
        'model'         => '\Yadda\Enso\Gyms\Models\ClassType',
    ],

    'sessiontype' => [
        'controller'    => '\Yadda\Enso\Gyms\Controllers\SessionTypeController',
        'config'        => '\Yadda\Enso\Gyms\Crud\SessionType',
        'model'         => '\Yadda\Enso\Gyms\Models\SessionType',
    ],

    'package' => [
        'controller'    => '\Yadda\Enso\Gyms\Controllers\PackageController',
        'config'        => '\Yadda\Enso\Gyms\Crud\Package',
        'model'         => '\Yadda\Enso\Gyms\Models\Package',
    ],

    'order' => [
        'controller'    => '\Yadda\Enso\Gyms\Controllers\OrderController',
        'config'        => '\Yadda\Enso\Gyms\Crud\Order',
        'model'         => '\Yadda\Enso\Gyms\Models\Order',
    ],

    'promotion' => [
        'controller'    => '\Yadda\Enso\Gyms\Controllers\PromotionController',
        'config'        => '\Yadda\Enso\Gyms\Crud\Promotion',
        'model'         => '\Yadda\Enso\Gyms\Models\Promotion',
    ],

    'membership' => [
        'controller'    => '\Yadda\Enso\Gyms\Controllers\Admin\MembershipController',
        'config'        => '\Yadda\Enso\Gyms\Crud\Membership',
        'model'         => '\Yadda\Enso\Gyms\Membership',
    ],
];
