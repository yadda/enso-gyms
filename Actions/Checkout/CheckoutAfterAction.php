<?php

namespace Yadda\Enso\Gyms\Actions\Checkout;

use Yadda\Enso\Gyms\Contracts\Action;
use Yadda\Enso\Gyms\Contracts\Order;

abstract class CheckoutAfterAction implements Action
{
    protected $order;

    public function __construct(Order $order)
    {
        $this->order = $order;
    }
}
