<?php

namespace Yadda\Enso\Gyms\Actions\Checkout;

use Illuminate\Support\Facades\Mail;
use Yadda\Enso\Gyms\Mail\RecipientOrderCompleted;

class SendUserEmail extends CheckoutAfterAction
{
    public function run()
    {
        Mail::to($this->order->billing_email)->queue(
            (new RecipientOrderCompleted($this->order))->onQueue('email')
        );
    }
}
