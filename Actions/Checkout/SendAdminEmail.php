<?php

namespace Yadda\Enso\Gyms\Actions\Checkout;

use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Mail;
use Yadda\Enso\Gyms\Mail\AdminOrderCompleted;
use Yadda\Enso\Settings\Facades\EnsoSettings;

class SendAdminEmail extends CheckoutAfterAction
{
    public function run()
    {
        $admin_email = EnsoSettings::get('administrator-email', Config::get('mail.from.address'));

        if ($admin_email) {
            Mail::to($admin_email)->queue(
                (new AdminOrderCompleted($this->order))->onQueue('email')
            );
        }
    }
}
