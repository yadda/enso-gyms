<?php

namespace Yadda\Enso\Gyms\Contracts;

interface Action
{
    public function run();
}
