# Installation

1. Add Service Provider to `config/app.php`:

`Yadda\Enso\Gyms\GymServiceProvider::class,`

1. Publish Enso Gyms vendor files:

`enso artisan vendor:publish --tag=enso-gyms`

1. Register a PaymentGateway in a Service Provider

`$this->app->bind(PaymentGateway::class, OmisePaymentGateway::class);`
