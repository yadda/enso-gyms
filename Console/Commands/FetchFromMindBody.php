<?php

namespace Yadda\Enso\Gyms\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\App;
use Yadda\Enso\Gyms\MindBody\Updaters\ClassType;
use Yadda\Enso\Gyms\MindBody\Updaters\Gym;
use Yadda\Enso\Gyms\MindBody\Updaters\Instructor;
use Yadda\Enso\Gyms\MindBody\Updaters\Package;
use Yadda\Enso\Gyms\MindBody\Updaters\SessionType;

class FetchFromMindBody extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'mindbody:fetchupdates
                            {--F|features=all : List of features to fetch}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Fetches updates to local data-types from MindBody';

    protected $handlers = [
        'classtype' => ClassType::class,
        'instructor' => Instructor::class,
        'gym' => Gym::class,
        'package' => Package::class,
        'sessiontype' => SessionType::class,
    ];

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $features = $this->option('features');

        if ($features !== 'all') {
            $features = explode(',', $features);
        }

        foreach ($this->handlers as $name => $handler) {
            if ($features === 'all' || in_array($name, $features)) {
                $items = App::make($handler)->fetch();

                $this->info(ucfirst($name) ." - {$items->count()} items updated");
            }
        }

        $this->info('Done!');
    }
}
