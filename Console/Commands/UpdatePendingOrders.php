<?php

namespace Yadda\Enso\Gyms\Console\Commands;

use Exception;
use Illuminate\Console\Command;
use Yadda\Enso\Gyms\Checkout\OmisePaymentGateway;
use Yadda\Enso\Gyms\Contracts\Order;

class UpdatePendingOrders extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'omise:updatepending';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Queries Omise for updates to Orders with pending charges';

    /**
     * Payment Gateway to use to query Orders
     *
     * @var OmisePaymentGateway
     */
    public $gateway;

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct(OmisePaymentGateway $gateway)
    {
        parent::__construct();

        $this->payment_gateway = $gateway;
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        resolve(Order::class)->where('payment_method', 'omise')
            ->where('status', 'pending')
            ->chunk(50, function ($orders) {
                foreach ($orders as $order) {
                    try {
                        $charge = $this->payment_gateway->getCharge($order->payment_token);

                        $order->update([
                            'status' => $charge->status,
                        ]);
                    } catch (Exception $e) {
                        Log::error($e);
                    }
                }
            });
    }
}
