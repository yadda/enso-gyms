<?php

namespace Yadda\Enso\Gyms\MindBody;

use Yadda\Enso\Gyms\MindBody\Interfaces\MindBodyInterface;
use Yadda\Enso\Gyms\MindBody\V5\MindBody as MindBodyV5;
use Yadda\Enso\Gyms\MindBody\V6\MindBody as MindBodyV6;

class MindBodyFactory
{
    protected $apis = [];

    /**
     * Gets a MindbodyInterface instance based on the passed version.
     *
     * @param string $version
     *
     * @return MindBodyInterface
     */
    public function v($version) : MindBodyInterface
    {
        if (!isset($this->apis[$version])) {
            switch ($version) {
                case '6':
                    $this->apis[$version] = new MindBodyV6;
                    break;
                case '5':
                default:
                    $this->apis[$version] = new MindBodyV5;
                    break;
            }
        }

        return $this->apis[$version];
    }
}
