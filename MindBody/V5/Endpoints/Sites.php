<?php

namespace Yadda\Enso\Gyms\MindBody\V5\Endpoints;

use Illuminate\Support\Arr;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Log;
use Yadda\Enso\Gyms\Facades\EnsoGyms;
use Yadda\Enso\Gyms\MindBody\DataTypes\Location;
use Yadda\Enso\Gyms\MindBody\DataTypes\Program;
use Yadda\Enso\Gyms\MindBody\DataTypes\SessionType;
use Yadda\Enso\Gyms\MindBody\V5\Endpoints\Endpoint;

class Sites extends Endpoint
{
    public function getSites($request_data = [], $return_object = false, $debug_errors = false)
    {
        $response = $this->api->GetSites($request_data, $return_object, $debug_errors);

        return $response;
    }

    /**
     * Gets a list of Programs.
     *
     * @param array $request_data
     * @param boolean $return_object
     * @param boolean $debug_errors
     *
     * @return Collection
     */
    public function getPrograms($request_data = [], $return_object = false, $debug_errors = false)
    {
        $response = $this->api->GetPrograms(
            array_merge(
                [
                    'ScheduleType' => 'DropIn',
                    'OnlineOnly' => false,
                ],
                $request_data
            ),
            $return_object,
            $debug_errors
        );

        return $this->collectData(
            Arr::get($response, 'GetProgramsResult.Programs.Program', [])
        )->map(function ($data) {
            return $this->mapProgram($data);
        });
    }

    /**
     * Gets the Session Types from MindBody
     *
     * @param array $request_data
     * @param boolean $return_object
     * @param boolean $debug_errors
     *
     * @return void
     */
    public function getSessionTypes($request_data = [], $return_object = false, $debug_errors = false)
    {
        $response = $this->api->GetSessionTypes(
            array_merge(
                [
                    'ProgramIDs' => EnsoGyms::getProgramIds(),
                    'OnlineOnly' => false,
                ],
                $request_data
            ),
            $return_object,
            $debug_errors
        );

        return $this->collectData(
            Arr::get($response, 'GetSessionTypesResult.SessionTypes.SessionType', [])
        )->map(function ($data) {
            return $this->mapSessionType($data);
        });
    }

    /**
     * Gets locations for the Mindbody site.
     *
     * @param array   $request_data
     * @param boolean $return_object
     * @param boolean $debug_errors
     *
     * @return Collection
     */
    public function getLocations($request_data = [], $return_object = false, $debug_errors = false)
    {
        $response = $this->api->GetLocations($request_data, $return_object, $debug_errors);

        if (!Arr::has($response, 'GetLocationsResult.Locations.Location')) {
            Log::error('Failed to get locations from Mindbody', [
                'response' => $response,
            ]);

            return new Collection;
        }

        $items = Arr::get($response, 'GetLocationsResult.Locations.Location', []);

        Log::info('Got ' . count($items) . ' locations from Mindbody.');

        return $this
            ->collectData($items)
            ->map(function ($data) {
                return $this->mapLocation($data);
            });
    }

    /**
     * Maps the response provided by this version's endpoint to a common format.
     *
     * @param array $data
     *
     * @return Program
     */
    public function mapProgram(array $data)
    {
        $program = new Program;

        $program->mindbody_id = Arr::get($data, 'ID');
        $program->name = Arr::get($data, 'Name');
        $program->type = Arr::get($data, 'ScheduleType');

        return $program;
    }

    /**
     * Maps the response provided by this version's endpoint to a common format.
     *
     * @param array $data
     *
     * @return Location
     */
    public function mapLocation(array $data)
    {
        $location = new Location;

        $location->mindbody_id = Arr::get($data, 'ID');
        $location->longitude = Arr::get($data, 'Longitude');
        $location->latitude = Arr::get($data, 'Latitude');
        $location->name = Arr::get($data, 'Name');
        $location->description = Arr::get($data, 'BusinessDescription');
        $location->address = Arr::get($data, 'Address');
        $location->address_2 = Arr::get($data, 'Address2');
        $location->city = Arr::get($data, 'City');
        $location->province = Arr::get($data, 'StateProvCode');
        $location->post_code = Arr::get($data, 'PostalCode');

        $location->phone = implode(' ', array_filter([
            Arr::get($data, 'PhoneExtension'),
            Arr::get($data, 'Phone')
        ]));

        return $location;
    }

    /**
     * Maps the response provided by this version's endpoint to a common format.
     *
     * @param array $data
     *
     * @return SessionType
     */
    public function mapSessionType(array $data)
    {
        $session_type = new SessionType;

        $session_type->mindbody_id = Arr::get($data, 'ID');
        $session_type->name = Arr::get($data, 'Name');

        return $session_type;
    }
}
