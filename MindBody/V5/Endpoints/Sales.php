<?php

namespace Yadda\Enso\Gyms\MindBody\V5\Endpoints;

use Illuminate\Support\Arr;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Log;
use Yadda\Enso\Gyms\Facades\EnsoGyms;
use Yadda\Enso\Gyms\MindBody\DataTypes\Service;
use Yadda\Enso\Gyms\MindBody\V5\Endpoints\Endpoint;

class Sales extends Endpoint
{
    /**
     * Gets the available Services from the MindBody Api
     *
     * @param array $request_data
     * @param boolean $return_object
     * @param boolean $debug_errors
     *
     * @return \Illuminate\Support\Collection
     */
    public function getServices($request_data = [], $return_object = false, $debug_errors = false)
    {
        $response = $this->api->GetServices(
            array_merge(
                [
                    'ProgramIDs' => EnsoGyms::getProgramIds(),
                    'LocationID' => 0,
                    'HideRelatedPrograms' => true
                ],
                $request_data
            ),
            $return_object,
            $debug_errors
        );

        if (!Arr::has($response, 'GetServicesResult.Services.Service')) {
            Log::error('Failed to get Mindbody services', [
                'response' => $response,
            ]);

            return new Collection();
        }

        $items = Arr::get($response, 'GetServicesResult.Services.Service', []);

        Log::info('Got ' . count($items) . ' services from Mindbody.');

        return $this
            ->collectData($items)
            ->map(function ($data) {
                return $this->mapService($data);
            });
    }

    /**
     * Maps service data from the endpoint into the common format
     *
     * @param array $data
     *
     * @return ClassDescription
     */
    public function mapService(array $data)
    {
        $service = new Service;

        $service->mindbody_id = Arr::get($data, 'ID');
        $service->name = Arr::get($data, 'Name');
        $service->price = intval(Arr::get($data, 'Price') * 100);
        $service->program_id = Arr::get($data, 'ProgramID');
        $service->class_count = Arr::get($data, 'Count');

        return $service;
    }
}
