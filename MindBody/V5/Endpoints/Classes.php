<?php

namespace Yadda\Enso\Gyms\MindBody\V5\Endpoints;

use Carbon\Carbon;
use Illuminate\Support\Arr;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Log;
use Yadda\Enso\Gyms\Facades\EnsoGyms;
use Yadda\Enso\Gyms\MindBody\DataTypes\ClassDescription;
use Yadda\Enso\Gyms\MindBody\DataTypes\ClassInstance;
use Yadda\Enso\Gyms\MindBody\Facades\MindBody;
use Yadda\Enso\Gyms\MindBody\V5\Endpoints\Endpoint;

class Classes extends Endpoint
{
    /**
     * Gets classes based on the arguments. Defaults to Today's classes in
     * the site's program ids.
     *
     * @param array $request_data
     * @param boolean $return_object
     * @param boolean $debug_errors
     * @return void
     */
    public function getClasses($request_data = [], $return_object = false, $debug_errors = false)
    {
        $response = $this->api->GetClasses(array_merge(
            ['ProgramIDs' => EnsoGyms::getProgramIds()],
            $request_data
        ), $return_object, $debug_errors);

        return $this->collectData(
            Arr::get($response, 'GetClassesResult.Classes.Class', [])
        )->map(function ($data) {
            return $this->mapClass($data);
        });
    }

    /**
     * WIP
     *
     * @param array $request_data
     * @param boolean $return_object
     * @param boolean $debug_errors
     * @return void
     */
    public function getClassSchedules($request_data = [], $return_object = false, $debug_errors = false)
    {
        $response = $this->api->GetClassSchedules($request_data, $return_object, $debug_errors);

        return $response;
    }

    /**
     * Gets class descriptions from the MindBody Api
     *
     * @param array   $request_data
     * @param boolean $return_object
     * @param boolean $debug_errors
     *
     * @return Collection
     */
    public function getClassDescriptions($request_data = [], $return_object = false, $debug_errors = false)
    {
        $response = $this->api->GetClassDescriptions(array_merge(
            ['ProgramIDs' => EnsoGyms::getProgramIds()],
            $request_data
        ), $return_object, $debug_errors);

        if (!Arr::has($response, 'GetClassDescriptionsResult.ClassDescriptions.ClassDescription')) {
            Log::error('Failed to get class description from Mindbody', [
                'response' => $response,
            ]);

            return new Collection;
        }

        $items = Arr::get($response, 'GetClassDescriptionsResult.ClassDescriptions.ClassDescription', []);

        Log::info('Got ' . count($items) . ' class descriptions from Mindbody');

        return $this
            ->collectData($items)
            ->map(function ($data) {
                return $this->mapClassDescription($data);
            });
    }

    /**
     * Maps class description data from the endpoint into the common format
     *
     * @param array $data
     *
     * @return ClassDescription
     */
    public function mapClassDescription(array $data)
    {
        $class_description = new ClassDescription;

        $class_description->mindbody_id = Arr::get($data, 'ID');
        $class_description->name = Arr::get($data, 'Name');
        $class_description->description = Arr::get($data, 'Description');

        $class_description->program = MindBody::v(5)->sites()->mapProgram(
            Arr::get($data, 'Program', [])
        );

        $class_description->session_type = MindBody::v(5)->sites()
            ->mapSessionType(Arr::get($data, 'SessionType', []));

        return $class_description;
    }

    /**
     * Maps class data from the endpoint into the common format
     *
     * @param array $data
     *
     * @return ClassInstance
     */
    public function mapClass(array $data)
    {
        $class = new ClassInstance;

        $class->mindbody_id = Arr::get($data, 'ClassScheduleID', []);
        $class->schedule_id = Arr::get($data, 'ID', []);
        $class->max_capacity = Arr::get($data, 'MaxCapacity', []);
        $class->web_capacity = Arr::get($data, 'WebCapacity', []);
        $class->booked = Arr::get($data, 'TotalBooked', []);
        $class->web_booked = Arr::get($data, 'WebBooked', []);
        $class->is_available = Arr::get($data, 'IsAvailable', []);
        $class->waitlisted = Arr::get($data, 'TotalBookedWaitlist', []);
        $class->waitlist_available = Arr::get($data, 'IsWaitlistAvailable', []);
        $class->active = Arr::get($data, 'Active', []);
        $class->cancelled = Arr::get($data, 'IsCanceled', []);
        $class->substitute = Arr::get($data, 'Substitute', []);

        $start_date = Arr::get($data, 'StartDateTime', null);
        if ($start_date) {
            $class->start_date = Carbon::parse($start_date);
        }

        $end_date = Arr::get($data, 'EndDateTime', []);
        if ($end_date) {
            $class->end_date = Carbon::parse($end_date);
        }

        $location = Arr::get($data, 'Location', []);
        if ($location) {
            $class->location = MindBody::v(5)->sites()
                ->mapLocation($location);
        }

        $class_description = Arr::get($data, 'ClassDescription', []);
        if ($class_description) {
            $class->class_description = $this->mapClassDescription(
                $class_description
            );
        }

        $staff = Arr::get($data, 'Staff', []);
        if ($staff) {
            $class->staff = MindBody::v(5)->staff()->mapStaff($staff);
        }

        return $class;
    }
}
