<?php

namespace Yadda\Enso\Gyms\MindBody\V5\Endpoints;

use Illuminate\Support\Arr;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Log;
use Yadda\Enso\Gyms\MindBody\DataTypes\StaffMember;
use Yadda\Enso\Gyms\MindBody\V5\Endpoints\Endpoint;

class Staff extends Endpoint
{
    public function getStaff($request_data = [], $return_object = false, $debug_errors = false)
    {
        $response = $this->api->GetStaff($request_data, $return_object, $debug_errors);

        if (!Arr::has($response, 'GetStaffResult.StaffMembers.Staff')) {
            Log::error('Failed to get staff from Mindbody', [
                'response' => $response,
            ]);

            return new Collection;
        }

        $items = Arr::get($response, 'GetStaffResult.StaffMembers.Staff', []);

        Log::info('Got ' . count($items) . ' staff from Mindbody.');

        return $this
            ->collectData($items)
            ->map(function ($data) {
                return $this->mapStaff($data);
            });
    }

    /**
     * Undocumented function
     *
     * @param array $data
     *
     * @return StaffMember
     */
    public function mapStaff($data)
    {
        $staff_member = new StaffMember;

        $staff_member->mindbody_id = Arr::get($data, 'ID');
        $staff_member->name = Arr::get($data, 'Name');
        $staff_member->first_name = Arr::get($data, 'FirstName');
        $staff_member->last_name = Arr::get($data, 'LastName');
        $staff_member->description = Arr::get($data, 'Bio');

        return $staff_member;
    }
}
