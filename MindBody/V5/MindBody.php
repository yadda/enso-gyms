<?php

namespace Yadda\Enso\Gyms\MindBody\V5;

use Exception;
use Yadda\Enso\Gyms\MindBody\Interfaces\MindBodyInterface;
use Yadda\Enso\Gyms\MindBody\V5\Api;
use Yadda\Enso\Gyms\MindBody\V5\Endpoints\Classes;
use Yadda\Enso\Gyms\MindBody\V5\Endpoints\Sales;
use Yadda\Enso\Gyms\MindBody\V5\Endpoints\Sites;
use Yadda\Enso\Gyms\MindBody\V5\Endpoints\Staff;

class MindBody implements MindBodyInterface
{
    public function __construct()
    {
        $this->api = new Api;
    }

    public function appointments()
    {
        throw new Exception('Not yet implemented');
    }

    public function classes()
    {
        return new Classes($this->api);
    }

    public function clients()
    {
        throw new Exception('Not yet implemented');
    }

    public function sales()
    {
        return new Sales($this->api);
    }

    public function sites()
    {
        return new Sites($this->api);
    }

    public function staff()
    {
        return new Staff($this->api);
    }
}
