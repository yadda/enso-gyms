<?php

namespace Yadda\Enso\Gyms\MindBody\V5;

use Carbon\Carbon;
use Exception;
use Illuminate\Support\Facades\Config;
use Log;
use SimpleXMLElement;
use SoapClient;

class Api
{
    /**
     * @var SoapClient
     */
    protected $client;

    /**
     * @var string
     */
    protected $api_key;

    /**
     * @var int
     */
    protected $site_id;

    /**
     * @var array
     */
    protected $user_credentials = [];

    /**
     * List of callable API Endopoints
     *
     * @var array
     */
    protected $api_services = [
        'AppointmentService' => 'https://api.mindbodyonline.com/0_5/AppointmentService.asmx?WSDL',
        'ClassService' => 'https://api.mindbodyonline.com/0_5/ClassService.asmx?WSDL',
        'ClientService' => 'https://api.mindbodyonline.com/0_5/ClientService.asmx?WSDL',
        'DataService' => 'https://api.mindbodyonline.com/0_5/DataService.asmx?WSDL',
        'FinderService' => 'https://api.mindbodyonline.com/0_5/FinderService.asmx?WSDL',
        'SaleService' => 'https://api.mindbodyonline.com/0_5/SaleService.asmx?WSDL',
        'SiteService' => 'https://api.mindbodyonline.com/0_5/SiteService.asmx?WSDL',
        'StaffService' => 'https://api.mindbodyonline.com/0_5/StaffService.asmx?WSDL',
    ];

    /**
     * @var array
     */
    protected $soap_options = [];

    /**
     * This has been filled out in with the results of calls to __getFunctions()
     * instead of querying each $api_service in every __construct of this class.
     *
     * The downside is that if the API changes, we'll need to update this.
     *
     * @var array
     */
    protected $api_methods = [
        "AppointmentService" => [
            "GetStaffAppointments",
            "AddOrUpdateAppointments",
            "GetBookableItems",
            "GetScheduleItems",
            "AddOrUpdateAvailabilities",
            "GetActiveSessionTimes",
            "GetAppointmentOptions",
        ],
        "ClassService" => [
            "GetClasses",
            "UpdateClientVisits",
            "GetClassVisits",
            "GetClassDescriptions",
            "GetEnrollments",
            "GetClassSchedules",
            "AddClientsToClasses",
            "RemoveClientsFromClasses",
            "AddClientsToEnrollments",
            "RemoveFromWaitlist",
            "GetSemesters",
            "GetCourses",
            "GetWaitlistEntries",
            "SubstituteClassTeacher",
            "SubtituteClassTeacher",
            "CancelSingleClass",
        ],
        "ClientService" => [
            "AddArrival",
            "AddOrUpdateClients",
            "GetClients",
            "GetCustomClientFields",
            "GetClientIndexes",
            "GetClientContactLogs",
            "AddOrUpdateContactLogs",
            "GetContactLogTypes",
            "UploadClientDocument",
            "UploadClientPhoto",
            "GetClientFormulaNotes",
            "AddClientFormulaNote",
            "DeleteClientFormulaNote",
            "GetClientReferralTypes",
            "GetActiveClientMemberships",
            "GetClientContracts",
            "GetClientAccountBalances",
            "GetClientServices",
            "GetClientVisits",
            "GetClientPurchases",
            "GetClientSchedule",
            "GetRequiredClientFields",
            "ValidateLogin",
            "UpdateClientServices",
            "SendUserNewPassword",
        ],
        "DataService" => [
            "SelectDataXml",
            "FunctionDataXml",
            "FunctionDataCSV",
            "SelectDataCSV",
            "FunctionAggregateDataXml",
            "SelectAggregateDataXml",
            "SelectAggregateDataCSV",
            "GetFunctionNames",
            "GetFunctionParameters",
            "GetSitesWithFunctionDataAccess",
            "RunFunctionForJob",
        ],
        "FinderService" => [
            "GetClassesWithinRadius",
            "GetSessionTypesWithinRadius",
            "GetBusinessLocationsWithinRadius",
            "FinderCheckoutShoppingCart",
            "AddOrUpdateFinderUsers",
            "GetFinderUser",
            "SendFinderUserNewPassword",
        ],
        "SaleService" => [
            "GetAcceptedCardType",
            "CheckoutShoppingCart",
            "GetSales",
            "GetServices",
            "UpdateServices",
            "GetPackages",
            "GetProducts",
            "UpdateProducts",
            "RedeemSpaFinderWellnessCard",
            "GetCustomPaymentMethods",
            "ReturnSale",
            "UpdateSaleDate",
        ],
        "SiteService" => [
            "GetSites",
            "GetLocations",
            "GetActivationCode",
            "GetPrograms",
            "GetSessionTypes",
            "GetResources",
            "GetRelationships",
            "GetResourceSchedule",
            "ReserveResource",
            "GetMobileProviders",
            "GetProspectStages",
            "GetGenders",
        ],
        "StaffService" => [
            "GetStaff",
            "GetStaffPermissions",
            "AddOrUpdateStaff",
            "GetStaffImgURL",
            "ValidateStaffLogin",
            "GetSalesReps",
        ],
    ];

    /**
     * @var boolean
     */
    protected $debug_soap_errors = true;

    /**
     * Create a new Api
     */
    public function __construct()
    {
        $this->api_key = Config::get('enso.gyms.mindbody.api_key');
        $this->site_id = Config::get('enso.gyms.mindbody.site_id');

        $this->user_credentials['Username'] = Config::get('enso.gyms.mindbody.user.username');
        $this->user_credentials['Password'] = Config::get('enso.gyms.mindbody.user.password');
        $this->user_credentials['SiteIDs'] = Config::get('enso.gyms.mindbody.user.site_ids');

        $this->soap_options = [
            'soap_version' => SOAP_1_1,
            'trace' => true,
            'stream_context' => stream_context_create([
                'ssl' => [
                    'crypto_method' => STREAM_CRYPTO_METHOD_TLSv1_2_CLIENT,
                    'ciphers' => 'SHA256',
                    'verify_peer' => false,
                    'verify_peer_name' => false,
                    'allow_self_signed' => true,
                ],
                'http' => [
                    'header' => "Api-key: " . $this->api_key . "\r\nSiteId: " . $this->site_id,
                ]
            ])
        ];

        if (empty($this->api_key) || empty($this->site_id)) {
            throw new Exception('MindBody API does not have credentials set.');
        }
    }

    /**
     * Magic method for calling MindBody API method if available
     *
     * @param string $name
     * @param array $arguments
     * @return void
     */
    public function __call($name, $arguments)
    {
        $soap_service = false;

        foreach ($this->api_methods as $api_service_name => $api_methods) {
            if (in_array($name, $api_methods)) {
                $soap_service = $api_service_name;
                break;
            }
        }

        if ($soap_service === false) {
            throw new Exception('Called unknown method ' . (string)$name . ' on MindBody/Api');
        }

        return $this->callMindBodyService($soap_service, $name, ...$arguments);
    }

    /**
     * Send a SOAP request to MindBody and return the result
     *
     * @param string $service_name
     * @param string $method_name
     * @param array $request_data
     * @param boolean $return_object
     * @param boolean $debug_errors
     *
     * @return array|false false on error
     */
    public function callMindBodyService($service_name, $method_name, $request_data = [], $return_object = false, $debug_errors = false)
    {
        $headers = [];

        $request_data = array_merge(['UserCredentials' => $this->user_credentials], $request_data);

        $this->client = new \SoapClient($this->api_services[$service_name], $this->soap_options);

        try {
            $result = $this->client->$method_name(['Request' => $request_data], [], $headers);

            if ($return_object) {
                return $result;
            } else {
                return json_decode(json_encode($result), 1);
            }
        } catch (SoapFault $e) {
            if ($this->debug_soap_errors && $debug_errors) {
                Log::debug('MindBody Soap Error: [' . $e->faultCode . '] ' . $e->faultString);
                $this->debug();
            }

            return false;
        } catch (Exception $e) {
            if ($this->debug_soap_errors && $debug_errors) {
                Log::error($e);
            }

            return false;
        }
    }

    /**
     * Return the previous Soap request as an XML string
     *
     * @return string
     */
    public function getXMLRequest()
    {
        return $this->client->__getLastRequest();
    }

    /**
     * Get the previous Soap request headers
     *
     * @return string
     */
    public function getRequestHeaders()
    {
        return $this->client->__getLastRequestHeaders();
    }

    /**
     * Get the previous Soap response as an XML string
     *
     * @return string
     */
    public function getXMLResponse()
    {
        return $this->client->__getLastResponse();
    }

    /**
     * Get the previous Soap response headers
     *
     * @return string
     */
    public function getResponseHeaders()
    {
        return $this->client->__getResponseHeaders();
    }

    public function debug()
    {
        Log::debug(print_r($this->getXMLRequest(), true));
        Log::debug(print_r($this->getXMLResponse(), true));
    }

    public function makeNumericArray($data)
    {
        return isset($data[0]) ? $data : array($data);
    }

    public function replaceEmptyArraysWithNull(array $array)
    {
        foreach ($array as &$value) {
            if (is_array($value)) {
                if (empty($value)) {
                    $value = null;
                } else {
                    $value = $this->replaceEmptyArraysWithNull($value);
                }
            }
        }

        return $array;
    }

    // phpcs:ignore PSR1.Methods.CamelCapsMethodName.NotCamelCaps
    public function FunctionDataXml()
    {
        $args = func_get_args();
        $request = empty($args[0]) ? null : $args[0];
        $return_obj = empty($args[1]) ? null : $args[1];
        $debug_errors = empty($args[2]) ? null : $args[2];
        $data = $this->callMindBodyService('DataService', 'FunctionDataXml', $request);
        $xmlString = $this->getXMLResponse();
        $sxe = new SimpleXMLElement($xmlString);
        $sxe->registerXPathNamespace('mindbody', 'http://clients.mindbodyonline.com/api/0_5');
        $res = $sxe->xpath('//mindbody:FunctionDataXmlResponse');

        if ($return_obj) {
            return $res[0];
        } else {
            $arr = $this->replaceEmptyArraysWithNull(json_decode(json_encode($res[0]), 1));

            if (isset($arr['FunctionDataXmlResult']['Results']['Row']) && is_array($arr['FunctionDataXmlResult']['Results']['Row'])) {
                $arr['FunctionDataXmlResult']['Results']['Row'] = $this->makeNumericArray($arr['FunctionDataXmlResult']['Results']['Row']);
            }

            return $arr;
        }
    }

    public static function createTimeString(Carbon $time)
    {
        return $time->toDateString() . 'T' . $time->toTimeString();
    }
}
