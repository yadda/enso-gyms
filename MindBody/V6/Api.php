<?php

namespace Yadda\Enso\Gyms\MindBody\V6;

use Exception;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\ClientException;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Log;

class Api
{
    /**
     * @var string
     */
    protected $api_key;

    /**
     * @var int
     */
    protected $site_id;

    /**
     * @var array
     */
    protected $user_credentials = [];

    /**
     * @var string
     */
    protected $endpoint = 'https://api.mindbodyonline.com/public/v6';

    /**
     * @var array
     */
    protected $get_methods = [
        'site/memberships',
        'usertoken/issue',
        'client/activeclientmemberships',
        'client/addclient',
        'client/clients',
        'client/requiredclientfields',
        'sale/checkoutshoppingcart',
        'sale/services',
    ];

    /**
     * @var string
     */
    protected $staff_token;

    /**
     * @var Client
     */
    protected $client;

    protected $max_retries = 2;

    protected $last_response;

    protected $last_status_code;

    /**
     * Create a new Api
     */
    public function __construct(Client $client)
    {
        $this->client = $client;

        $this->setUpCredentials();
        $this->getStaffToken();
    }

    /**
     * Populate the user_credentials array
     */
    protected function setUpCredentials(): void
    {
        $this->api_key = config('enso.gyms.mindbody.api_key');
        $this->site_id = config('enso.gyms.mindbody.site_id');

        $this->user_credentials['Username'] = config('enso.gyms.mindbody.user.username');
        $this->user_credentials['Password'] = config('enso.gyms.mindbody.user.password');

        if (empty($this->api_key) || empty($this->site_id)) {
            throw new Exception('MINDBODY API does not have credentials set.');
        }
    }

    /**
     * Get a Mindbody API staff token from the cache and use it to populate
     * staff_token. If there isn't one in the cache, get one from the API
     */
    protected function getStaffToken($force = false): void
    {
        $this->staff_token = Cache::get('enso-mindbody-staff-token');

        if (!$this->staff_token || $force) {
            Log::debug('Staff token NOT found in cache... Getting new token.');
            $this->staff_token = null;

            if (config('enso.gyms.mindbody.sandbox')) {
                $creds = [
                    'Username' => config('enso.gyms.mindbody.source.name'),
                    'Password' => config('enso.gyms.mindbody.source.password'),
                ];
            } else {
                $creds = [
                    'Username' => '_' . config('enso.gyms.mindbody.source.name'),
                    'Password' => config('enso.gyms.mindbody.source.password'),
                ];
            }

            $this->staff_token = $this->post('usertoken', 'issue', [], $creds);

            Cache::put('enso-mindbody-staff-token', $this->staff_token);
        } else {
            Log::debug('Staff token found in cache.', [
                'token' => $this->staff_token,
            ]);
        }

        if (!$this->staff_token) {
            throw new Exception('MINDBODY staff token could not be acquired.');
        }
    }

    /**
     * Make a GET request to the API
     */
    public function get(string $service, string $endpoint, array $query = [], $attempt = 1)
    {
        $action = $service . '/' . $endpoint;
        $uri = $this->endpoint . '/' . $action;

        if (!in_array($action, $this->get_methods)) {
            throw new Exception('MINDBODY API endpoint ' . $action . ' not known.');
        }

        $headers = [
            'SiteId' => $this->site_id,
            'Api-Key' => $this->api_key,
        ];

        if ($this->staff_token) {
            $headers['authorization'] = $this->staff_token['AccessToken'];
        }

        if (!config('enso.gyms.mindbody.sandbox')) {
            $headers['User-Agent'] = config('enso.gyms.mindbody.app_name');
        }

        $body = json_encode($this->user_credentials);

        try {
            $response = $this->client->request(
                'GET',
                $uri,
                [
                    'headers' => $headers,
                    'query' => $query,
                    'body' => $body,
                ]
            );
        } catch (ClientException $e) {
            Log::error($e);

            $this->last_response = $e->getResponse();
            $this->last_status_code = $e->getResponse()->getStatusCode();

            $body = json_decode((string) $e->getResponse()->getBody());

            if ($e->getResponse()->getStatusCode() === 401) {
                if ($attempt >= $this->max_retries) {
                    return 401;
                }

                $this->getStaffToken(true);

                return $this->get($service, $endpoint, $query, $attempt + 1);
            }

            return $e->getResponse()->getStatusCode();
        }

        $this->last_response = $response;
        $this->last_status_code = $response->getStatusCode();

        if ($response->getStatusCode() === 200) {
            return json_decode((string) $response->getBody(), true);
        } else {
            return $response->getStatusCode();
        }
    }

    /**
     * Make a post request to the api
     */
    public function post(string $service, string $endpoint, array $query = [], array $body = null, $attempt = 1)
    {
        $action = $service . '/' . $endpoint;
        $uri = $this->endpoint . '/' . $action;

        if (!in_array($action, $this->get_methods)) {
            throw new Exception('MINDBODY API endpoint ' . $action . ' not known.');
        }

        $headers = [
            'SiteId' => $this->site_id,
            'Api-Key' => $this->api_key,
            'Content-Type' => 'application/json',
        ];

        if (!config('enso.gyms.mindbody.sandbox')) {
            $headers['User-Agent'] = config('enso.gyms.mindbody.app_name');
        }

        if ($this->staff_token) {
            $headers['authorization'] = $this->staff_token['AccessToken'];
        }

        $body = json_encode($body);

        try {
            $response = $this->client->request(
                'POST',
                $uri,
                [
                    'headers' => $headers,
                    'query' => $query,
                    'body' => $body,
                ]
            );
        } catch (ClientException $e) {
            $this->last_response = $e->getResponse();
            $this->last_status_code = $e->getResponse()->getStatusCode();

            Log::error($e);

            $body = json_decode((string) $e->getResponse()->getBody());

            if ($e->getResponse()->getStatusCode() === 401) {
                if ($attempt >= $this->max_retries) {
                    return 401;
                }

                $this->getStaffToken(true);

                return $this->get($service, $endpoint, $query, $body, $attempt + 1);
            }

            return $e->getResponse()->getStatusCode();
        }

        $this->last_response = $response;
        $this->last_status_code = $response->getStatusCode();

        if ($response->getStatusCode() === 200) {
            return json_decode((string) $response->getBody(), true);
        } else {
            return $response->getStatusCode();
        }
    }

    public function response()
    {
        return $this->last_response;
    }

    public function statusCode()
    {
        return $this->last_status_code;
    }
}
