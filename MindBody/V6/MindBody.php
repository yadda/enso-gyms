<?php

namespace Yadda\Enso\Gyms\MindBody\V6;

use Exception;
use Illuminate\Support\Facades\App;
use Yadda\Enso\Gyms\MindBody\Interfaces\MindBodyInterface;
use Yadda\Enso\Gyms\MindBody\V6\Endpoints\Clients;
use Yadda\Enso\Gyms\MindBody\V6\Endpoints\Sales;
use Yadda\Enso\Gyms\MindBody\V6\Endpoints\Sites;

class MindBody implements MindBodyInterface
{
    public function __construct()
    {
        $this->api = App::make('mindbody-api-v6');
    }

    public function appointments()
    {
        throw new Exception('Not yet implemented');
    }

    public function classes()
    {
        throw new Exception('Not yet implemented');
    }

    public function clients()
    {
        return new Clients($this->api);
    }

    public function sales()
    {
        return new Sales($this->api);
    }

    public function sites()
    {
        return new Sites($this->api);
    }

    public function staff()
    {
        throw new Exception('Not yet implemented');
    }

    public function response()
    {
        return $this->api->response();
    }

    public function statusCode()
    {
        return $this->api->statusCode();
    }
}
