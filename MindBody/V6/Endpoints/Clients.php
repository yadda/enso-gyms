<?php

namespace Yadda\Enso\Gyms\MindBody\V6\Endpoints;

use Exception;
use Illuminate\Support\Arr;
use Yadda\Enso\Gyms\MindBody\DataTypes\Client;
use Yadda\Enso\Gyms\MindBody\DataTypes\ClientService;

class Clients extends Endpoint
{
    public function clients(array $options = [])
    {
        $response = $this->clientsRaw($options);

        return $this->collectData(
            Arr::get($response, 'Clients')
        )->map(function ($data) {
            return Client::makeFromApiData($data);
        });
    }

    public function clientsRaw(array $options = [])
    {
        $response = $this->api->get('client', 'clients', $options);

        if (!is_array($response)) {
            throw new Exception(
                'Failed to get clients. API response was not an array. ' . print_r($response, true)
            );
        }

        return $response;
    }

    public function requiredClientFields()
    {
        $response = $this->api->get('client', 'requiredclientfields');

        return Arr::get($response, 'RequiredClientFields', []);
    }

    public function activeClientMemberships(string $client_id)
    {
        $options = [
            'ClientId' => $client_id,
        ];

        $response = $this->api->get('client', 'activeclientmemberships', $options);

        if (!is_array($response)) {
            throw new Exception(
                'Failed to get clients active memberships. API response was not an array. '  . print_r($response, true)
            );
        }

        return $this->collectData(
            Arr::get($response, 'ClientMemberships')
        )->map(function ($data) {
            return $this->mapMembership($data);
        });
    }

    public function addClient(Client $client_data): ?Client
    {
        $data = [
            'BirthDate' => $client_data->birth_date,
            'Email' => $client_data->email,
            'FirstName' => $client_data->first_name,
            'LastName' => $client_data->last_name,
        ];

        $response = $this->api->post('client', 'addclient', [], $data);

        if (!is_array($response)) {
            throw new Exception(
                'Failed to create client. API response was not an array. ' . print_r($response, true)
            );
        }

        $client_data = Arr::get($response, 'Client');

        if (!$client_data) {
            return null;
        }

        return Client::makeFromApiData($client_data);
    }

    public function mapMembership(array $data): ClientService
    {
        $client_service = new ClientService;

        $client_service->active_date = Arr::get($data, 'ActiveDate');
        $client_service->count = Arr::get($data, 'Count');
        $client_service->current = Arr::get($data, 'Current');
        $client_service->exiration_date = Arr::get($data, 'ExirationDate');
        $client_service->id = Arr::get($data, 'Id');
        $client_service->product_id = Arr::get($data, 'ProductId');
        $client_service->name = Arr::get($data, 'Name');
        $client_service->payment_date = Arr::get($data, 'PaymentDate');
        $client_service->program = Arr::get($data, 'Program');
        $client_service->remaining = Arr::get($data, 'Remaining');
        $client_service->site_id = Arr::get($data, 'SiteId');
        $client_service->action = Arr::get($data, 'Action');
        $client_service->membership_id = Arr::get($data, 'MembershipId');
        $client_service->restricted_locations = Arr::get($data, 'RestrictedLocations');
        $client_service->icon_code = Arr::get($data, 'IconCode');

        return $client_service;
    }
}
