<?php

namespace Yadda\Enso\Gyms\MindBody\V6\Endpoints;

use Exception;
use Illuminate\Support\Arr;
use Yadda\Enso\Gyms\MindBody\DataTypes\Service;

class Sales extends Endpoint
{
    public function checkoutShoppingCart(array $options = [])
    {
        $response = $this->api->get('sale', 'checkoutshoppingcart', $options);

        if (!is_array($response)) {
            throw new Exception();
        }

        return $response;
    }

    public function getServices(array $options = [])
    {
        $response = $this->api->get('sale', 'services', $options);

        if (!is_array($response)) {
            throw new Exception('Failed to get Mindbody services. ' . print_r($response, true));
        }

        return $this->collectData(
            Arr::get($response, 'Services')
        )->map(function ($data) {
            return $this->mapService($data);
        });
    }

    public function mapService(array $data): Service
    {
        $service = new Service;

        $service->mindbody_id = Arr::get($data, 'Id');
        $service->name = Arr::get($data, 'Name');
        $service->price = intval(Arr::get($data, 'Price') * 100);
        $service->program_id = Arr::get($data, 'ProgramId');
        $service->class_count = Arr::get($data, 'Count');

        return $service;
    }
}
