<?php

namespace Yadda\Enso\Gyms\MindBody\V6\Endpoints;

use Exception;
use Illuminate\Support\Arr;
use Yadda\Enso\Gyms\MindBody\DataTypes\Membership;
use Yadda\Enso\Gyms\MindBody\V6\Endpoints\Endpoint;

class Sites extends Endpoint
{
    public function getMemberships(string $schedule_type = null, bool $online_only = false)
    {
        $options = array_filter([
            'ScheduleType' => $schedule_type,
            'OnlineOnly' => $online_only,
        ]);

        $response = $this->api->get('site', 'memberships', $options);

        if (gettype($response) !== 'array') {
            throw new Exception(
                'Failed to get clients for update. API call was not successful. ' . print_r($response, true)
            );
        }

        return $this->collectData(
            Arr::get($response, 'Memberships')
        )->map(function ($data) {
            return $this->mapMembership($data);
        });
    }

    /**
     * Maps service data from the endpoint into the common format
     *
     * @param array $data
     *
     * @return ClassDescription
     */
    public function mapMembership(array $data): Membership
    {
        $membership = new Membership;

        $membership->mindbody_id = Arr::get($data, 'MembershipId');
        $membership->name = Arr::get($data, 'MembershipName');
        $membership->published = Arr::get($data, 'IsActive');

        return $membership;
    }
}
