<?php

namespace Yadda\Enso\Gyms\MindBody\V6\Endpoints;

use Illuminate\Support\Collection;
use Yadda\Enso\Gyms\MindBody\V6\Api;

class Endpoint
{
    protected $api;

    public function __construct(Api $api)
    {
        $this->api = $api;
    }

    /**
     * Compensate for the fact that endpoints return different data structures
     * dependant on the number or results available (single item = array,
     * many item = array of arrays).
     *
     * @param array $data
     *
     * @return Collection
     */
    protected function collectData($data)
    {
        if (!isset($data[0])) {
            $data = [$data];
        }

        return new Collection($data);
    }
}
