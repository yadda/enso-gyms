<?php

namespace Yadda\Enso\Gyms\MindBody\DataTypes;

use Yadda\Enso\Gyms\Contracts\SessionType as SessionTypeContract;
use Yadda\Enso\Gyms\MindBody\DataTypes\Common;

class SessionType extends Common
{
    public $mindbody_id;
    public $name;

    /**
     * Creates a new SessionType from this location's data.
     *
     * @return SessionTypeContract
     */
    public function createSessionType()
    {
        $session_type_instance = resolve(SessionTypeContract::class);

        $new_slug = $this->getSlugFrom($this->name);

        $new_order = $session_type_instance::max('order') + 1;

        $session_type = $session_type_instance::create([
            'mb_session_type_id' => $this->mindbody_id,
            'slug' => $new_slug,
            'order' => $new_order,
        ]);

        return $this->updateSessionType($session_type);
    }

    /**
     * Updates an existing SessionType from the data on this Location
     *
     * @param SessionTypeContract $session_type
     *
     * @return SessionTypeContract
     */
    public function updateSessionType(SessionTypeContract $session_type)
    {
        $session_type->update([
            'name' => $this->name,
        ]);

        return $session_type;
    }

    /**
     * Gets a collection of existing slugs for Service Types that begin with the
     * given slug
     *
     * @param string $slug
     *
     * @return \Illuminate\Support\Collection
     */
    public function existingSlugs($slug)
    {
        return resolve(SessionTypeContract::class)::where('slug', 'LIKE', "{$slug}%")
            ->pluck('slug');
    }
}
