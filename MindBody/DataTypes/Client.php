<?php

namespace Yadda\Enso\Gyms\MindBody\DataTypes;

use Illuminate\Support\Arr;

class Client extends Common
{
    public $account_balance;
    public $active;
    public $action;
    public $address_line_1;
    public $address_line_2;
    public $appointment_gender_preference;
    public $birth_date;
    public $city;
    public $country;
    public $client_credit_card;
    public $client_indexes;
    public $client_number_of_visits_at_site;
    public $client_relationships;
    public $creation_date;
    public $credit_card_exp_date;
    public $custom_client_fields;
    public $direct_debit_last_four;
    public $email;
    public $emergency_contact_info_email;
    public $emergency_contact_info_name;
    public $emergency_contact_info_phone;
    public $emergency_contact_info_relationship;
    public $first_appointment_date;
    public $first_name;
    public $gender;
    public $home_location;
    public $home_phone;
    public $id;
    public $is_company;
    public $is_prospect;
    public $last_formula_notes;
    public $last_modified_date;
    public $last_name;
    public $liability_release;
    public $liability_release_date;
    public $liability_released;
    public $liability_released_by;
    public $membership_icon;
    public $middle_name;
    public $mobile_phone;
    public $mobile_provider;
    public $notes;
    public $photo_url;
    public $prospect_stage;
    public $postal_code;
    public $previous_email;
    public $referred_by;
    public $red_alert;
    public $sales_reps;
    public $state;
    public $status;
    public $send_account_emails;
    public $send_account_texts;
    public $send_promotional_emails;
    public $send_promotional_texts;
    public $send_schedule_emails;
    public $send_schedule_texts;
    public $site_id;
    public $unique_id;
    public $work_extension;
    public $work_phone;
    public $yellow_alert;

    public static function makeFromApiData(array $data)
    {
        $client = new Client;

        $client->appointment_gender_preference = Arr::get($data, 'AppointmentGenderPreference');
        $client->birth_date = Arr::get($data, 'BirthDate');
        $client->country = Arr::get($data, 'Country');
        $client->creation_date = Arr::get($data, 'CreationDate');
        $client->custom_client_fields = Arr::get($data, 'CustomClientFields');
        $client->client_credit_card = Arr::get($data, 'ClientCreditCard');
        $client->credit_card_last_four = Arr::get($data, 'ClientCreditCard.LastFour');
        $client->client_indexes = Arr::get($data, 'ClientIndexes');
        $client->client_relationships = Arr::get($data, 'ClientRelationships');
        $client->first_appointment_date = Arr::get($data, 'FirstAppointmentDate');
        $client->first_name = Arr::get($data, 'FirstName');
        $client->id = Arr::get($data, 'Id');
        $client->is_company = Arr::get($data, 'IsCompany');
        $client->is_prospect = Arr::get($data, 'IsProspect');
        $client->last_name = Arr::get($data, 'LastName');
        $client->liability_released = Arr::get($data, 'Liability.IsReleased');
        $client->liability_release_date = Arr::get($data, 'Liability.AgreementDate');
        $client->liability_released_by = Arr::get($data, 'Liability.ReleasedBy');
        $client->liability_release = Arr::get($data, 'LiabilityRelease');
        $client->membership_icon = Arr::get($data, 'MembershipIcon');
        $client->mobile_provider = Arr::get($data, 'MobileProvider');
        $client->notes = Arr::get($data, 'Notes');
        $client->state = Arr::get($data, 'State');
        $client->unique_id = Arr::get($data, 'UniqueId');
        $client->last_modified_date = Arr::get($data, 'LastModifiedDateTime');
        $client->red_alert = Arr::get($data, 'RedAlert');
        $client->yellow_alert = Arr::get($data, 'YellowAlert');
        $client->middle_name = Arr::get($data, 'MiddleName');
        $client->prospect_stage = Arr::get($data, 'ProspectStage');
        $client->email = Arr::get($data, 'Email');
        $client->mobile_phone = Arr::get($data, 'MobilePhone');
        $client->home_phone = Arr::get($data, 'HomePhone');
        $client->work_phone = Arr::get($data, 'WorkPhone');
        $client->account_balance = Arr::get($data, 'AccountBalance');
        $client->address_line_1 = Arr::get($data, 'AddressLine1');
        $client->address_line_2 = Arr::get($data, 'AddressLine2');
        $client->city = Arr::get($data, 'City');
        $client->postal_code = Arr::get($data, 'PostalCode');
        $client->work_extension = Arr::get($data, 'WorkExtension');
        $client->referred_by = Arr::get($data, 'ReferredBy');
        $client->photo_url = Arr::get($data, 'PhotoURL', Arr::get($data, 'PhotoUrl'));
        $client->emergency_contact_info_name = Arr::get($data, 'EmergencyContactInfoName');
        $client->emergency_contact_info_email = Arr::get($data, 'EmergencyContactInfoEmail');
        $client->emergency_contact_info_phone = Arr::get($data, 'EmergencyContactInfoPhone');
        $client->emergency_contact_info_relationship = Arr::get($data, 'EmergencyContactInfoRelationship');
        $client->gender = Arr::get($data, 'Gender');
        $client->last_formula_notes = Arr::get($data, 'LastFormulaNotes');
        $client->active = Arr::get($data, 'Active');
        $client->sales_reps = Arr::get($data, 'SalesReps');
        $client->status = Arr::get($data, 'Status');
        $client->action = Arr::get($data, 'Action');
        $client->send_account_emails = Arr::get($data, 'SendAccountEmails');
        $client->send_account_texts = Arr::get($data, 'SendAccountTexts');
        $client->send_promotional_emails = Arr::get($data, 'SendPromotionalEmails');
        $client->send_promotional_texts = Arr::get($data, 'SendPromotionalTexts');
        $client->send_schedule_emails = Arr::get($data, 'SendScheduleEmails');
        $client->send_schedule_texts = Arr::get($data, 'SendScheduleTexts');

        return $client;
    }

    public static function makeFromWebhookData(array $data)
    {
        $client = new Client;

        $client->site_id = Arr::get($data, 'siteId');
        $client->id = Arr::get($data, 'clientId');
        $client->unique_id = Arr::get($data, 'clientUniqueId');
        $client->creation_date = Arr::get($data, 'creationDateTime');
        $client->status = Arr::get($data, 'status');
        $client->first_name = Arr::get($data, 'firstName');
        $client->last_name = Arr::get($data, 'lastName');
        $client->email = Arr::get($data, 'email');
        $client->mobile_phone = Arr::get($data, 'mobilePhone');
        $client->home_phone = Arr::get($data, 'homePhone');
        $client->work_phone = Arr::get($data, 'workPhone');
        $client->address_line_1 = Arr::get($data, 'addressLine1');
        $client->address_line_2 = Arr::get($data, 'addressLine2');
        $client->city = Arr::get($data, 'city');
        $client->state = Arr::get($data, 'state');
        $client->postal_code = Arr::get($data, 'postalCode');
        $client->country = Arr::get($data, 'country');
        $client->birth_date = Arr::get($data, 'birthDateTime');
        $client->gender = Arr::get($data, 'gender');
        $client->appointment_gender_preference = Arr::get($data, 'appointmentGenderPreference');
        $client->first_appointment_date = Arr::get($data, 'firstAppointmentDateTime');
        $client->referred_by = Arr::get($data, 'referredBy');
        $client->is_prospect = Arr::get($data, 'isProspect');
        $client->is_company = Arr::get($data, 'isCompany');
        $client->liability_released = Arr::get($data, 'isLiabilityReleased');
        $client->liability_release_date = Arr::get($data, 'liabilityAgreementDateTime');
        $client->home_location = Arr::get($data, 'homeLocation');
        $client->client_number_of_visits_at_site = Arr::get($data, 'clientNumberOfVisitsAtSite');
        $client->client_indexes = Arr::get($data, 'indexes');
        $client->send_promotional_emails = Arr::get($data, 'sendPromotionalEmails');
        $client->send_schedule_emails = Arr::get($data, 'sendScheduleEmails');
        $client->send_account_emails = Arr::get($data, 'sendAccountEmails');
        $client->send_promotional_texts = Arr::get($data, 'sendPromotionalsTexts');
        $client->send_schedule_texts = Arr::get($data, 'sendScheduleTexts');
        $client->send_account_texts = Arr::get($data, 'sendAccountTexts');
        $client->credit_card_last_four = Arr::get($data, 'creditCardLastFour');
        $client->credit_card_exp_date = Arr::get($data, 'creditCardExpDate');
        $client->direct_debit_last_four = Arr::get($data, 'directDebitLastFour');
        $client->notes = Arr::get($data, 'notes');
        $client->photo_url = Arr::get($data, 'photoUrl');
        $client->previous_email = Arr::get($data, 'previousEmail');

        return $client;
    }
}
