<?php

namespace Yadda\Enso\Gyms\MindBody\DataTypes;

use Yadda\Enso\Gyms\Contracts\Instructor;
use Yadda\Enso\Gyms\MindBody\DataTypes\Common;

class StaffMember extends Common
{
    public $mindbody_id;
    public $name;
    public $first_name;
    public $last_name;
    public $description;

    /**
     * Creates a new Instructor from this location's data.
     *
     * @return Instructor
     */
    public function createInstructor()
    {
        $instructor_instance = resolve(Instructor::class);

        $new_slug = $this->getSlugFrom($this->name);

        $new_order = $instructor_instance::max('order') + 1;

        $instructor = $instructor_instance::create([
            'mb_staff_id' => $this->mindbody_id,
            'slug' => $new_slug,
            'order' => $new_order,
        ]);

        return $this->updateInstructor($instructor);
    }

    /**
     * Updates an existing Instructor from the data on this Location
     *
     * @param Instructor $instructor
     *
     * @return Instructor
     */
    public function updateInstructor(Instructor $instructor)
    {
        $instructor->update([
            'name' => $this->name,
            'first_name' => $this->first_name,
            'last_name' => $this->last_name,
            'description' => $this->description,
        ]);

        return $instructor;
    }

    /**
     * Gets a collection of existing slugs for Instructors that begin with the
     * given slug
     *
     * @param string $slug
     *
     * @return \Illuminate\Support\Collection
     */
    public function existingSlugs($slug)
    {
        return resolve(Instructor::class)::where('slug', 'LIKE', "{$slug}%")
            ->pluck('slug');
    }
}
