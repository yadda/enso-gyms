<?php

namespace Yadda\Enso\Gyms\MindBody\DataTypes;

use Illuminate\Support\Arr;
use Yadda\Enso\Gyms\Contracts\Membership as MembershipContract;

class Membership extends Common
{
    public $site_id;
    public $client_id;
    public $client_unique_id;
    public $client_first_name;
    public $client_last_name;
    public $client_email;
    public $membership_id;
    public $membership_name;

    /**
     * Creates a new Membership from this object's data
     *
     * @return void
     */
    public function createMembership(): MembershipContract
    {
        $membership_instance = resolve(MembershipContract::class);

        $new_slug = $this->getSlugFrom($this->name);

        $membership = $membership_instance::create([
            'mindbody_id' => $this->mindbody_id,
            'name' => $this->name,
            'slug' => $new_slug,
        ]);

        return $this->updateMembership($membership);
    }

    /**
     * Updates an existing Membership from the data on this object
     */
    public function updateMembership(MembershipContract $membership): MembershipContract
    {
        $data = [
            'name' => $this->name,
        ];

        if (!$membership->slug) {
            $data['slug'] = $this->getSlugFrom($this->name);
        }

        $membership->update($data);

        return $membership;
    }

    /**
     * Gets a collection of existing slugs for Instructors that begin with the
     * given slug
     *
     * @param string $slug
     *
     * @return \Illuminate\Support\Collection
     */
    public function existingSlugs($slug)
    {
        return resolve(MembershipContract::class)::where('slug', 'LIKE', "{$slug}%")
            ->pluck('slug');
    }

    public static function makeFromWebhookData(array $data)
    {
        $membership = new Membership;

        $membership->site_id = Arr::get($data, 'siteId');
        $membership->client_id = Arr::get($data, 'clientId');
        $membership->client_unique_id = Arr::get($data, 'clientUniqueId');
        $membership->client_first_name = Arr::get($data, 'clientFirstName');
        $membership->client_last_name = Arr::get($data, 'clientLastName');
        $membership->client_email = Arr::get($data, 'clientEmail');
        $membership->membership_id = Arr::get($data, 'membershipId');
        $membership->membership_name = Arr::get($data, 'memberhipsName');

        return $membership;
    }
}
