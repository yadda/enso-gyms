<?php

namespace Yadda\Enso\Gyms\MindBody\DataTypes;

use Yadda\Enso\Gyms\Contracts\ClassType;
use Yadda\Enso\Gyms\Contracts\SessionType;
use Yadda\Enso\Gyms\MindBody\DataTypes\Common;

class ClassDescription extends Common
{
    public $mindbody_id;
    public $name;
    public $description;
    public $program;
    public $session_type;

    /**
     * Creates a new Class Type from this object's data.
     *
     * @return ClassType
     */
    public function createClassType()
    {
        $class_type_instance = resolve(ClassType::class);

        $new_slug = $this->getSlugFrom($this->name);

        $new_order = $class_type_instance::max('order') + 1;

        $class_type = $class_type_instance::create([
            'mb_class_description_id' => $this->mindbody_id,
            'slug' => $new_slug,
            'order' => $new_order,
        ]);

        return $this->updateClassType($class_type);
    }

    /**
     * Updates an existing Class Type from the data on this object.
     *
     * @param ClassType $class_type
     *
     * @return ClassType
     */
    public function updateClassType(ClassType $class_type)
    {
        $updates = [
            'name' => $this->name,
            'description' => $this->description,
        ];

        if ($this->program) {
            $updates['program_id'] = $this->program->mindbody_id;
        }

        if ($this->session_type) {
            $session_type = resolve(SessionType::class)::query()
                ->where('mb_session_type_id', $this->session_type->mindbody_id)
                ->first();

            if (is_null($session_type)) {
                $session_type = $this->session_type->createSessionType();
            }

            $updates['session_type_id'] = $session_type->getKey();
        }

        $class_type->update($updates);

        return $class_type;
    }

    /**
     * Gets a collection of existing slugs for Class Types that begin with the
     * given slug
     *
     * @param string $slug
     *
     * @return \Illuminate\Support\Collection
     */
    public function existingSlugs($slug)
    {
        return resolve(ClassType::class)::where('slug', 'LIKE', "{$slug}%")
            ->pluck('slug');
    }
}
