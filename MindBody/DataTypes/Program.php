<?php

namespace Yadda\Enso\Gyms\MindBody\DataTypes;

use Yadda\Enso\Gyms\MindBody\DataTypes\Common;

class Program extends Common
{
    public $mindbody_id;
    public $name;
    public $type;
}
