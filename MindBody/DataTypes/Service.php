<?php

namespace Yadda\Enso\Gyms\MindBody\DataTypes;

use Yadda\Enso\Gyms\Contracts\Package;
use Yadda\Enso\Gyms\MindBody\DataTypes\Common;

class Service extends Common
{
    public $mindbody_id;
    public $name;
    public $price;
    public $program_id;
    public $class_count;

    /**
     * Creates a new Package from this object's data.
     *
     * @return Package
     */
    public function createPackage()
    {
        $package_instance = resolve(Package::class);

        $new_slug = $this->getSlugFrom($this->name);

        $new_order = $package_instance::max('order') + 1;

        $package = $package_instance::create([
            'mb_service_id' => $this->mindbody_id,
            'slug' => $new_slug,
            'order' => $new_order,
        ]);

        return $this->updatePackage($package);
    }

    /**
     * Updates an existing Package from the data on this object
     *
     * @param Package $package
     *
     * @return Package
     */
    public function updatePackage(Package $package)
    {
        $package->update([
            'name' => $this->name,
            'price' => $this->price,
            'class_count' => $this->class_count,
        ]);

        return $package;
    }

    /**
     * Gets a collection of existing slugs for Packages that begin with the
     * given slug
     *
     * @param string $slug
     *
     * @return \Illuminate\Support\Collection
     */
    public function existingSlugs($slug)
    {
        return resolve(Package::class)::where('slug', 'LIKE', "{$slug}%")
            ->pluck('slug');
    }
}
