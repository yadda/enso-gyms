<?php

namespace Yadda\Enso\Gyms\MindBody\DataTypes;

use Yadda\Enso\Gyms\MindBody\DataTypes\Common;

class ClassInstance extends Common
{
    public $mindbody_id;
    public $schedule_id;
    public $location;
    public $max_capacity;
    public $web_capacity;
    public $booked = 0;
    public $web_booked = 0;
    public $is_available = true;
    public $waitlisted = 0;
    public $waitlist_available = false;
    public $active = true;
    public $cancelled = false;
    public $substitute = false;
    public $start_date;
    public $end_date;
    public $class_description;
    public $staff;
}
