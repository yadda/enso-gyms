<?php

namespace Yadda\Enso\Gyms\MindBody\DataTypes;

class ClientService extends Common
{
    public $active_date;
    public $count;
    public $current;
    public $exiration_date;
    public $id;
    public $product_id;
    public $name;
    public $payment_date;
    public $program;
    public $remaining;
    public $site_id;
    public $action;

    // The following are only populated from
    // Client.ActiveClientMembership responses
    public $membership_id;
    public $restricted_locations;
    public $icon_code;
}