<?php

namespace Yadda\Enso\Gyms\MindBody\DataTypes;

use Yadda\Enso\Gyms\Contracts\Gym;
use Yadda\Enso\Gyms\MindBody\DataTypes\Common;

class Location extends Common
{
    public $mindbody_id;
    public $longitude;
    public $latitude;
    public $name;
    public $description;
    public $phone;
    public $address;
    public $address_2;
    public $city;
    public $province;
    public $post_code;

    /**
     * Creates a new Gym from this object's data.
     *
     * @return Gym
     */
    public function createGym()
    {
        $gym_instance = resolve(Gym::class);

        $new_slug = $this->getSlugFrom($this->name);

        $new_order = $gym_instance::max('order') + 1;

        $gym = $gym_instance::create([
            'mb_location_id' => $this->mindbody_id,
            'slug' => $new_slug,
            'order' => $new_order,
        ]);

        return $this->updateGym($gym);
    }

    /**
     * Updates an existing Gym from the data on this object.
     *
     * @param Gym $gym
     *
     * @return Gym
     */
    public function updateGym(Gym $gym)
    {
        $gym->update([
            'longitude' => $this->longitude,
            'latitude' => $this->latitude,
            'name' => $this->name,
            'description' => $this->description,
            'phone' => $this->phone,
            'address' => $this->address,
            'address_2' => $this->address_2,
            'city' => $this->city,
            'province' => $this->province,
            'post_code' => $this->post_code,
        ]);

        return $gym;
    }

    /**
     * Gets a collection of existing slugs for Gym that begin with the
     * given slug
     *
     * @param string $slug
     *
     * @return \Illuminate\Support\Collection
     */
    public function existingSlugs($slug)
    {
        return resolve(Gym::class)::where('slug', 'LIKE', "{$slug}%")
            ->pluck('slug');
    }
}
