<?php

namespace Yadda\Enso\Gyms\MindBody\DataTypes;

use Illuminate\Support\Str;

abstract class Common
{
    /**
     * Generates a new slug based on the input, ensuring that it is unique
     * withing a list of existing slugs
     *
     * @param string $basis
     *
     * @return string
     */
    public function getSlugFrom($basis)
    {
        $new_slug = Str::slug($basis);

        $all_slugs = $this->existingSlugs($new_slug)->flip();

        if ($all_slugs->count() === 0) {
            return $new_slug;
        }

        $count = $all_slugs->count() - 1;

        do {
            $count++;
            $next_slug = "$new_slug" . '-' . "{$count}";
        } while ($all_slugs->has($next_slug));

        return $next_slug;
    }
}
