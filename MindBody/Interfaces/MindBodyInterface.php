<?php

namespace Yadda\Enso\Gyms\MindBody\Interfaces;

interface MindBodyInterface
{
    public function appointments();

    public function classes();

    public function clients();

    public function sales();

    public function sites();

    public function staff();
}
