<?php

namespace Yadda\Enso\Gyms\MindBody\Updaters;

interface Handler
{
    /**
     * Fetches and returns all related records, Updating local records where
     * they already exist and creating where they do not.
     *
     * @return Illuminate\Support\Collection
     */
    public function fetch();
}
