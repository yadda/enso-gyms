<?php

namespace Yadda\Enso\Gyms\MindBody\Updaters;

use Illuminate\Support\Collection;
use Yadda\Enso\Gyms\MindBody\Facades\MindBody;
use Yadda\Enso\Gyms\MindBody\Updaters\Handler;
use Yadda\Enso\Gyms\Models\Membership as MembershipModel;

class Membership implements Handler
{
    public function fetch()
    {
        $memberships = MindBody::v('6')->sites()->getMemberships();

        if ($memberships->count() === 0) {
            return new Collection;
        }

        return $this->createOrUpdateMemberships($memberships);
    }

    protected function createOrUpdateMemberships(Collection $memberships)
    {
        $mindbody_ids = $memberships->pluck('mindbody_id');

        $local_memberships = MembershipModel::query()
            ->whereIn('mindbody_id', $mindbody_ids)
            ->get()
            ->keyBy('mindbody_id');

        $updates = new Collection;

        foreach ($memberships as $membership) {
            if ($local_memberships->has($membership->mindbody_id)) {
                $updates->push(
                    $membership->updateMembership(
                        $local_memberships->get($membership->mindbody_id)
                    )
                );
            } else {
                $updates->push($membership->createMembership());
            }
        }

        return $updates;
    }
}
