<?php

namespace Yadda\Enso\Gyms\MindBody\Updaters;

use Illuminate\Support\Collection;
use Illuminate\Support\Facades\App;
use Yadda\Enso\Gyms\Contracts\Package as PackageModel;
use Yadda\Enso\Gyms\MindBody\Facades\MindBody;
use Yadda\Enso\Gyms\MindBody\Updaters\Handler;

class Package implements Handler
{
    /**
     * Fetches and returns all Packages (Locations) from the Site, updating local
     * records where they already exist and creating ones where they do not
     *
     * @return Collection
     */
    public function fetch()
    {
        $services = MindBody::v('6')->sales()->getServices();

        if ($services->count()) {
            return $this->createOrUpdatePackagesFromServices($services);
        }

        return App::make(PackageModel::class)->newCollection();
    }

    /**
     * Creates or Updates Gyms with fresh information
     *
     * @param Collection $locations
     *
     * @return void
     */
    protected function createOrUpdatePackagesFromServices(Collection $services)
    {
        $mind_body_ids = $services->pluck('mindbody_id');

        $package_instance = resolve(PackageModel::class);

        $packages = $package_instance::query()
            ->whereIn('mb_service_id', $mind_body_ids)
            ->get()
            ->keyBy('mb_service_id');

        $updates = $package_instance->newCollection();

        foreach ($services as $service) {
            if ($packages->has($service->mindbody_id)) {
                $updates->push(
                    $service->updatePackage($packages->get($service->mindbody_id))
                );
            } else {
                $updates->push($service->createPackage());
            }
        }

        return $updates;
    }
}
