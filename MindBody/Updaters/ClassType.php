<?php

namespace Yadda\Enso\Gyms\MindBody\Updaters;

use Illuminate\Support\Collection;
use Illuminate\Support\Facades\App;
use Yadda\Enso\Gyms\Contracts\ClassType as ClassTypeModel;
use Yadda\Enso\Gyms\MindBody\Facades\MindBody;
use Yadda\Enso\Gyms\MindBody\Updaters\Handler;

class ClassType implements Handler
{
    /**
     * Fetches and returns all Classes (ClassTypes), updating local
     * records where they already exist and creating ones where they do not
     *
     * @return Collection
     */
    public function fetch()
    {
        $class_descriptions = MindBody::v('5')->classes()->getClassDescriptions();

        if ($class_descriptions->count()) {
            return $this->createOrUpdateClassTypesFromDescriptions($class_descriptions);
        }

        return App::make(ClassTypeModel::class)->newCollection();
    }

    /**
     * Creates or Updates class_type with fresh information
     *
     * @param Collection $locations
     *
     * @return Collection
     */
    protected function createOrUpdateClassTypesFromDescriptions(Collection $class_descriptions)
    {
        $mind_body_ids = $class_descriptions->pluck('mindbody_id');

        $class_type_instance = resolve(ClassTypeModel::class);

        $class_types = $class_type_instance::query()
            ->whereIn('mb_class_description_id', $mind_body_ids)
            ->get()
            ->keyBy('mb_class_description_id');

        $updates = $class_type_instance->newCollection();

        foreach ($class_descriptions as $class_description) {
            if ($class_types->has($class_description->mindbody_id)) {
                $updates->push($class_description->updateClassType(
                    $class_types->get($class_description->mindbody_id)
                ));
            } else {
                $updates->push($class_description->createClassType());
            }
        }

        return $updates;
    }
}
