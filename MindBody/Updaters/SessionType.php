<?php

namespace Yadda\Enso\Gyms\MindBody\Updaters;

use Illuminate\Support\Collection;
use Illuminate\Support\Facades\App;
use Yadda\Enso\Gyms\Contracts\SessionType as SessionTypeModel;
use Yadda\Enso\Gyms\MindBody\Facades\MindBody;
use Yadda\Enso\Gyms\MindBody\Updaters\Handler;

class SessionType implements Handler
{
    /**
     * Fetches and returns all Classes (SessionTypes), updating local
     * records where they already exist and creating ones where they do not
     *
     * @return Collection
     */
    public function fetch()
    {
        $session_types = MindBody::v('5')->sites()->getSessionTypes();

        if ($session_types->count()) {
            return $this->createOrUpdateSessionTypesFromMindbody($session_types);
        }

        return App::make(SessionTypeModel::class)->newCollection();
    }

    /**
     * Creates or Updates SessionTypes with fresh information
     *
     * @param Collection $session_types
     *
     * @return Collection
     */
    protected function createOrUpdateSessionTypesFromMindbody(Collection $session_types)
    {
        $mind_body_ids = $session_types->pluck('mindbody_id');

        $session_type_instance = resolve(SessionTypeModel::class);

        $db_session_types = $session_type_instance::query()
            ->whereIn('mb_session_type_id', $mind_body_ids)
            ->get()
            ->keyBy('mb_session_type_id');

        $updates = $session_type_instance->newCollection();

        foreach ($session_types as $session_type) {
            if ($db_session_types->has($session_type->mindbody_id)) {
                $updates->push(
                    $session_type->updateSessionType(
                        $db_session_types->get($session_type->mindbody_id)
                    )
                );
            } else {
                $updates->push($session_type->createSessionType());
            }
        }

        return $updates;
    }
}
