<?php

namespace Yadda\Enso\Gyms\MindBody\Updaters;

use Illuminate\Support\Collection;
use Illuminate\Support\Facades\App;
use Yadda\Enso\Gyms\Contracts\Gym as GymModel;
use Yadda\Enso\Gyms\MindBody\Facades\MindBody;
use Yadda\Enso\Gyms\MindBody\Updaters\Handler;

class Gym implements Handler
{
    /**
     * Fetches and returns all Gyms (Locations) from the Site, updating local
     * records where they already exist and creating ones where they do not
     *
     * @return Collection
     */
    public function fetch()
    {
        $locations = MindBody::v('5')->sites()->getLocations();

        if ($locations->count()) {
            return $this->createOrUpdateGymsFromLocations($locations);
        }

        return App::make(GymModel::class)->newCollection();
    }

    /**
     * Creates or Updates Gyms with fresh information
     *
     * @param Collection $locations
     *
     * @return Collection
     */
    protected function createOrUpdateGymsFromLocations(Collection $locations)
    {
        $mind_body_ids = $locations->pluck('mindbody_id');

        $gym_instance = resolve(GymModel::class);

        $gyms = $gym_instance::query()
            ->whereIn('mb_location_id', $mind_body_ids)
            ->get()
            ->keyBy('mb_location_id');

        $updates = $gym_instance->newCollection();

        foreach ($locations as $location) {
            if ($gyms->has($location->mindbody_id)) {
                $updates->push($location->updateGym($gyms->get($location->mindbody_id)));
            } else {
                $updates->push($location->createGym());
            }
        }

        return $updates;
    }
}
