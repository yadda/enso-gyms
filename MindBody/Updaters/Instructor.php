<?php

namespace Yadda\Enso\Gyms\MindBody\Updaters;

use Illuminate\Support\Collection;
use Illuminate\Support\Facades\App;
use Yadda\Enso\Gyms\Contracts\Instructor as InstructorModel;
use Yadda\Enso\Gyms\MindBody\Facades\MindBody;
use Yadda\Enso\Gyms\MindBody\Updaters\Handler;

class Instructor implements Handler
{
    /**
     * Fetches and returns all Intructors, updating local records where they
     * already exist and creating ones where they do not
     *
     * @return Collection
     */
    public function fetch()
    {
        $staff = MindBody::v('5')->staff()->getStaff();

        if ($staff->count()) {
            return $this->createOrUpdateInstructorsFromStaff($staff);
        }

        return App::make(InstructorModel::class)->newCollection();
    }

    /**
     * Creates or Updates Gyms with fresh information
     *
     * @param Collection $staff
     *
     * @return Collection
     */
    protected function createOrUpdateInstructorsFromStaff(Collection $staff)
    {
        $mind_body_ids = $staff->pluck('mindbody_id');

        $instructor_instance = resolve(InstructorModel::class);

        $instructors = $instructor_instance::query()
            ->whereIn('mb_staff_id', $mind_body_ids)
            ->get()
            ->keyBy('mb_staff_id');

        $updates = $instructor_instance->newCollection();

        foreach ($staff as $staff_member) {
            if ($instructors->has($staff_member->mindbody_id)) {
                $updates->push(
                    $staff_member->updateInstructor(
                        $instructors->get($staff_member->mindbody_id)
                    )
                );
            } else {
                $updates->push($staff_member->createInstructor());
            }
        }

        return $updates;
    }
}
