<?php

namespace Yadda\Enso\Gyms\MindBody\Facades;

use Illuminate\Support\Facades\Facade;

class MindBody extends Facade
{
    protected static function getFacadeAccessor()
    {
        return 'mindbody-api';
    }
}
