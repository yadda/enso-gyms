<?php

namespace Yadda\Enso\Gyms\Checkout;

use Illuminate\Contracts\Session\Session;
use Illuminate\Support\Arr;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\App;
use Yadda\Enso\Gyms\Checkout\MoneyHandler;
use Yadda\Enso\Gyms\Contracts\Basket as BasketContract;
use Yadda\Enso\Gyms\Contracts\Package as PackageModel;
use Yadda\Enso\Gyms\Contracts\Promotion as PromotionModel;

class Basket implements BasketContract
{
    protected $packages;

    protected $promotions;

    public function __construct(Session $store)
    {
        $this->session = $store;

        $this->parseBasketData(
            $this->session->get('basket', [])
        );
    }

    /**
     * Checks to see whether this basket is empty
     *
     * @return boolean
     */
    public function isEmpty() :bool
    {
        return !$this->hasPackages();
    }

    /**
     * Checks whether the basket has any Packages
     *
     * @return boolean
     */
    public function hasPackages() : bool
    {
        return (bool)$this->packages->count();
    }

    /**
     * Get Package information for this basket
     *
     * @return Collection
     */
    public function getPackages()
    {
        return $this->packages;
    }

    /**
     * Adds a package to the Basket, optionally specifying a quantity.
     *
     * @param PackageModel $package  Package to add
     * @param integer      $quantity Number to add
     *
     * @return self
     */
    public function addPackage(PackageModel $package, $quantity = null)
    {
        if ($this->packages->has($package->getKey())) {
            return $this->incrementPackage($package, $quantity);
        } else {
            $this->packages->put(
                $package->getKey(),
                [
                    'package' => $package,
                    'quantity' => $quantity ?? 1,
                ]
            );

            $this->updateSession();
        }

        return $this;
    }

    /**
     * Increments the quantity of a Package in the basket.
     *
     * @param PackageModel $package  Package who's data should be updated
     * @param integer      $quantity How much to increment
     *
     * @return self
     */
    public function incrementPackage(PackageModel $package, $quantity = null)
    {
        if ($this->packages->has($package->getKey())) {
            $basket_package = $this->packages->get($package->getKey());

            return $this->setPackageQuantity(
                $package,
                $basket_package['quantity'] + ($quantity ?? 1)
            );
        } else {
            return $this->addPackage($package, $quantity);
        }

        return $this;
    }

    /**
     * Removed the given package from the Basket
     *
     * @param PackageModel $package
     *
     * @return self
     */
    public function removePackage(PackageModel $package)
    {
        $this->packages->forget($package->getKey());

        $this->updateSession();

        return $this;
    }

    /**
     * Sets the quantity of the given package to the specified value.
     *
     * @param PackageModel $package
     * @param integer      $quantity
     *
     * @return self
     */
    public function setPackageQuantity(PackageModel $package, $quantity)
    {
        if ($quantity == 0) {
            $this->removePackage($package);
        }

        if ($this->packages->has($package->getKey())) {
            $this->packages->put(
                $package->getKey(),
                [
                    'package' => $package,
                    'quantity' => $quantity,
                ]
            );

            $this->updateSession();
        } else {
            return $this->addPackage($package, $quantity);
        }

        return $this;
    }

    /**
     * Checks whether the basket has any Promotions
     *
     * @return boolean
     */
    public function hasPromotions() : bool
    {
        return (bool)$this->promotions->count();
    }

    /**
     * Get Promotion information for this basket
     *
     * @return Collection
     */
    public function getPromotions()
    {
        return $this->promotions;
    }

    /**
     * Returns the current Promotions, after applying to the Basket contents
     *
     * @return Collection
     */
    public function getAppliedPromotions(): Collection
    {
        return ApplyPromotionsToBasket::with($this->getPromotions())
            ->toPackages($this->getPackages())
            ->getPromotions();
    }

    /**
     * Checks whether the given Promotion is valid to act on any item in the
     * basket
     *
     * @param PromotionModel $promotion
     *
     * @return boolean
     */
    public function promotionIsValidForBasket(PromotionModel $promotion): bool
    {
        if ($promotion->packages->count() === 0) {
            return true;
        }

        return count(
            $this->getPackages()->intersectKey(
                $promotion->packages->keyBy('id')
            )
        );
    }

    /**
     * Adds a promotion to the Basket, optionally specifying a quantity.
     *
     * @param PromotionModel $package  Promotion to add
     * @param integer      $quantity Number to add
     *
     * @return self
     */
    public function addPromotion(PromotionModel $promotion)
    {
        if (!$this->promotions->has($promotion->getKey())) {
            $this->promotions->put(
                $promotion->getKey(),
                [
                    'promotion' => $promotion,
                    'applied' => false,
                ]
            );

            $this->updateSession();
        }

        return $this;
    }

    /**
     * Removed the given Promotion from the Basket
     *
     * @param PromotionModel $promotion
     *
     * @return self
     */
    public function removePromotion(PromotionModel $promotion)
    {
        $this->promotions->forget($promotion->getKey());

        $this->updateSession();

        return $this;
    }

    /**
     * Returns the total quantity of all packages.
     *
     * @return integer
     */
    public function getTotalPackages() : int
    {
        return $this->packages->sum('quantity');
    }

    /**
     * Gets the total price of all items in the Basket
     *
     * @param Collection $promotions Collection of applied promotions if already
     *                               calculated
     *
     * @return float
     */
    public function getTotalPrice(Collection $promotions = null) : float
    {
        $package_total = $this->getPackages()->reduce(function ($carry, $item) {
            return $carry + ($item['quantity'] * $item['package']->price);
        }, 0);

        $promotions = $promotions ?? $this->getAppliedPromotions();

        $promotion_total = $promotions->filter(function ($promotion) {
            return $promotion['applied'];
        })->reduce(function ($carry, $item) {
            return $carry + ($item['value']);
        }, 0);

        return $package_total + $promotion_total;
    }

    /**
     * Empties the basket and removes items from the Session store
     *
     * @return self
     */
    public function emptyBasket()
    {
        $this->session->put('basket', []);

        $this->packages = new Collection;
        $this->promotions = new Collection;

        return $this;
    }

    /**
     * Converst this Basket into an array
     *
     * @return array
     */
    public function toArray() : array
    {
        return [
            'packages' => $this->getPackages()->map(function ($package) {
                return $this->createBasketPackage(
                    Arr::get($package, 'package'),
                    Arr::get($package, 'quantity', 1)
                );
            })->toArray(),
            'promotions' => $this->getPromotions()->map(function ($promotion) {
                return $this->createBasketPromotion($promotion);
            })->toArray(),
            'total' => $this->getTotalPrice($this->getPromotions()),
            'human_total' => App::make(MoneyHandler::class)->fromLowestDenomination(
                $this->getTotalPrice($this->getPromotions()),
                config('enso.gyms.mindbody.currency', 'THB')
            ),
        ];
    }

    /**
     * Converst this Basket into an array
     *
     * @return array
     */
    public function toArrayWithDiscounts() : array
    {
        $promotions = $this->getAppliedPromotions();

        return [
            'packages' => $this->getPackages()->map(function ($package) {
                return $this->createBasketPackage(
                    Arr::get($package, 'package'),
                    Arr::get($package, 'quantity', 1)
                );
            })->toArray(),
            'promotions' => $promotions->map(function ($promotion) {
                return $this->createBasketPromotion($promotion);
            })->toArray(),
            'total' => $this->getTotalPrice($promotions),
            'human_total' => App::make(MoneyHandler::class)->fromLowestDenomination(
                $this->getTotalPrice($promotions),
                config('enso.gyms.mindbody.currency', 'THB')
            ),
        ];
    }

    /**
     * Updates the session with current package data.
     *
     * @return void
     */
    protected function updateSession()
    {
        $this->promotions->pluck('id')->toArray();

        $this->session->put('basket', [
            'packages' => $this->packages->map(function ($package_data) {
                return $package_data['quantity'];
            })->toArray(),
            'promotions' => $this->promotions->map(function ($promotion_data) {
                return 1;
            })->toArray(),
        ]);
    }

    /**
     * Parses minified data back into full Basket information
     *
     * @param array $data
     *
     * @return void
     */
    protected function parseBasketData($data)
    {
        $this->packages = $this->parseSessionPackages(Arr::get($data, 'packages', []));
        $this->promotions = $this->parseSessionPromotions(Arr::get($data, 'promotions', []));
    }

    /**
     * Parses the package data as stored on the session into a new packages
     * Collection
     *
     * @param array $session_packages
     *
     * @return Collection
     */
    protected function parseSessionPackages($session_packages): Collection
    {
        $package_instance = resolve(PackageModel::class);

        if (empty($session_packages)) {
            return new Collection;
        }

        $package_data = new Collection($session_packages);

        $models = $package_instance::find($package_data->keys()->toArray())
            ->keyBy($package_instance->getKeyName());

        return $package_data->map(function ($item, $index) use ($models) {
            $model = $models->get($index);
            if (!$model) {
                return null;
            }

            return [
                'package' => $model,
                'quantity' => $item,
            ];
        })->filter();
    }

    /**
     * Creates Basket 'Package' entry from a Model and quantity.
     *
     * @param PackageModel $model
     * @param integer      $quantity
     *
     * @return array
     */
    protected function createBasketPackage(PackageModel $model, $quantity = null): array
    {
        return array_merge(
            $model->toArray(),
            [
                'quantity' => $quantity ?? 1,
                'quantity_human_price' => App::make(MoneyHandler::class)->fromLowestDenomination(
                    $model->price * $quantity ?? 1,
                    config('enso.gyms.mindbody.currency', 'THB')
                )
            ]
        );
    }

    /**
     * Parses the promotion data as stored on the session into a new promotions
     * Collection
     *
     * @param array $session_promotions
     *
     * @return Collection
     */
    protected function parseSessionPromotions($session_promotions): Collection
    {
        $promotion_instance = resolve(PromotionModel::class);

        if (empty($session_promotions)) {
            return new Collection;
        }

        $promotion_data = new Collection($session_promotions);

        $models = $promotion_instance::find($promotion_data->keys()->toArray())
            ->keyBy($promotion_instance->getKeyName());

        return $promotion_data->map(function ($item, $key) use ($models) {
            $model = $models->get($key);
            if (!$model) {
                return null;
            }

            return [
                'promotion' => $model,
                'applied' => false,
            ];
        })->filter();
    }

    /**
     * Creates Basket 'Promotion' entry from a Model and quantity.
     *
     * @param array   $promotion
     * @param integer $quantity
     *
     * @return array
     */
    protected function createBasketPromotion(array $promotion): array
    {
        $promotion_model = clone $promotion['promotion'];
        $promotion_model->setRelations([]);

        return array_merge(
            $promotion_model->toArray(),
            [
                'applied' => $promotion['applied'] ?? false,
                'discount_value' => $promotion['value'] ?? 0,
                'human_discount' => $promotion['human_value'] ?? ($promotion['applied'] ? 'Applied' : 'Not Applied'),
            ]
        );
    }
}
