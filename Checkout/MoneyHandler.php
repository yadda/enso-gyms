<?php

namespace Yadda\Enso\Gyms\Checkout;

use Money\Currencies\ISOCurrencies;
use Money\Formatter\IntlMoneyFormatter;
use Money\Money;
use Money\Parser\AggregateMoneyParser;
use Money\Parser\DecimalMoneyParser;
use Money\Parser\IntlMoneyParser;

class MoneyHandler
{
    protected $parser;

    protected $formatter;

    public function __construct()
    {
        $currencies = new ISOCurrencies();
        $number_formatter = new \NumberFormatter(
            config('enso.gyms.mindbody.locale', 'th-TH'),
            \NumberFormatter::CURRENCY
        );

        $number_formatter->setTextAttribute(
            \NumberFormatter::CURRENCY_CODE,
            config('enso.gyms.mindbody.currency', 'THB')
        );

        $number_formatter->setAttribute(
            \NumberFormatter::MIN_FRACTION_DIGITS,
            config('enso.gyms.mindbody.money_min_fraction_digits', 0)
        );

        $intl_parser = new IntlMoneyParser($number_formatter, $currencies);
        $decimal_parser = new DecimalMoneyParser($currencies);
        $this->parser = new AggregateMoneyParser([
            $intl_parser,
            $decimal_parser,
        ]);

        $this->formatter = new IntlMoneyFormatter($number_formatter, $currencies);
    }

    /**
     * Converts an integer representing a currency's lowest denomination units
     * into a string representation of that Currency.

     *
     * @param integer $value
     * @param string $currency
     *
     * @return string
     */
    public function fromLowestDenomination(int $value, string $currency = null) : string
    {
        $currency = $currency ?? $this->getCurrency();

        return $this->formatter->format(Money::$currency($value));
    }

    /**
     * Convert a Representation of a currency into an integer of it's lowest
     * denomination units
     *
     * @param string $value
     * @param string $currency
     *
     * @return int
     */
    public function toLowestDenomination(string $value, string $currency = null) : int
    {
        $money = $this->parser->parse(
            $value,
            $currency ?? $this->getCurrency()
        );

        return (int) $money->getAmount();
    }

    /**
     * Get's the Site currency
     *
     * @return string
     */
    protected function getCurrency()
    {
        return config('enso.currency', 'GBP');
    }
}
