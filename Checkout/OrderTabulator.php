<?php

namespace Yadda\Enso\Gyms\Checkout;

use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Config;
use Yadda\Enso\Gyms\Checkout\MoneyHandler;
use Yadda\Enso\Gyms\Contracts\Order;

class OrderTabulator
{
    /**
     * Converts an Order instance into a rows representing a markdown table
     *
     * @param Order $order
     *
     * @return array
     */
    public static function convertOrderToMarkdownRows(Order $order) : array
    {
        $currency = Config::get('enso.gyms.mindbody.currency', 'THB');

        $money = App::make(MoneyHandler::class);

        $table = [
            '| Quantity | Item | Subtotal | Total |',
            '|:--------:|:----:|---------:|------:|',
        ];

        foreach ($order->items as $item) {
            // phpcs:ignore
            $table[] = "| " . $item->quantity .' | '
                . $item->saleable_name . ' | '
                . ($item->subtotal !== 0
                    ? $money->fromLowestDenomination($item->subtotal, $currency)
                    : '-') . ' | '
                . ($item->total !== 0
                    ? ($money->fromLowestDenomination($item->total, $currency))
                    : '-') . ' |';
        }

        $table[] = "| | | | {$money->fromLowestDenomination($order->total, $currency)} |";

        return $table;
    }
}
