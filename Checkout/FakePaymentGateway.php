<?php

namespace Yadda\Enso\Gyms\Checkout;

use Yadda\Enso\Gyms\Checkout\Charge;
use Yadda\Enso\Gyms\Checkout\PaymentGateway;

class FakePaymentGateway implements PaymentGateway
{
    /**
     * Gets information about a specific charge
     *
     * @param string $charge_id
     *
     * @return Charge
     */
    public function getCharge($charge_id) : Charge
    {
        return new Charge([
            'id' => $charge_id,
            'gateway' => 'fake-gateway',
            'amount' => 10000,
            'status' => 'paid',
        ]);
    }

    /**
     * Attempts to make a charge to the Omise gateway, based on the given parameters/
     *
     * @param string  $payment_token Payment token
     * @param integer $amount        Amount to charge (smallest currency denomination)
     * @param string  $currency      Currency to charge
     *
     * @return Charge
     */
    public function charge(string $payment_token, $amount) : Charge
    {
        return new Charge([
            'id' => 'valid-charge-id',
            'gateway' => 'fake-gateway',
            'amount' => $amount,
            'status' => 'paid',
        ]);
    }
}
