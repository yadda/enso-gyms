<?php

namespace Yadda\Enso\Gyms\Checkout;

use Exception;
use Illuminate\Contracts\Session\Session;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Auth;
use Yadda\Enso\Gyms\Contracts\Basket;
use Yadda\Enso\Gyms\Contracts\Order;
use Yadda\Enso\Gyms\Contracts\OrderItem;
use Yadda\Enso\Gyms\Contracts\Package;
use Yadda\Enso\Gyms\Exceptions\OrderGenerationException;

class OrderGenerator
{
    protected $session;

    protected $basket_state = [];

    protected $basket;

    public function __construct(Session $store)
    {
        $this->session = $store;

        $this->basket_state = $this->session->get('basket');
    }

    /**
     * Creates an Order and OrderItems from the current Basket state.
     *
     * @throws OrderGenerationException
     *
     * @return Order
     */
    public function createFromBasket(): Order
    {
        $this->basket = App::make(Basket::class);

        $this->validateState();

        $this->package_instance = App::make(Package::class);

        try {
            $order = $this->createOrder();

            $this->createItems($order);
        } catch (Exception $e) {
            $this->restoreSessionBasket();

            throw OrderGenerationException::creationFailed($e);
        }

        return $order;
    }

    /**
     * Ensures that current state of the system will allow for successful
     * order generation.
     *
     * @throws OrderGenerationException
     *
     * @return void
     */
    public function validateState()
    {
        if ($this->basket->isEmpty()) {
            throw OrderGenerationException::emptyBasket();
        };
    }

    /**
     * Creates an Order for the content of the basket.
     *
     * @return Order
     */
    protected function createOrder(): Order
    {
        $order = App::make(Order::class);

        $order->fill([
            'user_id' => Auth::id(),
            'status' => 'pending',
            'total' => $this->basket->getTotalPrice(),
            'discount' => 0,
        ])->save();

        return $order;
    }

    /**
     * Creates individual OrderItems for each element in the basket.
     *
     * @param Order $order
     *
     * @return void
     */
    protected function createItems(Order $order)
    {
        $order_items = $order->items;

        foreach ($this->basket->getPackages() as $package_data) {
            $order_items->push($this->createOrderItemFromPackage($order, $package_data));
        }

        foreach ($this->basket->getAppliedPromotions() as $promotion_data) {
            $order_items->push($this->createOrderItemFromPromotion($order, $promotion_data));
        }

        $order->setRelation('items', $order_items);

        $order->update(['discount' => $order->items->reduce(function ($carry, $item) {
            if ($item->total < 0) {
                return $carry - $item->total; // Convert all discounts to a positive number
            }

            return $carry;
        }, 0)]);
    }

    /**
     * Creates an individual OrderItem from the given package data.
     *
     * @param Order $order
     * @param array $package_data
     *
     * @return OrderItem
     */
    protected function createOrderItemFromPackage(Order $order, $package_data): OrderItem
    {
        $order_item = App::make(OrderItem::class);

        $package = $package_data['package'];

        $order_item->fill([
            'order_id' => $order->getKey(),
            'saleable_id' => $package->id,
            'saleable_type' => get_class($package),
            'name' => $package->name,
            'quantity' => $package_data['quantity'],
            'subtotal' => $package->price,
            'total' => $package_data['quantity'] * $package->price,
        ])->save();

        return $order_item;
    }

    /**
     * Creates an individual OrderItem from the given package data.
     *
     * @param Order $order
     * @param array $promotion_data
     *
     * @return OrderItem
     */
    protected function createOrderItemFromPromotion(Order $order, $promotion_data): OrderItem
    {
        $order_item = App::make(OrderItem::class);

        $promotion = $promotion_data['promotion'];

        $order_item->fill([
            'order_id' => $order->getKey(),
            'saleable_id' => $promotion->id,
            'saleable_type' => get_class($promotion),
            'name' => $promotion_data['human_value'],
            'quantity' => 1,
            'subtotal' => $promotion_data['value'],
            'total' => $promotion_data['value'],
        ])->save();

        return $order_item;
    }

    /**
     * Reverst the state of the basket
     *
     * @return void
     */
    protected function restoreSessionBasket()
    {
        $this->session->put('basket', $this->basket_state);
    }
}
