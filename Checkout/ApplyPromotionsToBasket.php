<?php

namespace Yadda\Enso\Gyms\Checkout;

use Illuminate\Support\Collection;
use Illuminate\Support\Facades\App;
use Yadda\Enso\Gyms\Contracts\Promotion;

class ApplyPromotionsToBasket
{
    protected $promotions;

    public function __construct(Collection $promotions)
    {
        $this->promotions = App::make(Promotion::class)->newCollection();

        $promotions->each(function ($promotion) {
            $this->promotions->put(
                $promotion['promotion']->getKey(),
                array_merge(
                    $promotion,
                    [
                        'promotion' => clone $promotion['promotion'],
                    ]
                )
            );
        });

        App::make(Promotion::class)->newCollection($promotions->map(function ($promotion) {
            return $promotion['promotion'];
        })->all())->load('packages');
    }

    /**
     * Static constuctor method.
     *
     * @param Collection $promotions
     *
     * @return \Yadda\Enso\Gyms\Checkout\ApplyPromotionsToBasket
     */
    public static function with(Collection $promotions)
    {
        return new static($promotions);
    }

    /**
     * Returns the duplicated promotions
     *
     * @return Collection
     */
    public function getPromotions(): Collection
    {
        return $this->promotions;
    }

    /**
     * Applies promotions from the promotions list to the given Basket Packages.
     *
     * @param Collection $basket_packages
     *
     * @return ApplyPromotionsToBasket
     */
    public function toPackages(Collection $basket_packages)
    {
        // Ensure that the collection can be altered (items pulled) so that
        // that multiple promotions can't be applied to the same packages
        $undiscounted_basket_packages = $basket_packages->map(function ($package) {
            return $package;
        });

        foreach ($this->promotions as $id => $promotion) {
            if ($promotion['applied'] === true) {
                continue;
            }

            switch ($promotion['promotion']->type) {
                case '1': // Free Class
                    if ($promotion['promotion']->packages->count() <= 0) {
                        $this->applyFreeClassPromotion($promotion);
                        break;
                    }

                    // Prevent multiple discounts applying to the same package(s)
                    $packages = $this->getMatchingPackages($undiscounted_basket_packages, $promotion['promotion']);
                    if ($packages->count()) {
                        $undiscounted_basket_packages->pull($packages->first()['package']->getKey());
                        $this->applyFreeClassPromotion($promotion);
                        break;
                    }

                    break; // No discount, only email addition required.
                default:
                    break; // Can not apply unknown Discount type
            }
        }

        return $this;
    }

    /**
     * Fills in a FreeClass type promotion
     *
     * @param array $promotion
     *
     * @return void
     */
    protected function applyFreeClassPromotion(array $promotion)
    {
        $this->promotions->put(
            $promotion['promotion']->getKey(),
            [
                'promotion' => $promotion['promotion'],
                'applied' => true,
                'value' => 0,
                'human_value' => $promotion['promotion']->value
                    ? ($promotion['promotion']->value . ' Free Class'
                        . (($promotion['promotion']->value == 1) ? '': 'es'))
                    : 'Applied'
            ]
        );
    }

    /**
     * Finds packages from a list that are also in a promotion's package list
     *
     * @param Collection $basket_packages
     * @param Promotion  $promotion
     *
     * @return void
     */
    protected function getMatchingPackages(Collection $basket_packages, Promotion $promotion)
    {
        return $basket_packages->intersectKey($promotion->packages->keyBy('id'));
    }
}
