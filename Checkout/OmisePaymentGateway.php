<?php

namespace Yadda\Enso\Gyms\Checkout;

use OmiseCharge;
use Yadda\Enso\Gyms\Checkout\Charge;
use Yadda\Enso\Gyms\Checkout\PaymentGateway;
use Yadda\Enso\Gyms\Exceptions\PaymentGatewayException;

class OmisePaymentGateway implements PaymentGateway
{
    /**
     * Gets information about a specific charge
     *
     * @param string $charge_id
     *
     * @return
     */
    public function getCharge($charge_id): Charge
    {
        return Charge::fromOmise(
            OmiseCharge::retrieve(
                $charge_id,
                $this->publicKey(),
                $this->secretKey()
            )
        );
    }

    /**
     * Attempts to make a charge to the Omise gateway, based on the given parameters/
     *
     * @param string  $payment_token Payment token
     * @param integer $amount        Amount to charge (smallest currency denomination)
     * @param string  $currency      Currency to charge
     *
     * @return Charge
     */
    public function charge(string $payment_token, $amount): Charge
    {
        try {
            $charge = OmiseCharge::create([
                'card' => $payment_token,
                'amount' => $amount,
                'currency' => 'thb',
            ], $this->publicKey(), $this->secretKey());
        } catch (OmiseInvalidChargeException $e) {
            throw PaymentGatewayException::failedCharge($e);
        } catch (OmiseFailedCaptureException $e) {
            throw PaymentGatewayException::failedCharge($e);
        } catch (OmiseFailedFraudCheckException $e) {
            throw PaymentGatewayException::failedCharge($e);
        } catch (OmiseInvalidRecipientException $e) {
            throw PaymentGatewayException::failedCharge($e);
        } catch (OmiseInvalidBankAccountException $e) {
            throw PaymentGatewayException::failedCharge($e);
        } catch (Exception $e) {
            throw PaymentGatewayException::unknownError($e);
        }

        return Charge::fromOmise($charge);
    }

    /**
     * Gets the public key
     *
     * @return string
     */
    protected function publicKey(): string
    {
        return config('services.omise.key');
    }

    /**
     * Gets the secret key
     *
     * @return string
     */
    protected function secretKey(): string
    {
        return config('services.omise.secret');
    }

    /**
     * Gets the API version
     *
     * @return string
     */
    protected function apiVersion(): string
    {
        return config('services.omise.version');
    }
}
