<?php

namespace Yadda\Enso\Gyms\Checkout;

use OmiseCharge;

class Charge
{
    public $id;
    public $gateway;
    public $amount;
    public $status;

    public function __construct($attributes = [])
    {
        foreach ($attributes as $attribute => $value) {
            if (property_exists($this, $attribute)) {
                $this->$attribute = $value;
            }
        }
    }

    /**
     * Creates a new Charge instance from an OmiseCharge
     *
     * @param OmiseCharge $omise_charge
     *
     * @return Charge
     */
    public static function fromOmise(OmiseCharge $omise_charge) : Charge
    {
        return new static([
            'id' => $omise_charge['id'],
            'gateway' => 'omise',
            'amount' => $omise_charge['amount'],
            'status' => static::omiseChargeStatus($omise_charge),
        ]);
    }

    /**
     * Gets the charge Status from an Omise Charge
     *
     * @param OmiseCharge $omise_charge
     *
     * @return string
     */
    protected static function omiseChargeStatus(OmiseCharge $omise_charge) : string
    {
        if ($omise_charge['paid']) {
            return 'paid';
        }

        if ($omise_charge['expired']) {
            return 'expired';
        }

        return 'pending';
    }
}
