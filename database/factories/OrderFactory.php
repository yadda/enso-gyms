<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use Faker\Generator as Faker;
use Yadda\Enso\Gyms\Models\Order;

$factory->define(Order::class, function (Faker $faker) {
    return [
        'uuid' => $faker->uuid,
        'status' => 'paid',
    ];
});
