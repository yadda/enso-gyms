<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddsClassTypesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('enso_gyms_class_types', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('mb_class_description_id')->unique();
            $table->string('slug')->unique();
            $table->boolean('published')->index()->default(false);
            $table->dateTime('publish_at')->index()->nullable();
            $table->unsignedInteger('order')->index()->nullable();
            $table->unsignedInteger('session_type_id')->index()->nullable();
            $table->unsignedInteger('program_id')->index()->nullable();
            $table->unsignedInteger('image_id')->nullable();
            $table->string('name')->nullable();
            $table->text('description')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('enso_gyms_class_types');
    }
}
