<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddInstructorsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('enso_gyms_instructors', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('mb_staff_id')->unique();
            $table->string('slug')->unique();
            $table->boolean('published')->index()->default(false);
            $table->dateTime('publish_at')->index()->nullable();
            $table->unsignedInteger('order')->index()->nullable();
            $table->unsignedInteger('image_id')->nullable();
            $table->string('name')->nullable();
            $table->string('first_name')->nullable();
            $table->string('last_name')->nullable();
            $table->text('description')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('enso_gyms_instructors');
    }
}
