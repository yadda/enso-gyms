<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddsClassCountToPackages extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('enso_gyms_packages', function (Blueprint $table) {
            $table->integer('class_count')->nullable()->after('name');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('enso_gyms_packages', function (Blueprint $table) {
            $table->dropColumn(['class_count']);
        });
    }
}
