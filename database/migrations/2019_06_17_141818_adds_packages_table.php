<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddsPackagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('enso_gyms_packages', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('mb_service_id')->unique();
            $table->string('slug')->unique();
            $table->boolean('published')->index()->default(false);
            $table->dateTime('publish_at')->index()->nullable();
            $table->decimal('price', 16, 4)->index()->nullable();
            $table->unsignedInteger('order')->index()->nullable();
            $table->unsignedInteger('image_id')->nullable();
            $table->string('name')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('enso_gyms_packages');
    }
}
