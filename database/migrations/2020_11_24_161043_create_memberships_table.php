<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMembershipsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('enso_gyms_memberships', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('mindbody_id')->nullable()->index();
            $table->string('name')->nullable();
            $table->string('slug')->nullable()->index()->unique();
            $table->boolean('allows_access')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('memberships');
    }
}
