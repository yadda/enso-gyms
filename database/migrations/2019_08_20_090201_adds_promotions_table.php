<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddsPromotionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('enso_gyms_promotions', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('type')->nullable()->index();
            $table->boolean('published')->default(false);
            $table->dateTime('publish_at')->index()->nullable();
            $table->string('code')->nullable()->index();
            $table->datetime('expires_on')->nullable()->index();
            $table->unsignedInteger('limit')->nullable()->index();
            $table->unsignedInteger('used')->default(0)->index();
            $table->unsignedInteger('order')->defaut(0)->index();
            $table->unsignedInteger('max_per_basket')->default(1);
            $table->string('name')->nullable();
            $table->unsignedInteger('value')->default(0);
            $table->timestamps();
        });

        Schema::create('enso_gyms_package_promotion', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('package_id')->index();
            $table->unsignedInteger('promotion_id')->index();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('enso_gyms_package_promotion');

        Schema::dropIfExists('enso_gyms_promotions');
    }
}
