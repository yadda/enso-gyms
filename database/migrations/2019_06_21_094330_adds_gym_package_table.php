<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddsGymPackageTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('enso_gyms_gym_package', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('gym_id')->index();
            $table->unsignedInteger('package_id')->index();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('enso_gyms_gym_package');
    }
}
