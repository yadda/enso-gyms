<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;
use Yadda\Enso\Gyms\Contracts\Order;
use Yadda\Enso\Gyms\Contracts\OrderItem;
use Yadda\Enso\Gyms\Contracts\Package;

class ChangesCurrencyFieldsToIntegeres extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('enso_gyms_packages', function (Blueprint $table) {
            $table->unsignedInteger('price')->nullable()->change();
        });

        $table = resolve(Package::class)->getTable();
        DB::table($table)->update(['price' => DB::raw('price * 100')]);

        Schema::table('enso_gyms_orders', function (Blueprint $table) {
            $table->unsignedInteger('total')->nullable()->change();
            $table->unsignedInteger('discount')->nullable()->change();
        });

        $table = resolve(Order::class)->getTable();
        DB::table($table)->update([
            'total' => DB::raw('total * 100'),
            'discount' => DB::raw('discount * 100')
        ]);

        Schema::table('enso_gyms_order_items', function (Blueprint $table) {
            $table->unsignedInteger('subtotal')->nullable()->change();
            $table->unsignedInteger('total')->nullable()->change();
        });

        $table = resolve(OrderItem::class)->getTable();
        DB::table($table)->update([
            'subtotal' => DB::raw('subtotal * 100'),
            'total' => DB::raw('total * 100')
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('enso_gyms_packages', function (Blueprint $table) {
            $table->decimal('price', 16, 4)->nullable()->change();
        });

        $table = resolve(Package::class)->getTable();
        DB::table($table)->update(['price' => DB::raw('price / 100')]);

        Schema::table('enso_gyms_orders', function (Blueprint $table) {
            $table->decimal('total', 32, 4)->nullable()->change();
            $table->decimal('discount', 32, 4)->nullable()->change();
        });

        $table = resolve(Order::class)->getTable();
        DB::table($table)->update([
            'total' => DB::raw('total / 100'),
            'discount' => DB::raw('discount / 100')
        ]);

        Schema::table('enso_gyms_order_items', function (Blueprint $table) {
            $table->decimal('subtotal', 32, 4)->nullable()->change();
            $table->decimal('total', 32, 4)->nullable()->change();
        });

        $table = resolve(OrderItem::class)->getTable();
        DB::table($table)->update([
            'subtotal' => DB::raw('subtotal / 100'),
            'total' => DB::raw('total / 100')
        ]);
    }
}
