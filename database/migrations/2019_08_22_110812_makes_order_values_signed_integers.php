<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class MakesOrderValuesSignedIntegers extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('enso_gyms_orders', function (Blueprint $table) {
            $table->integer('total')->nullable()->change();
            $table->integer('discount')->nullable()->change();
        });

        Schema::table('enso_gyms_order_items', function (Blueprint $table) {
            $table->integer('subtotal')->nullable()->change();
            $table->integer('total')->nullable()->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('enso_gyms_order_items', function (Blueprint $table) {
            $table->unsignedInteger('subtotal')->nullable()->change();
            $table->unsignedInteger('total')->nullable()->change();
        });

        Schema::table('enso_gyms_orders', function (Blueprint $table) {
            $table->unsignedInteger('total')->nullable()->change();
            $table->unsignedInteger('discount')->nullable()->change();
        });
    }
}
