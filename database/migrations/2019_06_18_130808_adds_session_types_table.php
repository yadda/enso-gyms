<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddsSessionTypesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('enso_gyms_session_types', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('mb_session_type_id')->unique();
            $table->string('slug')->unique();
            $table->boolean('published')->index()->default(false);
            $table->dateTime('publish_at')->index()->nullable();
            $table->unsignedInteger('order')->index();
            $table->string('name')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('enso_gyms_session_types');
    }
}
