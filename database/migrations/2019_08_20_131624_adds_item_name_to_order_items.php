<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddsItemNameToOrderItems extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('enso_gyms_order_items', function (Blueprint $table) {
            $table->string('name')->nullable()->after('saleable_type');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('enso_gyms_order_items', function (Blueprint $table) {
            $table->dropColumn(['name']);
        });
    }
}
