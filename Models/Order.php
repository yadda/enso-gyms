<?php

namespace Yadda\Enso\Gyms\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Arr;
use Yadda\Enso\Crud\Traits\HasUuids;
use Yadda\Enso\Gyms\Contracts\Order as OrderContract;
use Yadda\Enso\Gyms\Contracts\OrderItem;
use Yadda\Enso\Users\Contracts\User;

class Order extends Model implements OrderContract
{
    use HasUuids;

    protected $table = "enso_gyms_orders";

    protected $uuid_column = 'uuid';

    protected $fillable = [
        'uuid',
        'user_id',
        'status',
        'payment_method',
        'payment_token',
        'total',
        'discount',
        'billing_name',
        'billing_company',
        'billing_email',
        'billing_phone',
        'billing_address_1',
        'billing_address_2',
        'billing_city',
        'billing_state',
        'billing_postcode',
        'billing_country',
        'notes',
        'completed_at',
        'completed_at_bool',
        'notes',
    ];

    protected $casts = [
        'completed_at' => 'datetime',
    ];

    protected $dates = [
        'completed_at',
    ];

    public function items()
    {
        return $this->hasMany(resolve(OrderItem::class));
    }

    public function user()
    {
        return $this->belongsTo(resolve(User::class), 'user_id');
    }

    public function getCompletedAtBoolAttribute()
    {
        return isset($this->attributes['completed_at'])
            && !is_null($this->attributes['completed_at']);
    }

    public function setCompletedAtBoolAttribute($value)
    {
        if (!$value) {
            $this->attributes['completed_at'] = null;
        } else {
            if (is_null($this->attributes['completed_at'])) {
                $this->attributes['completed_at'] = Carbon::now();
            }
        }
    }

    /**
     * Adds billing information to this order based on the given Billing
     * model
     *
     * @param array $billing
     *
     * @return self
     */
    public function addBillingInformation(array $billing)
    {
        $this->update(Arr::only($billing, [
            'billing_name',
            'billing_company',
            'billing_email',
            'billing_phone',
            'billing_address_1',
            'billing_address_2',
            'billing_city',
            'billing_state',
            'billing_postcode',
            'billing_country',
        ]));

        return $this;
    }

    /**
     * Limits an Order query to only incomplete orders
     *
     * @param Builder $builder
     *
     * @return Builder
     */
    public function scopeNotCompleted(Builder $builder)
    {
        return $builder->whereNull('completed_at');
    }

    /**
     * Limits an Order query to only completed orders
     *
     * @param Builder $builder
     *
     * @return Builder
     */
    public function scopeCompleted(Builder $builder)
    {
        return $builder->whereNotNull('completed_at');
    }

    /**
     * Limits an Order query to only paid orders
     *
     * @param Builder $builder
     *
     * @return Builder
     */
    public function scopePaid(Builder $builder)
    {
        return $builder->where('status', 'paid');
    }

    /**
     * Limits an Order query to only unpaid orders
     *
     * @param Builder $builder
     *
     * @return Builder
     */
    public function scopeUnpaid(Builder $builder)
    {
        return $builder->where('status', '!=', 'paid');
    }
}
