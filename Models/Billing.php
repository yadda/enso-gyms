<?php

namespace Yadda\Enso\Gyms\Models;

use Illuminate\Database\Eloquent\Model;
use Yadda\Enso\Users\Contracts\User;

class Billing extends Model
{
    protected $table = "enso_gyms_billing";

    protected $fillable = [
        'user_id',
        'billing_name',
        'billing_company',
        'billing_email',
        'billing_phone',
        'billing_address_1',
        'billing_address_2',
        'billing_city',
        'billing_state',
        'billing_postcode',
        'billing_country',
        'billing_meta',
    ];

    protected $casts = [
        'billing_meta' => 'array',
    ];

    public function user()
    {
        return $this->belongsTo(resolve(User::class));
    }
}
