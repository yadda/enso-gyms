<?php

namespace Yadda\Enso\Gyms\Models;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\DB;
use Yadda\Enso\Crud\Contracts\IsPublishable as IsPublishableInterface;
use Yadda\Enso\Crud\Traits\IsPublishable;
use Yadda\Enso\Gyms\Contracts\Package;
use Yadda\Enso\Gyms\Contracts\Promotion as PromotionContract;

class Promotion extends Model implements PromotionContract, IsPublishableInterface
{
    use IsPublishable;

    protected $table = "enso_gyms_promotions";

    protected $fillable = [
        'type',
        'published',
        'publish_at',
        'code',
        'expires_on',
        'limit',
        'used',
        'order',
        'max_per_basket',
        'name',
        'value',
    ];

    protected $casts = [
        'published' => 'boolean',
        'publish_at' => 'boolean',
        'expires_on' => 'datetime',
    ];

    protected $dates = [
        'expires_on',
        'publish_at',
    ];

    const PROMOTION_TYPES = [
        1 => [
            'name' => 'Free Class',
        ],
    ];

    public static function getTypeOptions()
    {
        return array_map(function ($index, $value) {
            return [
                'id' => $index,
                'name' => Arr::get($value, 'name'),
            ];
        }, array_keys(static::PROMOTION_TYPES), static::PROMOTION_TYPES);
    }

    public function packages()
    {
        return $this->belongsToMany(resolve(Package::class), 'enso_gyms_package_promotion');
    }

    /**
     * Limits a Promotion query to only those that have uses remaining
     *
     * @param Builder $builder
     *
     * @return Builder
     */
    public function scopeIsRedeemable(Builder $builder)
    {
        return $builder->accessibleToUser()->where(function ($builder) {
            return $builder->whereNull('limit')
                ->orWhere('limit', '>', DB::raw('`used`'));
        })->whereNotNull('code');
    }
}
