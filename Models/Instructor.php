<?php

namespace Yadda\Enso\Gyms\Models;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Yadda\Enso\Crud\Traits\IsPublishable;
use Yadda\Enso\Gyms\Contracts\Instructor as InstructorContract;
use Yadda\Enso\Media\Contracts\ImageFile;
use Yadda\Enso\Media\Traits\HasFilesTrait;
use Yadda\Enso\Meta\Traits\HasMeta;

class Instructor extends Model implements InstructorContract
{
    use HasMeta, IsPublishable, HasFilesTrait;

    protected $table = "enso_gyms_instructors";

    protected $fillable = [
        'mb_staff_id',
        'slug',
        'published',
        'publish_at',
        'order',
        'image_id',
        'name',
        'first_name',
        'last_name',
        'description',
    ];

    protected $casts = [
        'published' => 'boolean',
        'publish_at' => 'datetime',
    ];

    protected $dates = [
        'publish_at'
    ];

    /**
     * Gets the name of the Publish At DateTime column on this publishable
     *
     * @return string|null
     */
    public function getPublishAtColumn()
    {
        return 'publish_at';
    }

    /**
     * @todo - Enso currently doesn't support this without a fix. Once that has
     * been added, we can uncomment this and remove the frontend scope from
     * frontend of the site.
     */
    // /**
    //  * The "booting" method of the model.
    //  *
    //  * @return void
    //  */
    // protected static function boot()
    // {
    //     parent::boot();

    //     static::addGlobalScope('accessibleToUser', function (Builder $builder) {
    //         return $builder->accessibleToUser();
    //     });
    // }

    /**
     * An image of this person
     *
     * @return BelongsTo
     */
    public function image()
    {
        return $this->belongsTo(resolve(ImageFile::class), 'image_id');
    }

    /**
     * Gets the CMS index page thumbnail url
     *
     * @return string
     */
    public function getCmsThumbnailAttribute()
    {
        return $this->image ? $this->image->getResizeUrl('uploader_preview') : null;
    }

    /**
     * Modifies the query to order by the Order column (default).
     *
     * @param Builder $builder
     *
     * @return Builder
     */
    public function scopeFrontend(Builder $builder)
    {
        return $builder->accessibleToUser();
    }

    /**
     * Modifies the query to order by the Order column (default).
     *
     * @param Builder $builder
     *
     * @return Builder
     */
    public function scopeOrdered(Builder $builder)
    {
        return $builder->orderBy('order', 'DESC');
    }
}
