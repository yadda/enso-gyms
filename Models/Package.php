<?php

namespace Yadda\Enso\Gyms\Models;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\App;
use Yadda\Enso\Crud\Traits\IsPublishable;
use Yadda\Enso\Facades\MoneyFormatter;
use Yadda\Enso\Gyms\Checkout\MoneyHandler;
use Yadda\Enso\Gyms\Contracts\Gym;
use Yadda\Enso\Gyms\Contracts\Package as PackageContract;
use Yadda\Enso\Media\Contracts\ImageFile;
use Yadda\Enso\Media\Traits\HasFilesTrait;
use Yadda\Enso\Meta\Traits\HasMeta;

class Package extends Model implements PackageContract
{
    use HasMeta, IsPublishable, HasFilesTrait;

    /**
     * Describes the criteria for successfuly publishing a Package.
     *
     * @var string
     */
    public $publish_criteria = "Packages require at least one assigned Gym and an image to publish";

    protected $table = "enso_gyms_packages";

    protected $fillable = [
        'mb_service_id',
        'slug',
        'published',
        'publish_at',
        'price',
        'order',
        'image_id',
        'name',
        'class_count',
    ];

    protected $appends = [
        'cost_per_class',
        'human_price',
    ];

    protected $casts = [
        'published' => 'boolean',
        'publish_at' => 'datetime',
    ];

    protected $dates = [
        'publish_at'
    ];

    /**
     * @todo - Enso currently doesn't support this without a fix. Once that has
     * been added, we can uncomment this and remove the frontend scope from
     * frontend of the site.
     */
    // /**
    //  * The "booting" method of the model.
    //  *
    //  * @return void
    //  */
    // protected static function boot()
    // {
    //     parent::boot();

    //     static::addGlobalScope('accessibleToUser', function (Builder $builder) {
    //         return $builder->accessibleToUser();
    //     });
    // }

    /**
     * An image of this person
     *
     * @return BelongsTo
     */
    public function image()
    {
        return $this->belongsTo(resolve(ImageFile::class), 'image_id');
    }

    /**
     * Gets the Gyms to which this Package has been directly assigned.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function gyms()
    {
        return $this->belongsToMany(resolve(Gym::class), 'enso_gyms_gym_package')->withTimestamps();
    }

    /**
     * Gets the CMS index page thumbnail url
     *
     * @return string
     */
    public function getCmsThumbnailAttribute()
    {
        return $this->image ? $this->image->getResizeUrl('uploader_preview') : null;
    }

    /**
     * Gets the Human readable version of the package prices, based on the site
     * currency.
     *
     * @return string
     */
    public function getHumanPriceAttribute(): string
    {
        if (is_null($this->price)) {
            return '';
        }

        return App::make(MoneyHandler::class)->fromLowestDenomination(
            (int) $this->price,
            config('enso.gyms.mindbody.currency', 'THB')
        );
    }

    /**
     * Determines whether this instance can be published.
     *
     * @return boolean
     */
    public function canPublish()
    {
        return $this->image && ($this->gyms()->count() >= 1);
    }

    public function isUnlimitedPackage()
    {
        // Setting a Service as unlimited usage in MindBody results in a service
        // with 99999 for it's class count (at least, via the API).
        return $this->class_count === 99999;
    }

    /**
     * Returns the cost per class for this Package
     *
     * @return float|null
     */
    public function getCostPerClassAttribute()
    {
        if ($this->isUnlimitedPackage() || $this->price <= 0 || $this->class_count <= 0) {
            return null;
        }

        return $this->price / $this->class_count;
    }

    /**
     * Returns the cost per class, formatted with the MindBody integrations
     * currency format.
     *
     * @return string
     */
    public function getCostPerClassString() : string
    {
        if (is_null($this->cost_per_class)) {
            return '';
        }

        return App::make(MoneyHandler::class)->fromLowestDenomination(
            (int) $this->cost_per_class,
            config('enso.gyms.mindbody.currency', 'THB')
        );
    }

    /**
     * Modifies the query to order by the Order column (default).
     *
     * @param Builder $builder
     *
     * @return Builder
     */
    public function scopeFrontend(Builder $builder)
    {
        return $builder->accessibleToUser();
    }

    /**
     * Modifies the query to order by the Order column (default).
     *
     * @param Builder $builder
     *
     * @return Builder
     */
    public function scopeOrdered(Builder $builder)
    {
        return $builder->orderBy('order', 'DESC');
    }

    /**
     * Undocumented function
     *
     * @param Builder $builder
     * @return Builder
     */
    public function scopeForGym(Builder $builder, Gym $gym) : Builder
    {
        return $builder->whereHas('gyms', function ($builder) use ($gym) {
            return $builder->where($gym->getQualifiedKeyName(), $gym->getKey());
        });
    }
}
