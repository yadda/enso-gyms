<?php

namespace Yadda\Enso\Gyms\Models;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Yadda\Enso\Crud\Traits\IsPublishable;
use Yadda\Enso\Gyms\Contracts\ClassType;
use Yadda\Enso\Gyms\Contracts\SessionType as SessionTypeContract;

class SessionType extends Model implements SessionTypeContract
{
    use IsPublishable;

    protected $table = "enso_gyms_session_types";

    protected $fillable = [
        'mb_session_type_id',
        'slug',
        'published',
        'publish_at',
        'order',
        'name',
    ];

    protected $casts = [
        'published' => 'boolean',
        'publish_at' => 'datetime',
    ];

    /**
     * @todo - Enso currently doesn't support this without a fix. Once that has
     * been added, we can uncomment this and remove the frontend scope from
     * frontend of the site.
     */
    // /**
    //  * The "booting" method of the model.
    //  *
    //  * @return void
    //  */
    // protected static function boot()
    // {
    //     parent::boot();

    //     static::addGlobalScope('accessibleToUser', function (Builder $builder) {
    //         return $builder->accessibleToUser();
    //     });
    // }

    /**
     * The classes that are in this session type
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function classTypes()
    {
        return $this->HasMany(resolve(ClassType::class));
    }

    /**
     * Modifies the query to order by the Order column (default).
     *
     * @param Builder $builder
     *
     * @return Builder
     */
    public function scopeFrontend(Builder $builder)
    {
        return $builder->accessibleToUser();
    }

    /**
     * Modifies the query to order by the Order column (default).
     *
     * @param Builder $builder
     *
     * @return Builder
     */
    public function scopeOrdered(Builder $builder)
    {
        return $builder->orderBy('order', 'DESC');
    }
}
