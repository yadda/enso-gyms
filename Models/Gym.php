<?php

namespace Yadda\Enso\Gyms\Models;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Yadda\Enso\Crud\Traits\IsPublishable;
use Yadda\Enso\Gyms\Contracts\Gym as GymContract;
use Yadda\Enso\Gyms\Contracts\Package;
use Yadda\Enso\Meta\Traits\HasMeta;

class Gym extends Model implements GymContract
{
    use HasMeta, IsPublishable;

    protected $table = "enso_gyms_gyms";

    protected $fillable = [
        'mb_location_id',
        'slug',
        'published',
        'publish_at',
        'order',
        'longitude',
        'latitude',
        'name',
        'email',
        'phone',
        'address',
        'address_2',
        'city',
        'province',
        'post_code',
        'description',
    ];

    protected $casts = [
        'published' => 'boolean',
        'publish_at' => 'datetime',
        'longitude' => 'double',
        'latitude' => 'double',
    ];

    protected $dates = [
        'publish_at'
    ];

    /**
     * @todo - Enso currently doesn't support this without a fix. Once that has
     * been added, we can uncomment this and remove the frontend scope from
     * frontend of the site.
     */
    // /**
    //  * The "booting" method of the model.
    //  *
    //  * @return void
    //  */
    // protected static function boot()
    // {
    //     parent::boot();

    //     static::addGlobalScope('accessibleToUser', function (Builder $builder) {
    //         return $builder->accessibleToUser();
    //     });
    // }

    /**
     * Gets the packages that have been directly assigned to this Gym
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function packages()
    {
        return $this->belongsToMany(resolve(Package::class), 'enso_gyms_gym_package')->withTimestamps();
    }

    /**
     * Modifies the query to order by the Order column (default).
     *
     * @param Builder $builder
     *
     * @return Builder
     */
    public function scopeFrontend(Builder $builder)
    {
        return $builder->accessibleToUser();
    }

    /**
     * Modifies the query to order by the Order column (default).
     *
     * @param Builder $builder
     *
     * @return Builder
     */
    public function scopeOrdered(Builder $builder)
    {
        return $builder->orderBy('order', 'DESC');
    }
}
