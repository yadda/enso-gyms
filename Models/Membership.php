<?php

namespace Yadda\Enso\Gyms\Models;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Yadda\Enso\Crud\Traits\IsCrudModel;
use Yadda\Enso\Gyms\Contracts\Membership as MembershipContract;

class Membership extends Model implements MembershipContract
{
    use IsCrudModel;

    protected $table = "enso_gyms_memberships";

    protected $fillable = [
        'mindbody_id',
        'name',
        'slug',
        'allows_access',
    ];

    /**
     * Modifies the query to order by the Order column (default).
     */
    public function scopeFrontend(Builder $query): void
    {
        $query->accessibleToUser();
    }

    /**
     * Membership provides access to content
     */
    public function scopeAllowsAccess(Builder $query): void
    {
        $query->where('allows_access', 1);
    }
}
