<?php

namespace Yadda\Enso\Gyms\Models;

use Illuminate\Database\Eloquent\Model;
use Yadda\Enso\Gyms\Contracts\Order;
use Yadda\Enso\Gyms\Contracts\OrderItem as OrderItemContract;

class OrderItem extends Model implements OrderItemContract
{
    protected $table = "enso_gyms_order_items";

    protected $fillable = [
        'order_id',
        'saleable_id',
        'saleable_type',
        'name',
        'quantity',
        'subtotal',
        'total',
    ];

    public function order()
    {
        return $this->belongsTo(resolve(Order::class));
    }

    public function saleable()
    {
        return $this->morphTo();
    }

    public function getSaleableNameAttribute()
    {
        return $this->saleable ? $this->saleable->name : $this->name;
    }
}
