<?php

namespace Yadda\Enso\Gyms\Models;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Yadda\Enso\Crud\Traits\IsPublishable;
use Yadda\Enso\Gyms\Contracts\ClassType as ClassTypeContract;
use Yadda\Enso\Gyms\Contracts\SessionType;
use Yadda\Enso\Media\Contracts\ImageFile;
use Yadda\Enso\Media\Traits\HasFilesTrait;
use Yadda\Enso\Meta\Traits\HasMeta;

class ClassType extends Model implements ClassTypeContract
{
    use HasMeta, IsPublishable, HasFilesTrait;

    protected $table = "enso_gyms_class_types";

    protected $fillable = [
        'mb_class_description_id',
        'slug',
        'published',
        'publish_at',
        'order',
        'session_type_id',
        'program_id',
        'image_id',
        'name',
        'description',
    ];

    protected $casts = [
        'published' => 'boolean',
        'publish_at' => 'datetime',
    ];

    protected $dates = [
        'publish_at'
    ];

    /**
     * @todo - Enso currently doesn't support this without a fix. Once that has
     * been added, we can uncomment this and remove the frontend scope from
     * frontend of the site.
     */
    // /**
    //  * The "booting" method of the model.
    //  *
    //  * @return void
    //  */
    // protected static function boot()
    // {
    //     parent::boot();

    //     static::addGlobalScope('accessibleToUser', function (Builder $builder) {
    //         return $builder->accessibleToUser();
    //     });
    // }

    /**
     * An image of this person
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function image()
    {
        return $this->belongsTo(resolve(ImageFile::class), 'image_id');
    }

    /**
     * Gets the CMS index page thumbnail url
     *
     * @return string
     */
    public function getCmsThumbnailAttribute()
    {
        return $this->image ? $this->image->getResizeUrl('uploader_preview') : null;
    }

    /**
     * The Session type that this class type belongs to.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function sessionType()
    {
        return $this->belongsTo(resolve(SessionType::class));
    }

    /**
     * Modifies the query to order by the Order column (default).
     *
     * @param Builder $builder
     *
     * @return Builder
     */
    public function scopeFrontend(Builder $builder)
    {
        return $builder->accessibleToUser();
    }

    /**
     * Modifies the query to order by the Order column (default).
     *
     * @param Builder $builder
     *
     * @return Builder
     */
    public function scopeOrdered(Builder $builder)
    {
        return $builder->orderBy('order', 'DESC');
    }
}
