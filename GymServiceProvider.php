<?php

namespace Yadda\Enso\Gyms;

use Illuminate\Database\Eloquent\Factory;
use Illuminate\Foundation\AliasLoader;
use Illuminate\Support\ServiceProvider;
use Yadda\Enso\Gyms\Checkout\Basket;
use Yadda\Enso\Gyms\Contracts\Basket as BasketContract;
use Yadda\Enso\Gyms\Contracts\BasketController as BasketControllerContract;
use Yadda\Enso\Gyms\Contracts\CaseStudy as CaseStudyContract;
use Yadda\Enso\Gyms\Contracts\CaseStudyController as CaseStudyControllerContract;
use Yadda\Enso\Gyms\Contracts\CaseStudyCrud as CaseStudyCrudContract;
use Yadda\Enso\Gyms\Contracts\CheckoutController as CheckoutControllerContract;
use Yadda\Enso\Gyms\Contracts\ClassType as ClassTypeContract;
use Yadda\Enso\Gyms\Contracts\ClassTypeController as ClassTypeControllerContract;
use Yadda\Enso\Gyms\Contracts\ClassTypeCrud as ClassTypeCrudContract;
use Yadda\Enso\Gyms\Contracts\Gym as GymContract;
use Yadda\Enso\Gyms\Contracts\GymController as GymControllerContract;
use Yadda\Enso\Gyms\Contracts\GymCrud as GymCrudContract;
use Yadda\Enso\Gyms\Contracts\Instructor as InstructorContract;
use Yadda\Enso\Gyms\Contracts\InstructorController as InstructorControllerContract;
use Yadda\Enso\Gyms\Contracts\InstructorCrud as InstructorCrudContract;
use Yadda\Enso\Gyms\Contracts\Membership as MembershipContract;
use Yadda\Enso\Gyms\Contracts\MembershipController as MembershipControllerContract;
use Yadda\Enso\Gyms\Contracts\MembershipCrud as MembershipCrudContract;
use Yadda\Enso\Gyms\Contracts\Order as OrderContract;
use Yadda\Enso\Gyms\Contracts\OrderController as OrderControllerContract;
use Yadda\Enso\Gyms\Contracts\OrderCrud as OrderCrudContract;
use Yadda\Enso\Gyms\Contracts\OrderItem as OrderItemContract;
use Yadda\Enso\Gyms\Contracts\Package as PackageContract;
use Yadda\Enso\Gyms\Contracts\PackageController as PackageControllerContract;
use Yadda\Enso\Gyms\Contracts\PackageCrud as PackageCrudContract;
use Yadda\Enso\Gyms\Contracts\Promotion as PromotionContract;
use Yadda\Enso\Gyms\Contracts\PromotionController as PromotionControllerContract;
use Yadda\Enso\Gyms\Contracts\PromotionCrud as PromotionCrudContract;
use Yadda\Enso\Gyms\Contracts\SessionType as SessionTypeContract;
use Yadda\Enso\Gyms\Contracts\SessionTypeController as SessionTypeControllerContract;
use Yadda\Enso\Gyms\Contracts\SessionTypeCrud as SessionTypeCrudContract;
use Yadda\Enso\Gyms\Controllers\Admin\CaseStudyController;
use Yadda\Enso\Gyms\Controllers\Admin\ClassTypeController;
use Yadda\Enso\Gyms\Controllers\Admin\GymController;
use Yadda\Enso\Gyms\Controllers\Admin\InstructorController;
use Yadda\Enso\Gyms\Controllers\Admin\MembershipController;
use Yadda\Enso\Gyms\Controllers\Admin\OrderController;
use Yadda\Enso\Gyms\Controllers\Admin\PackageController;
use Yadda\Enso\Gyms\Controllers\Admin\PromotionController;
use Yadda\Enso\Gyms\Controllers\Admin\SessionTypeController;
use Yadda\Enso\Gyms\Controllers\Json\BasketController;
use Yadda\Enso\Gyms\Controllers\Json\CheckoutController;
use Yadda\Enso\Gyms\Crud\CaseStudy as CaseStudyCrud;
use Yadda\Enso\Gyms\Crud\ClassType as ClassTypeCrud;
use Yadda\Enso\Gyms\Crud\Gym as GymCrud;
use Yadda\Enso\Gyms\Crud\Instructor as InstructorCrud;
use Yadda\Enso\Gyms\Crud\Membership as MembershipCrud;
use Yadda\Enso\Gyms\Crud\Order as OrderCrud;
use Yadda\Enso\Gyms\Crud\Package as PackageCrud;
use Yadda\Enso\Gyms\Crud\Promotion as PromotionCrud;
use Yadda\Enso\Gyms\Crud\SessionType as SessionTypeCrud;
use Yadda\Enso\Gyms\EnsoGyms;
use Yadda\Enso\Gyms\Facades\EnsoGyms as EnsoGymsFacade;
use Yadda\Enso\Gyms\MindBody\Facades\MindBody as MindBodyFacade;
use Yadda\Enso\Gyms\MindBody\MindBodyFactory;
use Yadda\Enso\Gyms\MindBody\V6\Api as MindbodyApiV6;
use Yadda\Enso\Gyms\Models\CaseStudy;
use Yadda\Enso\Gyms\Models\ClassType;
use Yadda\Enso\Gyms\Models\Gym;
use Yadda\Enso\Gyms\Models\Instructor;
use Yadda\Enso\Gyms\Models\Membership;
use Yadda\Enso\Gyms\Models\Order;
use Yadda\Enso\Gyms\Models\OrderItem;
use Yadda\Enso\Gyms\Models\Package;
use Yadda\Enso\Gyms\Models\Promotion;
use Yadda\Enso\Gyms\Models\SessionType;

class GymServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        $this->publishes([
            __DIR__.'/database/migrations/' => database_path('migrations'),
            __DIR__.'/config/gyms/' => config_path('enso/gyms/'),
        ], 'enso-gyms');

        $this->loadViewsFrom(__DIR__ . '/resources/views/crud', 'enso-crud');
        $this->loadViewsFrom(__DIR__ . '/resources/views/notifications', 'notifications');

        $this->publishes([
            __DIR__ . '/resources/views/notifications' => resource_path('views/vendor/notifications'),
        ], 'config');

        $this->mergeConfigFrom(
            __DIR__.'/config/crud.php',
            'enso.crud'
        );

        $this->app->make(Factory::class)->load(__DIR__.'/database/factories');
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->singleton('enso-gyms', function () {
            return $this->app->make(EnsoGyms::class);
        });

        AliasLoader::getInstance()->alias('EnsoGyms', EnsoGymsFacade::class);

        $this->app->singleton('mindbody-api', function () {
            return $this->app->make(MindBodyFactory::class);
        });

        $this->app->singleton('mindbody-api-v6', function () {
            return $this->app->make(MindbodyApiV6::class);
        });

        AliasLoader::getInstance()->alias('MindBody', MindBodyFacade::class);

        $this->app->bind(CaseStudyContract::class, CaseStudy::class);
        $this->app->bind(CaseStudyCrudContract::class, CaseStudyCrud::class);
        $this->app->bind(CaseStudyControllerContract::class, CaseStudyController::class);

        $this->app->bind(GymContract::class, Gym::class);
        $this->app->bind(GymCrudContract::class, GymCrud::class);
        $this->app->bind(GymControllerContract::class, GymController::class);

        $this->app->bind(ClassTypeContract::class, ClassType::class);
        $this->app->bind(ClassTypeCrudContract::class, ClassTypeCrud::class);
        $this->app->bind(ClassTypeControllerContract::class, ClassTypeController::class);

        $this->app->bind(PackageContract::class, Package::class);
        $this->app->bind(PackageCrudContract::class, PackageCrud::class);
        $this->app->bind(PackageControllerContract::class, PackageController::class);

        $this->app->bind(PromotionContract::class, Promotion::class);
        $this->app->bind(PromotionCrudContract::class, PromotionCrud::class);
        $this->app->bind(PromotionControllerContract::class, PromotionController::class);

        $this->app->bind(InstructorContract::class, Instructor::class);
        $this->app->bind(InstructorCrudContract::class, InstructorCrud::class);
        $this->app->bind(InstructorControllerContract::class, InstructorController::class);

        $this->app->bind(SessionTypeContract::class, SessionType::class);
        $this->app->bind(SessionTypeCrudContract::class, SessionTypeCrud::class);
        $this->app->bind(SessionTypeControllerContract::class, SessionTypeController::class);

        $this->app->bind(BasketContract::class, Basket::class);
        $this->app->bind(BasketControllerContract::class, BasketController::class);

        $this->app->bind(OrderContract::class, Order::class);
        $this->app->bind(OrderCrudContract::class, OrderCrud::class);
        $this->app->bind(OrderControllerContract::class, OrderController::class);

        $this->app->bind(MembershipContract::class, Membership::class);
        $this->app->bind(MembershipCrudContract::class, MembershipCrud::class);
        $this->app->bind(MembershipControllerContract::class, MembershipController::class);

        $this->app->bind(OrderItemContract::class, OrderItem::class);

        $this->app->bind(CheckoutControllerContract::class, CheckoutController::class);
    }
}
