@extends(config('enso.crud.layout'))

@section('content')
    <div class="container is-fluid">
      <header class="is-clearfix">
        <h1 class="title is-pulled-left">{{ $crud->getNamePlural() }} - Programs</h1>

        @include($crud->getCrudView('lists.index-actions'))
      </header>

      <hr>

      @include($crud->getCrudView('lists.index-head'))

      @include('enso-crud::partials.alerts')

      <div class="columns">
        <div class="column is-6">
          <h2 class="title is-3">Class Programs</h2>
          <table class="table">
            <thead>
              <tr>
                <td>ID</td>
                <td>Name</td>
              </tr>
            </thead>
            <tbody>
              @foreach($class_programs as $program)
                <tr>
                  <td>{{ $program->mindbody_id }}</td>
                  <td>{{ $program->name }}</td>
                </tr>
              @endforeach
            </tbody>
          </table>
        </div>

        <div class="column is-6">
          <h2 class="title is-3">Appointment Programs</h2>
          <table class="table">
            <thead>
              <tr>
                <td>ID</td>
                <td>Name</td>
              </tr>
            </thead>
            <tbody>
              @foreach($appointment_programs as $program)
                <tr>
                  <td>{{ $program->mindbody_id }}</td>
                  <td>{{ $program->name }}</td>
                </tr>
              @endforeach
            </tbody>
          </table>
        </div>
      </div>
    </div>
@endsection
