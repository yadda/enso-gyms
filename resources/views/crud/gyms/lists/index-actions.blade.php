<div class="field is-grouped is-pulled-right">
  <p class="control">
    <a href="{{ route('admin.gyms.gyms.import') }}" class="button">
      <span class="icon is-small">
        <i class="fa fa-wrench"></i>
      </span>
      <span>Import</span>
    </a>

    @can('superuser')
      <a href="{{ route('admin.gyms.gyms.get-programs') }}" class="button">
        <span class="icon is-small">
          <i class="fa fa-wrench"></i>
        </span>
        <span>Programs</span>
      </a>
    @endcan
  </p>
</div>
