<div class="field is-grouped is-pulled-right">
  <p class="control">
    <a href="{{ route('admin.gyms.session-types.import') }}" class="button">
      <span class="icon is-small">
        <i class="fa fa-wrench"></i>
      </span>
      <span>Import</span>
    </a>
  </p>
</div>
