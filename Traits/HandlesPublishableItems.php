<?php

namespace Yadda\Enso\Gyms\Traits;

use Illuminate\Support\Arr;

/**
 * Adds functionality to handle default Bulk actions and filtering for
 * Crud models that use the IsPublishable trait.
 *
 * This should be applied to the Corresponding CrudConfig
 */
trait HandlesPublishableItems
{
    /**
     * Basic Select Filter options for Publishable items
     *
     * @return array
     */
    public function getPublishedOptions()
    {
        return [
            ['id' => 'all', 'name' => 'All'],
            ['id' => 'published', 'name' => 'Published'],
            ['id' => 'unpublished', 'name' => 'Unpublished'],
            ['id' => 'future', 'name' => 'Future'],
        ];
    }

    /**
     * Basic callable for handling Publishing filters.
     *
     * @param Builder $query
     * @param string  $value
     *
     * @return Builder
     */
    public function filterPublishedState($query, $value)
    {
        $key = Arr::get($value, 'id', null);

        switch ($key) {
            case 'future':
                return $query->willPublishLater();
            case 'unpublished':
                return $query->notPublished();
            case 'published':
                return $query->published();
            case 'all':
            default:
                return $query;
        }
    }

    /**
     * Gets some basic Bulk actions for publishing and unpublishing items that
     * have the IsPublishable trait.
     *
     * @return array
     */
    protected function getPublishingBulkActions()
    {
        return [
            'publish' => [
                'route' => route($this->getRoute('publish')),
                'method' => 'POST',
                'label' => "Publish selected",
            ],
            'unpublish' => [
                'route' => route($this->getRoute('unpublish')),
                'method' => 'POST',
                'label' => "Unpublish selected",
            ],
        ];
    }

    /**
     * Gets the data structure for the basic Publish state filter.
     *
     * @return void
     */
    protected function getPublishingFilter()
    {
        return [
            'type' => 'select',
            'label' => 'Is Published',
            'props' => [
                'settings' => [
                    'options' => $this->getPublishedOptions(),
                    'label' => 'name',
                    'track_by' => 'id',
                    'allow_empty' => false,
                    'show_labels' => false,
                ],
            ],
            'default' => ['id' => 'all', 'name' => 'All'],
            'callable' => [$this, 'filterPublishedState']
        ];
    }
}
