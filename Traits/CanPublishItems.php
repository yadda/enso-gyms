<?php

namespace Yadda\Enso\Gyms\Traits;

use Carbon\Carbon;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

/**
 * Trait that adds functionality to CrudControllers for publishing and
 * unpublishing items that use the IsPublishable trait.
 */
trait CanPublishItems
{
    /**
     * Publishes a set of ClassTypes, based on passed class type ids.
     *
     * @param Request $request
     *
     * @return Responses
     */
    public function publish(Request $request)
    {
        $item_ids = $request->get('values', []);

        $config = $this->getConfig();

        $model = $config->getModel();

        try {
            $items = $model::withoutGlobalScopes()->whereIn('id', $item_ids)->get();

            $validation_response = $this->validatePublishing($items);
            if ($validation_response !== true) {
                return $validation_response;
            }

            $this->publishItems($items);
        } catch (Exception $e) {
            DB::rollback();

            Log::error($e);

            return [
                'status' => 'error',
                'message' => 'Failed publishing these ' . $config->getNamePlural(),
            ];
        }

        return [
            'status' => 'success',
        ];
    }

    /**
     * Unpublishes a set of ClassTypes, based on passed class type ids.
     *
     * @param Request $request
     *
     * @return Responses
     */
    public function unpublish(Request $request)
    {
        $item_ids = $request->get('values', []);

        $config = $this->getConfig();

        $model = $config->getModel();

        try {
            $items = $model::withoutGlobalScopes()->whereIn('id', $item_ids)->get();

            if ($items->count() === 0) {
                return [
                    'status' => 'error',
                    'message' => 'No ' . $config->getNamePlural() . ' selected to unpublish.',
                ];
            }

            DB::beginTransaction();

            $items->each(function ($item) {
                $item->update(['published' => false]);
            });
        } catch (Exception $e) {
            DB::rollback();

            Log::error($e);

            return [
                'status' => 'error',
                'message' => 'Failed unpublishing these ' . $config->getNamePlural(),
            ];
        }

        DB::commit();

        return [
            'status' => 'success',
        ];
    }

    /**
     * Checks whether the given Items collection is valid for publishing.
     * Invalid Collections are ones with no items, or ones where the contained
     * items have a 'canPublish' method, and one or more items do not pass
     * this test.
     *
     * @param Collection $items
     *
     * @return array|bool
     */
    protected function validatePublishing(Collection $items)
    {
        $config = $this->getConfig();

        if ($items->count() === 0) {
            return [
                'status' => 'error',
                'message' => 'No ' . $config->getNamePlural() . ' selected to publish.',
            ];
        }

        // Items can optionally have a 'canPublish' method.
        if (method_exists($items->first(), 'canPublish')) {
            $failures = $items->reject(function ($item) {
                return $item->isPublished() || $item->canPublish();
            });

            if ($failures->count()) {
                $message = 'Unable to Publish some ' . $config->getNamePlural();

                if (property_exists($failures->first(), 'publish_criteria')) {
                    $message .= ". " . $failures->first()->publish_criteria;
                }

                return [
                    'status' => 'error',
                    'message' => $message,
                ];
            }
        }

        return true;
    }

    /**
     * Publishes items in the given collection, ignoring items already
     * published.
     *
     * @param Collection $items
     *
     * @return void
     */
    protected function publishItems(Collection $items)
    {
        $current_time = Carbon::now();

        DB::beginTransaction();

        $items->each(function ($item) use ($current_time) {
            if ($item->isPublished()) {
                return;
            }

            $item->fill(['published' => true]);

            if ($item->publish_at && $item->publish_at->gt($current_time)) {
                $item->fill(['publish_at' => $current_time]);
            }

            $item->save();
        }, new Collection);

        DB::commit();
    }
}
