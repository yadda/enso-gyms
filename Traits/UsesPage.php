<?php

namespace Yadda\Enso\Gyms\Traits;

use App\Page;
use Illuminate\Support\Facades\Log;
use Yadda\Enso\Facades\EnsoMeta;

trait UsesPage
{
    /**
     * Rely on a page for this route. If the page doesn't exist, return a 404.
     * If it does, use the meta from the page.
     *
     * @param string $slug
     *
     * @return void
     */
    public function usePage($slug)
    {
        $page = Page::where('slug', $slug)->accessibleToUser()->first();

        if (!$page) {
            Log::error('Tried to load a page that does not exist. Slug: ' . $slug);
            abort(404);
        }

        $meta = $page->getMeta();

        $meta->overrideTitle($page->title);

        EnsoMeta::use($meta);

        return $page;
    }
}
