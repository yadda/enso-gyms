<?php

namespace Yadda\Enso\Gyms\Traits;

use Yadda\Enso\Gyms\Models\Billing;

trait HasBillingInformation
{
    public function billingInformation()
    {
        return $this->hasOne(Billing::class);
    }
}
