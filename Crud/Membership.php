<?php

namespace Yadda\Enso\Gyms\Crud;

use Yadda\Enso\Crud\Config;
use Yadda\Enso\Crud\Forms\Fields\CheckboxField;
use Yadda\Enso\Crud\Forms\Fields\TextField;
use Yadda\Enso\Crud\Forms\Form;
use Yadda\Enso\Crud\Forms\Section;
use Yadda\Enso\Crud\Tables\Text;
use Yadda\Enso\Gyms\Models\Membership as MembershipModel;

class Membership extends Config
{
    public function configure()
    {
        $this->model(MembershipModel::class)
            ->route('admin.gyms.memberships')
            ->views('memberships')
            ->name('Membership')
            ->columns([
                Text::make('name'),
                Text::make('slug'),
            ])
            ->rules([
                'main.name' => 'required',
            ])
            ->removeIndexAction('delete');
    }

    public function create(Form $form)
    {
        $form->addSections([
            Section::make('main')
                ->addFields([
                    TextField::make('name')
                        ->addFieldsetClass('is-half'),
                    TextField::make('mindbody_id')
                        ->setReadonly(true)
                        ->setDisabled(true)
                        ->addFieldsetClass('is-half'),
                    CheckboxField::make('allows_access')
                        ->setOptions([
                            'allows_access' => 'If checked, users with this membership will be able to access the site',
                        ]),
                ]),
        ]);

        return $form;
    }
}
