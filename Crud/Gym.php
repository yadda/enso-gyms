<?php

namespace Yadda\Enso\Gyms\Crud;

use Yadda\Enso\Crud\Config;
use Yadda\Enso\Crud\Forms\Fields\CheckboxField;
use Yadda\Enso\Crud\Forms\Fields\DateTimeField;
use Yadda\Enso\Crud\Forms\Fields\SlugField;
use Yadda\Enso\Crud\Forms\Fields\TextareaField;
use Yadda\Enso\Crud\Forms\Fields\TextField;
use Yadda\Enso\Crud\Forms\Form;
use Yadda\Enso\Crud\Forms\Section;
use Yadda\Enso\Crud\Tables\Publish;
use Yadda\Enso\Crud\Tables\Text;
use Yadda\Enso\Gyms\Contracts\Gym as GymModel;
use Yadda\Enso\Gyms\Contracts\GymCrud;
use Yadda\Enso\Gyms\Traits\HandlesPublishableItems;
use Yadda\Enso\Meta\Crud\MetaSection;

class Gym extends Config implements GymCrud
{
    use HandlesPublishableItems;

    public function configure()
    {
        $gym_model = resolve(GymModel::class);

        $this
            ->model(get_class($gym_model))
            ->route('admin.gyms.gyms')
            ->views('gyms')
            ->name('Gym')
            ->orderable('order')
            ->order('order', 'desc')
            ->paginate(25)
            ->columns([
                Text::make('name'),
                Text::make('slug'),
                Publish::make('is_published', $this)
                    ->setLabel('Published')
                    ->addThClass('is-narrow'),
            ])
            ->setBulkActions([
                "actions" => $this->getPublishingBulkActions(),
            ])
            ->setItemFilters([
                'published' => $this->getPublishingFilter(),
                'search' => [
                    'type' => 'text',
                    'label' => 'Search',
                    'callable' => [$this, 'filterSearch'],
                ]
            ])
            ->rules([
                'contact.email' => ['nullable', 'email'],
            ])
            ->messages([
                //
            ])
            ->setDeletable(false);
    }

    public function create(Form $form)
    {
        $form->addSections([
            Section::make('main')
                ->addFields([
                    TextField::make('mb_location_id')
                        ->setLabel("MindBody ID")
                        ->setDisabled(true),
                    TextField::make('name')
                        ->setDisabled(true)
                        ->addFieldsetClass('is-6'),
                    SlugField::make('slug')
                        ->setRoute($this->getSlugRoute())
                        ->setSource('name')
                        ->addFieldsetClass('is-6'),
                    TextareaField::make('description')
                        ->setDisabled(true)
                        ->setDefaultValue(null),
                    DateTimeField::make('publish_at')
                        ->addFieldsetClass('is-10')
                        ->setDefaultValue(null),
                    CheckboxField::make('published')
                        ->addFieldsetClass('is-2')
                        ->setLabel('Ready to Publish?')
                        ->setOptions([
                            'published' => '',
                        ]),
                ]),
            Section::make('address')
                ->addFields([
                    TextField::make('mb_location_id')
                        ->setLabel("MindBody ID")
                        ->setDisabled(true),

                    TextField::make('address')
                        ->setDisabled(true)
                        ->addFieldsetClass('is-6')
                        ->setDefaultValue(null),
                    TextField::make('address_2')
                        ->setDisabled(true)
                        ->addFieldsetClass('is-6')
                        ->setDefaultValue(null),
                    TextField::make('city')
                        ->setDisabled(true)
                        ->addFieldsetClass('is-4')
                        ->setDefaultValue(null),
                    TextField::make('province')
                        ->setDisabled(true)
                        ->addFieldsetClass('is-4')
                        ->setDefaultValue(null),
                    TextField::make('post_code')
                        ->setDisabled(true)
                        ->addFieldsetClass('is-4')
                        ->setDefaultValue(null),
                    TextField::make('longitude')
                        ->setDisabled(true)
                        ->addFieldsetClass('is-6')
                        ->setDefaultValue(null),
                    TextField::make('latitude')
                        ->setDisabled(true)
                        ->addFieldsetClass('is-6')
                        ->setDefaultValue(null),
                ]),
            Section::make('contact')
                ->addFields([
                    TextField::make('mb_location_id')
                        ->setLabel("MindBody ID")
                        ->setDisabled(true),
                    TextField::make('phone')
                        ->setDisabled(true)
                        ->addFieldsetClass('is-6')
                        ->setDefaultValue(null),
                    TextField::make('email')
                        ->addFieldsetClass('is-6')
                        ->setDefaultValue(null),
                ]),
            MetaSection::make('meta'),
        ]);

        return $form;
    }

    public function filterSearch($query, $value)
    {
        return $query->where(function ($sub_query) use ($value) {
            return $sub_query->where('name', 'LIKE', "%{$value}%")
                ->orWhere('slug', 'LIKE', "%{$value}%");
        });
    }

    /**
     * Gets the route to use for the slug field.
     *
     * @return void
     */
    protected function getSlugRoute()
    {
        return route('gyms.show', '%SLUG%');
    }
}
