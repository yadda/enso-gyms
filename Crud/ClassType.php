<?php

namespace Yadda\Enso\Gyms\Crud;

use Yadda\Enso\Crud\Config;
use Yadda\Enso\Crud\Forms\Fields\CheckboxField;
use Yadda\Enso\Crud\Forms\Fields\DateTimeField;
use Yadda\Enso\Crud\Forms\Fields\FileUploadFieldResumable;
use Yadda\Enso\Crud\Forms\Fields\SlugField;
use Yadda\Enso\Crud\Forms\Fields\TextareaField;
use Yadda\Enso\Crud\Forms\Fields\TextField;
use Yadda\Enso\Crud\Forms\Form;
use Yadda\Enso\Crud\Forms\Section;
use Yadda\Enso\Crud\Tables\Publish;
use Yadda\Enso\Crud\Tables\Text;
use Yadda\Enso\Crud\Tables\Thumbnail;
use Yadda\Enso\Gyms\Contracts\ClassType as ClassTypeModel;
use Yadda\Enso\Gyms\Contracts\ClassTypeCrud;
use Yadda\Enso\Gyms\Traits\HandlesPublishableItems;
use Yadda\Enso\Meta\Crud\MetaSection;

class ClassType extends Config implements ClassTypeCrud
{
    use HandlesPublishableItems;

    public function configure()
    {
        $class_type_model = resolve(ClassTypeModel::class);

        $this
            ->model(get_class($class_type_model))
            ->route('admin.gyms.class-types')
            ->views('class-types')
            ->name('Class Type')
            ->orderable('order')
            ->order('order', 'desc')
            ->paginate(25)
            ->columns([
                Thumbnail::make('cms_thumbnail')
                    ->setLabel('')
                    ->orderableBy(null),
                Text::make('name'),
                Text::make('slug'),
                Publish::make('is_published', $this)
                    ->setLabel('Published')
                    ->addThClass('is-narrow'),
            ])
            ->setBulkActions([
                "actions" => $this->getPublishingBulkActions(),
            ])
            ->setItemFilters([
                'published' => $this->getPublishingFilter(),
                'search' => [
                    'type' => 'text',
                    'label' => 'Search',
                    'callable' => [$this, 'filterSearch'],
                ]
            ])
            ->rules([
                //
            ])
            ->messages([
                //
            ])
            ->setDeletable(false);
    }

    public function create(Form $form)
    {
        $form->addSections([
            Section::make('main')
                ->addFields([
                    TextField::make('mb_class_description_id')
                        ->setLabel("MindBody ID")
                        ->setDisabled(true),
                    TextField::make('name')
                        ->setDisabled(true)
                        ->addFieldsetClass('is-6'),
                    SlugField::make('slug')
                        ->setRoute($this->getSlugRoute())
                        ->setSource('name')
                        ->addFieldsetClass('is-6'),
                    FileUploadFieldResumable::make('image_id')
                        ->setRelationshipName('image')
                        ->setUploadPath('gyms/class-types'),
                    TextareaField::make('description')
                        ->setDisabled(true)
                        ->setDefaultValue(null),
                    DateTimeField::make('publish_at')
                        ->addFieldsetClass('is-10')
                        ->setDefaultValue(null),
                    CheckboxField::make('published')
                        ->addFieldsetClass('is-2')
                        ->setLabel('Ready to Publish?')
                        ->setOptions([
                            'published' => '',
                        ]),
                ]),
            MetaSection::make('meta'),
        ]);

        return $form;
    }

    public function filterSearch($query, $value)
    {
        return $query->where(function ($sub_query) use ($value) {
            return $sub_query->where('name', 'LIKE', "%{$value}%")
                ->orWhere('slug', 'LIKE', "%{$value}%")
                ->orWhere('description', 'LIKE', "%{$value}%");
        });
    }

    /**
     * Gets the route to use for the slug field.
     *
     * @return void
     */
    protected function getSlugRoute()
    {
        return route('class-types.show', '%SLUG%');
    }
}
