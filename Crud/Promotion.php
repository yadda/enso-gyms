<?php

namespace Yadda\Enso\Gyms\Crud;

use Eventy;
use Yadda\Enso\Crud\Config;
use Yadda\Enso\Crud\Forms\Fields\BelongsToManyField;
use Yadda\Enso\Crud\Forms\Fields\CheckboxField;
use Yadda\Enso\Crud\Forms\Fields\DateTimeField;
use Yadda\Enso\Crud\Forms\Fields\RandomTextField;
use Yadda\Enso\Crud\Forms\Fields\SelectField;
use Yadda\Enso\Crud\Forms\Fields\TextField;
use Yadda\Enso\Crud\Forms\Form;
use Yadda\Enso\Crud\Forms\Section;
use Yadda\Enso\Crud\Tables\Publish;
use Yadda\Enso\Crud\Tables\Text;
use Yadda\Enso\Gyms\Contracts\Package;
use Yadda\Enso\Gyms\Contracts\PromotionCrud;
use Yadda\Enso\Gyms\Models\Promotion as PromotionModel;
use Yadda\Enso\Gyms\Traits\HandlesPublishableItems;

class Promotion extends Config implements PromotionCrud
{
    use HandlesPublishableItems;

    public function configure()
    {
        $promotion_model = resolve(PromotionModel::class);

        $this
            ->model(get_class($promotion_model))
            ->route('admin.gyms.promotions')
            ->views('promotions')
            ->name('Promotion')
            ->orderable('order')
            ->order('order', 'desc')
            ->paginate(25)
            ->columns([
                Text::make('name'),
                Text::make('code'),
                Text::make('limit')
                    ->setFormatter(function ($value) {
                        if (is_null($value) || $value === '') {
                            return 'Unlimited';
                        }

                        return $value;
                    }),
                Text::make('used')
                    ->setLabel('# of Uses'),
                Publish::make('is_published', $this)
                    ->setLabel('Published')
                    ->addThClass('is-narrow'),
            ])
            ->setBulkActions([
                "actions" => $this->getPublishingBulkActions(),
            ])
            ->setItemFilters([
                'published' => $this->getPublishingFilter(),
                'search' => [
                    'type' => 'text',
                    'label' => 'Search',
                    'callable' => [$this, 'filterSearch'],
                ]
            ])
            ->rules([
                'main.code' => ['required', 'unique:enso_gyms_promotions,code'],
            ])
            ->messages([
                //
            ])
            ->setDeletable(false);
    }

    public function create(Form $form)
    {
        $form->addSections([
            Section::make('main')
                ->addFields([
                    TextField::make('name')
                        ->addFieldsetClass('is-6')
                        ->setDefaultValue(null),
                    RandomTextField::make('code')
                        ->addFieldsetClass('is-6')
                        ->setTextLength(12)
                        ->setDefaultValue(null),
                    SelectField::make('type')
                        ->addFieldsetClass('is-6')
                        ->setOptions(PromotionModel::getTypeOptions())
                        ->setDefaultValue(null),
                    TextField::make('value')
                        ->addFieldsetClass('is-6')
                        ->setDefaultValue(null),
                    BelongsToManyField::make('packages')
                        ->setOptions(resolve(Package::class)::query())
                        ->setHelpText(
                            'Add packages to create a voucher that only affect '
                            . 'those packages. Otherwise, it will affect the '
                            . 'whole basket'
                        )
                        ->addFieldSetClass('is-12'),
                    DateTimeField::make('expires_on')
                        ->addFieldsetClass('is-8')
                        ->setDefaultValue(null),
                    TextField::make('max_per_basket')
                        ->addFieldsetClass('is-4')
                        ->setDefaultValue('1')
                        ->setHelpText('For unlimited use per basket, set to 0'),
                    DateTimeField::make('publish_at')
                        ->addFieldsetClass('is-10')
                        ->setDefaultValue(null),
                    CheckboxField::make('published')
                        ->addFieldsetClass('is-2')
                        ->setLabel('Ready to Publish?')
                        ->setOptions([
                            'published' => '',
                        ]),
                ]),
        ]);

        return $form;
    }

    public function filterSearch($query, $value)
    {
        return $query->where(function ($sub_query) use ($value) {
            return $sub_query->where('name', 'LIKE', "%{$value}%")
                ->orWhere('code', 'LIKE', "%{$value}%");
        });
    }
}
