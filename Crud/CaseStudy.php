<?php

namespace Yadda\Enso\Gyms\Crud;

use Yadda\Enso\Crud\Config;
use Yadda\Enso\Crud\Forms\Fields\CheckboxField;
use Yadda\Enso\Crud\Forms\Fields\DateTimeField;
use Yadda\Enso\Crud\Forms\Fields\FileUploadFieldResumable;
use Yadda\Enso\Crud\Forms\Fields\FlexibleContentField;
use Yadda\Enso\Crud\Forms\Fields\SlugField;
use Yadda\Enso\Crud\Forms\Fields\TextField;
use Yadda\Enso\Crud\Forms\Form;
use Yadda\Enso\Crud\Forms\Section;
use Yadda\Enso\Crud\Forms\Sections\TextSection;
use Yadda\Enso\Crud\Tables\Publish;
use Yadda\Enso\Crud\Tables\Text;
use Yadda\Enso\Crud\Tables\Thumbnail;
use Yadda\Enso\Gyms\Contracts\CaseStudy as CaseStudyModel;
use Yadda\Enso\Gyms\Contracts\CaseStudyCrud;
use Yadda\Enso\Gyms\Traits\HandlesPublishableItems;
use Yadda\Enso\Meta\Crud\MetaSection;

class CaseStudy extends Config implements CaseStudyCrud
{
    use HandlesPublishableItems;

    public function configure()
    {
        $case_study = resolve(CaseStudyModel::class);

        $this
            ->model(get_class($case_study))
            ->route('admin.gyms.case-studies')
            ->views('case-studies')
            ->name('Case Study')
            ->orderable('order')
            ->order('order', 'desc')
            ->paginate(25)
            ->columns([
                Thumbnail::make('cms_thumbnail')
                    ->setLabel('')
                    ->orderableBy(null),
                Text::make('name'),
                Text::make('slug'),
                Publish::make('is_published', $this)
                    ->setLabel('Published')
                    ->addThClass('is-narrow'),
            ])
            ->setBulkActions([
                "actions" => $this->getPublishingBulkActions(),
            ])
            ->setItemFilters([
                'published' => $this->getPublishingFilter(),
                'search' => [
                    'type' => 'text',
                    'label' => 'Search',
                    'callable' => [$this, 'filterSearch'],
                ]
            ])
            ->rules([
                'main.name' => 'required|string',
            ])
            ->messages([
                //
            ]);
    }

    public function create(Form $form)
    {
        $form->addSections([
            Section::make('main')
                ->addFields([
                    TextField::make('name')
                        ->addFieldsetClass('is-6'),
                    SlugField::make('slug')
                        ->setRoute($this->getSlugRoute())
                        ->setSource('name')
                        ->addFieldsetClass('is-6'),
                    FileUploadFieldResumable::make('image_id')
                        ->setRelationshipName('image')
                        ->setUploadPath('gyms/case-studies'),
                    DateTimeField::make('publish_at')
                        ->addFieldsetClass('is-10'),
                    CheckboxField::make('published')
                        ->addFieldsetClass('is-2')
                        ->setLabel('Ready to Publish?')
                        ->setOptions([
                            'published' => '',
                        ]),
                    FlexibleContentField::make('content')
                        ->addRowSpecs([
                            TextSection::make('text'),
                        ]),
                ]),
            MetaSection::make('meta'),
        ]);

        return $form;
    }

    /**
     * Applies a serach filter on the Index query.
     *
     * @param Illuminate\Database\Eloquent\Builder $query Initial Query
     * @param string                               $value Search term
     *
     * @return Illuminate\Database\Eloquent\Builder
     */
    public function filterSearch($query, $value)
    {
        return $query->where(function ($sub_query) use ($value) {
            return $sub_query->where('name', 'LIKE', "%{$value}%")
                ->orWhere('content', 'LIKE', "%{$value}%");
        });
    }

    /**
     * Gets the route to use for the slug field.
     *
     * @return void
     */
    protected function getSlugRoute()
    {
        return route('case-studies.show', '%SLUG%');
    }
}
