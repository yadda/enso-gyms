<?php

namespace Yadda\Enso\Gyms\Crud;

use Illuminate\Support\Arr;
use Illuminate\Support\Facades\App;
use Money\Money;
use Yadda\Enso\Crud\Config;
use Yadda\Enso\Crud\Forms\Fields\CheckboxField;
use Yadda\Enso\Crud\Forms\Fields\HasManyRepeaterField;
use Yadda\Enso\Crud\Forms\Fields\TextareaField;
use Yadda\Enso\Crud\Forms\Fields\TextField;
use Yadda\Enso\Crud\Forms\FlexibleContentSection;
use Yadda\Enso\Crud\Forms\Form;
use Yadda\Enso\Crud\Forms\Section;
use Yadda\Enso\Crud\Tables\Boolean;
use Yadda\Enso\Crud\Tables\Text;
use Yadda\Enso\Gyms\Checkout\MoneyHandler;
use Yadda\Enso\Gyms\Contracts\Order as OrderModel;
use Yadda\Enso\Gyms\Contracts\OrderCrud;
use Yadda\Enso\Gyms\Contracts\OrderItem;

class Order extends Config implements OrderCrud
{
    public function configure()
    {
        $order_mordel = resolve(OrderModel::class);

        $this
            ->model(get_class($order_mordel))
            ->route('admin.gyms.orders')
            ->views('orders')
            ->name('Order')
            ->paginate(25)
            ->columns([
                Text::make('billing_name'),
                Text::make('status'),
                Boolean::make('completed_at_bool')
                    ->setLabel('Complete'),
            ])
            ->setItemFilters([
                'payment_state' => [
                    'type' => 'select',
                    'label' => 'Payment taken',
                    'props' => [
                        'settings' => [
                            'options' => $this->getPaymentStateOptions(),
                            'label' => 'name',
                            'track_by' => 'id',
                            'allow_empty' => false,
                            'show_labels' => false,
                        ],
                    ],
                    'default' => ['id' => 'all', 'name' => 'All'],
                    'callable' => [$this, 'filterPaymentState']
                ],
                'completion_state' => [
                    'type' => 'select',
                    'label' => 'Is Published',
                    'props' => [
                        'settings' => [
                            'options' => $this->getCompletionStateOptions(),
                            'label' => 'name',
                            'track_by' => 'id',
                            'allow_empty' => false,
                            'show_labels' => false,
                        ],
                    ],
                    'default' => ['id' => 'all', 'name' => 'All'],
                    'callable' => [$this, 'filterCompletionState']
                ],
                'search' => [
                    'type' => 'text',
                    'label' => 'Search',
                    'callable' => [$this, 'filterSearch'],
                ],
            ])
            ->rules([
                //
            ])
            ->messages([
                //
            ])
            ->setDeletable(false);
    }

    public function create(Form $form)
    {
        $form->addSections([
            Section::make('main')->addFields([
                TextField::make('billing_name')
                    ->setDisabled(true)
                    ->setDefaultValue(null)
                    ->addFieldsetClass('is-6'),
                TextField::make('billing_company')
                    ->setDisabled(true)
                    ->setDefaultValue(null)
                    ->addFieldsetClass('is-6'),
                TextField::make('billing_address_1')
                    ->setDisabled(true)
                    ->setDefaultValue(null)
                    ->addFieldsetClass('is-6'),
                TextField::make('billing_address_2')
                    ->setDisabled(true)
                    ->setDefaultValue(null)
                    ->addFieldsetClass('is-6'),
                TextField::make('billing_city')
                    ->setDisabled(true)
                    ->setDefaultValue(null)
                    ->addFieldsetClass('is-6'),
                TextField::make('billing_state')
                    ->setDisabled(true)
                    ->setDefaultValue(null)
                    ->addFieldsetClass('is-6'),
                TextField::make('billing_postcode')
                    ->setDisabled(true)
                    ->setDefaultValue(null)
                    ->addFieldsetClass('is-6'),
                TextField::make('billing_country')
                    ->setDisabled(true)
                    ->setDefaultValue(null)
                    ->addFieldsetClass('is-6'),
                TextField::make('billing_email')
                    ->setDisabled(true)
                    ->setDefaultValue(null)
                    ->addFieldsetClass('is-6'),
                TextField::make('billing_phone')
                    ->setDisabled(true)
                    ->setDefaultValue(null)
                    ->addFieldsetClass('is-6'),
                HasManyRepeaterField::make('items')
                    ->setInverseRelationshipName('order')
                    ->setRequestApplicationCallback([$this, 'overrideRequestApplication'])
                    ->setRelationApplicationCallback([$this, 'overrideRelationApplication'])
                    ->addRowSpecs([
                        $this->createExistingLineItemRowspec(),
                    ]),
                TextField::make('total')
                    ->setAlterFormDataCallback([$this, 'fromLowestDenomination'])
                    ->setAlterRequestDataCallback([$this, 'toLowestDenomination'])
                    ->setDisabled(true)
                    ->setDefaultValue(null)
                    ->addFieldsetClass('is-6'),
                TextField::make('discount')
                    ->setAlterFormDataCallback([$this, 'fromLowestDenomination'])
                    ->setAlterRequestDataCallback([$this, 'toLowestDenomination'])
                    ->setDisabled(true)
                    ->setDefaultValue(null)
                    ->addFieldsetClass('is-6'),
                TextField::make('status')
                    ->setDisabled(true)
                    ->setDefaultValue(null)
                    ->addFieldsetClass('is-6'),
                TextareaField::make('notes')
                    ->addFieldsetClass('is-12'),
                CheckboxField::make('completed_at_bool')
                    ->setLabel('Check this box when the packages have been assigned on MindBody')
                    ->setOptions([
                        'completed_at_bool' => 'Complete?'
                    ])
                    ->addFieldsetClass('is-6'),
            ]),
        ]);

        return $form;
    }


    public function fromLowestDenomination($pence)
    {
        return App::make(MoneyHandler::class)->fromLowestDenomination(
            $pence ?? 0,
            config('enso.gyms.mindbody.currency', 'THB')
        );
    }

    public function toLowestDenomination($value)
    {
        return App::make(MoneyHandler::class)->toLowestDenomination(
            $value,
            config('enso.gyms.mindbody.currency', 'THB')
        );
    }

    /**
     * Basic Select Filter options for Completion state items
     *
     * @return array
     */
    public function getPaymentStateOptions()
    {
        return [
            ['id' => 'all', 'name' => 'All'],
            ['id' => 'paid', 'name' => 'Paid'],
            ['id' => 'unpaid', 'name' => 'Unpaid'],
        ];
    }

    /**
     * Basic callable for handling Copmletion state filters.
     *
     * @param Builder $query
     * @param string  $value
     *
     * @return Builder
     */
    public function filterPaymentState($query, $value)
    {
        $key = Arr::get($value, 'id', null);

        switch ($key) {
            case 'unpaid':
                return $query->unpaid();
            case 'paid':
                return $query->paid();
            case 'all':
            default:
                return $query;
        }
    }

    /**
     * Basic Select Filter options for Completion state items
     *
     * @return array
     */
    public function getCompletionStateOptions()
    {
        return [
            ['id' => 'all', 'name' => 'All'],
            ['id' => 'complete', 'name' => 'Complete'],
            ['id' => 'incomplete', 'name' => 'Incomplete'],
        ];
    }

    /**
     * Basic callable for handling Copmletion state filters.
     *
     * @param Builder $query
     * @param string  $value
     *
     * @return Builder
     */
    public function filterCompletionState($query, $value)
    {
        $key = Arr::get($value, 'id', null);

        switch ($key) {
            case 'incomplete':
                return $query->notCompleted();
            case 'complete':
                return $query->completed();
            case 'all':
            default:
                return $query;
        }
    }

    /**
     * Filters search queries
     *
     * @param Builder $query
     * @param string  $value
     *
     * @return Builder
     */
    public function filterSearch($query, $value)
    {
        return $query->where(function ($sub_query) use ($value) {
            return $sub_query->where('billing_name', 'LIKE', "%{$value}%")
                ->orWhere('billing_company', 'LIKE', "%{$value}%")
                ->orWhere('billing_address_1', 'LIKE', "%{$value}%")
                ->orWhere('billing_address_2', 'LIKE', "%{$value}%")
                ->orWhere('billing_city', 'LIKE', "%{$value}%")
                ->orWhere('billing_state', 'LIKE', "%{$value}%")
                ->orWhere('billing_postcode', 'LIKE', "%{$value}%")
                ->orWhere('billing_country', 'LIKE', "%{$value}%")
                ->orWhere('billing_email', 'LIKE', "%{$value}%")
                ->orWhere('billing_phone', 'LIKE', "%{$value}%");
        });
    }

    /**
     * Ensures that no data can be added/saved through the Line-items rows
     *
     * @param OrderModel $parent
     * @param OrderItem $item
     * @param array $data
     *
     * @return OrderModel
     */
    public function overrideRequestApplication($parent, $item, $data)
    {
        // Do nothing. This is display Only.
        return $item;
    }

    /**
     * Ensures that no data can be added/saved through the Line-items rows
     *
     * @param OrderItem $item
     * @param array $new_current_relations
     * @param array $removed_relations
     *
     * @return OrderModel
     */
    public function overrideRelationApplication($item, $new_current_relations, $removed_relations)
    {
        // Do nothing. This is display Only.
        return $item;
    }

    protected function createExistingLineItemRowspec()
    {
        return FlexibleContentSection::make('items')
            ->setLabel('Line Items')
            ->allownew(false)
            ->allowDeletion(false)
            ->addFields([
                (new TextField('id'))
                    ->addFieldSetClass('is-hidden')
                    ->setDisabled(),
                (new TextField('saleable_name'))
                    ->addFieldSetClass('is-3')
                    ->setDisabled(),
                (new TextField('quantity'))
                    ->addFieldSetClass('is-3')
                    ->setDisabled(),
                (new TextField('subtotal'))
                    ->setAlterFormDataCallback([$this, 'fromLowestDenomination'])
                    ->addFieldSetClass('is-3')
                    ->setDisabled(),
                (new TextField('total'))
                    ->setAlterFormDataCallback([$this, 'fromLowestDenomination'])
                    ->addFieldSetClass('is-3')
                    ->setDisabled(),
            ]);
    }
}
