<?php

namespace Yadda\Enso\Gyms\Crud\Sections;

use Yadda\Enso\Crud\Forms\Fields\TextField;
use Yadda\Enso\Crud\Forms\HasOneSection;

class BillingSection extends HasOneSection
{
    public function __construct(string $name = 'main')
    {
        parent::__construct($name);

        $this->allowNullRelation(true)
            ->setLabel('Billing')
            ->addFields([
                TextField::make('billing_name')
                    ->setDisabled(true)
                    ->addFieldsetClass('is-6'),
                TextField::make('billing_company')
                    ->setDisabled(true)
                    ->addFieldsetClass('is-6'),
                TextField::make('billing_email')
                    ->setDisabled(true)
                    ->addFieldsetClass('is-6'),
                TextField::make('billing_phone')
                    ->setDisabled(true)
                    ->addFieldsetClass('is-6'),
                TextField::make('billing_address_1')
                    ->setDisabled(true)
                    ->addFieldsetClass('is-6'),
                TextField::make('billing_address_2')
                    ->setDisabled(true)
                    ->addFieldsetClass('is-6'),
                TextField::make('billing_city')
                    ->setDisabled(true)
                    ->addFieldsetClass('is-6'),
                TextField::make('billing_state')
                    ->setDisabled(true)
                    ->addFieldsetClass('is-6'),
                TextField::make('billing_postcode')
                    ->setDisabled(true)
                    ->addFieldsetClass('is-6'),
                TextField::make('billing_country')
                    ->setDisabled(true)
                    ->addFieldsetClass('is-6'),
            ]);
    }
}
