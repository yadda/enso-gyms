<?php

namespace Yadda\Enso\Gyms\Crud;

use Yadda\Enso\Crud\Config;
use Yadda\Enso\Crud\Forms\Fields\CheckboxField;
use Yadda\Enso\Crud\Forms\Fields\DateTimeField;
use Yadda\Enso\Crud\Forms\Fields\SlugField;
use Yadda\Enso\Crud\Forms\Fields\TextField;
use Yadda\Enso\Crud\Forms\Form;
use Yadda\Enso\Crud\Forms\Section;
use Yadda\Enso\Crud\Tables\Publish;
use Yadda\Enso\Crud\Tables\Text;
use Yadda\Enso\Gyms\Contracts\SessionType as SessionTypeModel;
use Yadda\Enso\Gyms\Contracts\SessionTypeCrud;
use Yadda\Enso\Gyms\Traits\HandlesPublishableItems;

class SessionType extends Config implements SessionTypeCrud
{
    use HandlesPublishableItems;

    public function configure()
    {
        $session_type_model = resolve(SessionTypeModel::class);

        $this
            ->model(get_class($session_type_model))
            ->route('admin.gyms.session-types')
            ->views('session-types')
            ->name('Session Type')
            ->orderable('order')
            ->order('order', 'desc')
            ->paginate(25)
            ->columns([
                Text::make('name'),
                Text::make('slug'),
                Publish::make('is_published', $this)
                    ->setLabel('Published')
                    ->addThClass('is-narrow'),
            ])
            ->setBulkActions([
                "actions" => $this->getPublishingBulkActions(),
            ])
            ->setItemFilters([
                'published' => $this->getPublishingFilter(),
                'search' => [
                    'type' => 'text',
                    'label' => 'Search',
                    'callable' => [$this, 'filterSearch']
                ],
            ])
            ->rules([
                //
            ])
            ->messages([
                //
            ])
            ->setDeletable(false);
    }

    public function create(Form $form)
    {
        $form->addSections([
            Section::make('main')
                ->addFields([
                    TextField::make('mb_session_type_id')
                        ->setLabel("MindBody ID")
                        ->setDisabled(true),
                    TextField::make('name')
                        ->setDisabled(true)
                        ->addFieldsetClass('is-6'),
                    SlugField::make('slug')
                        ->setRoute($this->getSlugRoute())
                        ->setSource('name')
                        ->addFieldsetClass('is-6'),
                    DateTimeField::make('publish_at')
                        ->addFieldsetClass('is-10')
                        ->setDefaultValue(null),
                    CheckboxField::make('published')
                        ->addFieldsetClass('is-2')
                        ->setLabel('Ready to Publish?')
                        ->setOptions([
                            'published' => '',
                        ]),
                ])
        ]);

        return $form;
    }

    public function filterSearch($query, $value)
    {
        return $query->where(function ($sub_query) use ($value) {
            return $sub_query->where('name', 'LIKE', "%{$value}%")
                ->orWhere('slug', 'LIKE', "%{$value}%");
        });
    }

    /**
     * Gets the route to use for the slug field.
     *
     * @return void
     */
    protected function getSlugRoute()
    {
        return route('session-types.show', '%SLUG%');
    }
}
