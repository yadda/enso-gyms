<?php

namespace Yadda\Enso\Gyms\Crud;

use Illuminate\Support\Facades\App;
use Yadda\Enso\Crud\Config;
use Yadda\Enso\Crud\Forms\Fields\BelongsToManyField;
use Yadda\Enso\Crud\Forms\Fields\CheckboxField;
use Yadda\Enso\Crud\Forms\Fields\DateTimeField;
use Yadda\Enso\Crud\Forms\Fields\FileUploadFieldResumable;
use Yadda\Enso\Crud\Forms\Fields\SlugField;
use Yadda\Enso\Crud\Forms\Fields\TextField;
use Yadda\Enso\Crud\Forms\Form;
use Yadda\Enso\Crud\Forms\Section;
use Yadda\Enso\Crud\Tables\Publish;
use Yadda\Enso\Crud\Tables\Text;
use Yadda\Enso\Crud\Tables\Thumbnail;
use Yadda\Enso\Gyms\Checkout\MoneyHandler;
use Yadda\Enso\Gyms\Contracts\Gym;
use Yadda\Enso\Gyms\Contracts\Package as PackageModel;
use Yadda\Enso\Gyms\Contracts\PackageCrud;
use Yadda\Enso\Gyms\Traits\HandlesPublishableItems;
use Yadda\Enso\Meta\Crud\MetaSection;

class Package extends Config implements PackageCrud
{
    use HandlesPublishableItems;

    public function configure()
    {
        $package_model = resolve(PackageModel::class);

        $this
            ->model(get_class($package_model))
            ->route('admin.gyms.packages')
            ->views('packages')
            ->name('Package')
            ->orderable('order')
            ->order('order', 'desc')
            ->paginate(25)
            ->columns([
                Thumbnail::make('cms_thumbnail')
                    ->setLabel('')
                    ->orderableBy(null),
                Text::make('name'),
                Text::make('slug'),
                Publish::make('is_published', $this)
                    ->setLabel('Published')
                    ->addThClass('is-narrow'),
            ])
            ->setBulkActions([
                "actions" => $this->getPublishingBulkActions(),
            ])
            ->setItemFilters([
                'published' => $this->getPublishingFilter(),
                'search' => [
                    'type' => 'text',
                    'label' => 'Search',
                    'callable' => [$this, 'filterSearch'],
                ]
            ])
            ->rules([
                'main.slug'     => ['required', 'string'],
                'main.image_id' => ['required_if:main.published.published,published'],
                'main.gyms'     => ['required_if:main.published.published,published'],
            ])
            ->messages([
                'main.image_id.required_if' => 'A package must have an image to be published',
                'main.gyms.required_if'     => 'At least one Gym must be selected to published a package',
            ])
            ->setDeletable(false);
    }

    public function create(Form $form)
    {
        $form->addSections([
            Section::make('main')
                ->addFields([
                    TextField::make('mb_service_id')
                        ->setLabel("MindBody ID")
                        ->setDisabled(true),
                    TextField::make('price')
                        ->setHelpText('This is the price in the currency\'s lowest denomination.')
                        ->setAlterFormDataCallback([$this, 'fromLowestDenomination'])
                        ->setAlterRequestDataCallback([$this, 'toLowestDenomination'])
                        ->setDisabled(true)
                        ->setDefaultValue(null),
                    TextField::make('name')
                        ->setDisabled(true)
                        ->addFieldsetClass('is-6')
                        ->setDefaultValue(null),
                    SlugField::make('slug')
                        ->setRoute($this->getSlugRoute())
                        ->setSource('name')
                        ->addFieldsetClass('is-6'),
                    FileUploadFieldResumable::make('image_id')
                        ->setRelationshipName('image')
                        ->setUploadPath('gyms/packages'),
                    BelongsToManyField::make('gyms')
                        ->setOptions(resolve(Gym::class)::query()),
                    DateTimeField::make('publish_at')
                        ->addFieldsetClass('is-10')
                        ->setDefaultValue(null),
                    CheckboxField::make('published')
                        ->addFieldsetClass('is-2')
                        ->setLabel('Ready to Publish?')
                        ->setOptions([
                            'published' => '',
                        ]),
                ]),
            MetaSection::make('meta'),
        ]);

        return $form;
    }

    public function filterSearch($query, $value)
    {
        return $query->where(function ($sub_query) use ($value) {
            return $sub_query->where('name', 'LIKE', "%{$value}%")
                ->orWhere('slug', 'LIKE', "%{$value}%");
        });
    }

    public function fromLowestDenomination($pence)
    {
        return App::make(MoneyHandler::class)->fromLowestDenomination(
            $pence,
            config('enso.gyms.mindbody.currency', 'THB')
        );
    }

    public function toLowestDenomination($value)
    {
        return App::make(MoneyHandler::class)->toLowestDenomination(
            $value,
            config('enso.gyms.mindbody.currency', 'THB')
        );
    }

    /**
     * Gets the route to use for the slug field.
     *
     * @return void
     */
    protected function getSlugRoute()
    {
        return route('packages.show', '%SLUG%');
    }
}
