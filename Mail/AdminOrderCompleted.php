<?php

namespace Yadda\Enso\Gyms\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Queue\SerializesModels;
use Yadda\Enso\Gyms\Checkout\OrderTabulator;
use Yadda\Enso\Gyms\Contracts\Order;

class AdminOrderCompleted extends Mailable
{
    use Queueable, SerializesModels;

    public $order;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(Order $order)
    {
        $this->order = $order;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $this->subject('An order has been completed');

        $message = (new MailMessage)
            ->greeting('Hello,')
            ->line('An order has been completed and now needs assigning to the purchaser\'s MindBody account');

        if (!is_null($this->order->user_id)) {
            $message->line('The user has an account so their purchased packages should have automatically been attached to their account on Mindbody.');
        }

        $message->line('Order ID: **'. $this->order->uuid . '**')
            ->action(
                'See the Order',
                route('admin.gyms.orders.edit', $this->order->getKey())
            );

        $data = $message->data();
        $data['header_title'] = 'Order Complete';
        $data['table'] = OrderTabulator::convertOrderToMarkdownRows($this->order);

        return $this->markdown('vendor.notifications.order')->with($data);
    }
}
