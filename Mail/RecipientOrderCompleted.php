<?php

namespace Yadda\Enso\Gyms\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Queue\SerializesModels;
use Yadda\Enso\Gyms\Checkout\OrderTabulator;
use Yadda\Enso\Gyms\Contracts\Order;

class RecipientOrderCompleted extends Mailable
{
    use Queueable, SerializesModels;

    public $order;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(Order $order)
    {
        $this->order = $order;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $this->subject('Thanks for your order');

        $message = (new MailMessage)
            ->greeting('Hello ' . $this->order->billing_name)
            ->line('We have received your order and will add it to your MindBody account shortly')
            ->line('Order ID: **'. $this->order->uuid . '**');

        $data = $message->data();
        $data['header_title'] = 'Order Confirmation';
        $data['table'] = OrderTabulator::convertOrderToMarkdownRows($this->order);

        return $this->markdown('vendor.notifications.order')->with($data);
    }
}
